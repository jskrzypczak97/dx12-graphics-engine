struct VS_INPUT_UNLIT
{
    float3 vfMsPosition : POSITION; // Position in model space.
};

struct VS_OUTPUT_UNLIT
{
    float4 vfSsPosition : SV_POSITION;
};

struct VERTEX_TRANSFORM_MATRICES_UNLIT
{
    float4x4 mfWVP; // World view projection matrix.
};

// Resources
ConstantBuffer<VERTEX_TRANSFORM_MATRICES_UNLIT> cbTransformMatrices : register(b0);

VS_OUTPUT_UNLIT main(VS_INPUT_UNLIT vsInput)
{
    VS_OUTPUT_UNLIT vsOutput;
    
    vsOutput.vfSsPosition = mul(float4(vsInput.vfMsPosition, 1.0f), cbTransformMatrices.mfWVP);
    
    return vsOutput;
}