
#include "C:\Dev\dx12-graphics-engine\Includes\Renderer\Lights\LightDefines.h"
//#include "C:\dev\rendering\dx12-graphics-engine\Includes\Renderer\Lights\LightDefines.h"

#define PI 3.14159265359f

struct PS_INPUT
{
    float4 vfSsPosition : SV_Position; // Position in screen space.
    float3 vfWsPosition : POSITION; // Position in world space.
    float3 vfWsNormal : NORMAL; // Normal in world space.
    float3 vfWsTangent : TANGENT; // Tangent in world space.
    float3 vfWsBitangent : BITANGENT; // Bitangent in world space.
    float2 vfTextureCoords : TEXCOORD; // Texture coordinates.
};

struct LIGHT_DATA
{
    float3 vfWsPosition;
    float fIntensity;
    
    float3 vfWsDirection;
    uint uiLightType;
    
    float3 vfBaseColor;
    uint uiFlags;
};

struct LIGHT_ARRAY
{
    LIGHT_DATA data[LIGHT_COUNT_LIMIT];
};

struct CAMERA_DATA
{
    float3 vfWsPosition;
    uint _pad0;
};

struct MATERIAL_DATA_LIT
{
    uint uBaseColorTexIdx;
	float3 vfBaseColor;

	uint uRoughnessMetalnessTexIdx;
	float fRoughness;
	float fMetalness;
	uint uNormalTexIdx;
};

struct Surface
{
    float3 vfBaseColor;
    float fRoughness;

    float3 vfWsNormal;
    float fMetalness;
};

// Resources

ConstantBuffer<LIGHT_ARRAY> cbLightsData : register(b0);
ConstantBuffer<CAMERA_DATA> cbCameraData : register(b1);
ConstantBuffer<MATERIAL_DATA_LIT> cbMaterialData : register(b2);

SamplerState samp : register(s0);

bool CheckBit(uint uiFlags, uint uiBitPos)
{
    return (uiFlags & (1 << uiBitPos)) > 0;
}


// Normal distribution function, GGX/Triwbridge-Reitz
float GGX_D(float fNoH, float fAlpha)
{
    float fAlphaSqr = fAlpha * fAlpha;

    float fNumerator = fAlphaSqr;
    
    float fDenominatorTerm = fNoH * fNoH * (fAlphaSqr - 1.0) + 1.0;
    float fDenominator = PI * fDenominatorTerm * fDenominatorTerm;

    return fNumerator / fDenominator;
}

// Geometry function, Smith's method with Schlick-GGX
float GGX_G(float fNoV, float fNoL, float fAlpha)
{
    // Remapping for direct lighting.
    float fK = (fAlpha + 1.0) * (fAlpha + 1.0) / 8.0;

    float fGeometryTermGGX1 = fNoV / (fNoV * (1.0 - fK) + fK);
    float fGeometryTermGGX2 = fNoL / (fNoL * (1.0 - fK) + fK);
    
    return fGeometryTermGGX1 * fGeometryTermGGX2;
}

// Fresnel function, Fresnel-Schlick
float GGX_F(float fVoH, float3 vfF0)
{
    return vfF0 + (1.0 - vfF0) * pow(1.0 - fVoH, 5.0);
}

float3 ComputeSpecularTerm(Surface sSurface, float fNoL, float fNoV, float fVoH, float fNoH, float fAlpha, out float3 vfFresnel)
{
    // Surface reflection at zero/normal incidence.
    // Dielectric objects have 0.04 and for metallic objects we need to lerp using metalness. 
    float3 vfF0 = lerp(0.04.xxx, sSurface.vfBaseColor, sSurface.fMetalness);
    float3 vfF = GGX_F(fVoH, vfF0);
    float fG = GGX_G(fNoV, fNoL, fAlpha);
    float fD = GGX_D(fNoH, fAlpha);
    
    float fSpecularEpsilon = 0.0001;
    float3 vfSpecularNumerator = fD * fG * vfF;
    float fSpecularDenominator = max(4.0 * fNoV * fNoL, fSpecularEpsilon);
    
    vfFresnel = vfF;
    return vfSpecularNumerator / fSpecularDenominator;
}

float3 ComputeDiffuseTerm(Surface sSurface, float3 vfF)
{
    float kD = (float3(1.0.xxx) - vfF) * (1.0 - sSurface.fMetalness);
    
    return kD * (sSurface.vfBaseColor / PI);
}

float3 ComputeLightContribution(LIGHT_DATA sLightData, Surface sSurface, float3 vfWsSurfacePos, float3 vfWsViewDir)
{
    // Early exit if light disabled.
    if (!CheckBit(sLightData.uiFlags, LF_LIGHT_ENABLED))
    {
        return float3(0.0f, 0.0f, 0.0f);
    }
    
    const float fMinRoughness = 0.001;
    
    float fLinearRoughness = max(sSurface.fRoughness, fMinRoughness);
    float fRemappedRoughness = fLinearRoughness * fLinearRoughness;
    
    float3 vfWsRayToLight;
    float3 vfWsRayToLightDir; // L
    float fLightDistance;
    
    if (sLightData.uiLightType == LT_POINT_LIGHT)
    {
        vfWsRayToLight = sLightData.vfWsPosition - vfWsSurfacePos;
        vfWsRayToLightDir = normalize(vfWsRayToLight);
        fLightDistance = length(vfWsRayToLight);
    }
    else if (sLightData.uiLightType == LT_DIRECTIONAL_LIGHT)
    {
        vfWsRayToLight = -sLightData.vfWsDirection;
        vfWsRayToLightDir = normalize(vfWsRayToLight);
        fLightDistance = 1.0;
    }
    
    float fAttenuation = 1.0 / (fLightDistance * fLightDistance);
    
    float fNoL = saturate(dot(sSurface.vfWsNormal, vfWsRayToLightDir));

    if (fNoL <= 0.0)
    {
        return float3(0.0f, 0.0f, 0.0f);
    }

    float fNoV = max(abs(dot(sSurface.vfWsNormal, vfWsViewDir)), 1e-5f);
                 
    float3 vfWsHalfVectorDir = normalize(vfWsViewDir + vfWsRayToLightDir); // H
    
    float fNoH = saturate(dot(sSurface.vfWsNormal, vfWsHalfVectorDir));
    float fVoH = saturate(dot(vfWsViewDir, vfWsHalfVectorDir));

    float3 vfDiffuseLighting = sLightData.vfBaseColor * sLightData.fIntensity * fAttenuation;
    float3 vfSpecularLighting = sLightData.vfBaseColor * sLightData.fIntensity * fAttenuation;
    
    float3 vfF;
    float3 vfSpecularBrdf = ComputeSpecularTerm(sSurface, fNoL, fNoV, fVoH, fNoH, fRemappedRoughness, vfF);
    float3 fDiffuseBrdf = ComputeDiffuseTerm(sSurface, vfF);
    
    float3 vfDiffuseContribution = vfDiffuseLighting * fDiffuseBrdf * fNoL;
    float3 vfSpecularContribution = vfSpecularLighting * vfSpecularBrdf * fNoL;
    
    return vfDiffuseContribution + vfSpecularContribution;
}

float4 main(PS_INPUT psInput) : SV_TARGET
{
    float3 vfWsViewDir = normalize(cbCameraData.vfWsPosition - psInput.vfWsPosition);
    
    Surface sSurface;
    sSurface.vfBaseColor = cbMaterialData.vfBaseColor;
    if (cbMaterialData.uBaseColorTexIdx < 0xffffffff)
    {
        Texture2D colorTexture = ResourceDescriptorHeap[cbMaterialData.uBaseColorTexIdx];
        sSurface.vfBaseColor = colorTexture.Sample(samp, psInput.vfTextureCoords).rgb;
    }
    sSurface.fRoughness = cbMaterialData.fRoughness;
    sSurface.fMetalness = cbMaterialData.fMetalness;
    if (cbMaterialData.uRoughnessMetalnessTexIdx < 0xffffffff)
    {
        Texture2D roughnessMetalnessTexture = ResourceDescriptorHeap[cbMaterialData.uRoughnessMetalnessTexIdx];
        float2 roughnessMetalness = roughnessMetalnessTexture.Sample(samp, psInput.vfTextureCoords).gb;
        sSurface.fRoughness = roughnessMetalness.x;
        sSurface.fMetalness = roughnessMetalness.y;
    }
    sSurface.vfWsNormal = normalize(psInput.vfWsNormal);
    if (cbMaterialData.uNormalTexIdx < 0xffffffff)
    {
        Texture2D normalsTexture = ResourceDescriptorHeap[cbMaterialData.uNormalTexIdx];
        float3 rawNormal = normalsTexture.Sample(samp, psInput.vfTextureCoords).xyz;
        //sSurface.vfWsNormal = rawNormal;
        float3 normN = normalize(sSurface.vfWsNormal);
        float3 normT = normalize(psInput.vfWsTangent);
        float3 normB = normalize(psInput.vfWsBitangent);
        
        float3x3 mTBN = float3x3
        (
            normT,
            normB,
            normN
        );
        
        rawNormal.x = (rawNormal.x * 2.0 - 1.0);
        rawNormal.y = (rawNormal.y * 2.0 - 1.0);
        rawNormal.z = (rawNormal.z * 2.0 - 1.0);
        
        sSurface.vfWsNormal = normalize(mul(rawNormal, mTBN)); // Tangent space to World space.
    }

    float3 vfLightContribution = float3(0.0.xxx);
    for (uint i = 0; i < LIGHT_COUNT_LIMIT; ++i)
    {
        LIGHT_DATA sLightData = cbLightsData.data[i];
        
        vfLightContribution += ComputeLightContribution(sLightData, sSurface, psInput.vfWsPosition, vfWsViewDir);
    }
    
    float3 vfShadingPointColor = vfLightContribution;
    vfShadingPointColor *= sSurface.vfBaseColor;

    // HDR tonemapping
    vfShadingPointColor = vfShadingPointColor / (vfShadingPointColor + float3(1.0.xxx));
    // Gamma correction
    vfShadingPointColor = pow(vfShadingPointColor, float3((1.0 / 2.2).xxx));
    
    return float4(vfShadingPointColor, 1.0f);
}

// =====================================================================================================================

// I don't like how it looks, maybe needs to be debugged.
#if 0
// Normal distribution function
float D_Frostbite(float fNoH, float fAlpha)
{
    float fAlphaSqr = fAlpha * fAlpha;

    float fNumerator = fAlphaSqr;
    
    float fDenominatorTerm = (fNoH * fAlphaSqr - fNoH) * fNoH + 1.0;
    float fDenominator = fDenominatorTerm * fDenominatorTerm;

    return fNumerator / fDenominator;
}

// Visibility/Geometry function
float V_Frostbite(float fNoV, float fNoL, float fAlpha)
{
    // Remapping for direct lighting.
    float fAlphaSqr = fAlpha * fAlpha;

    float fGeometryTermV = fNoL * sqrt((-fNoV * fAlphaSqr + fNoV) * fNoV + fAlphaSqr);
    float fGeometryTermL = fNoV * sqrt((-fNoL * fAlphaSqr + fNoL) * fNoL + fAlphaSqr);

    return 0.5f / (fGeometryTermV + fGeometryTermL);
}

// Fresnel function
float3 F_Frostbite(float fVoH, float3 vfF0, float fF90)
{
    return vfF0 + (fF90 - vfF0) * pow(1.0 - fVoH, 5.0);
}

// Frostbite
float3 ComputeSpecularBRDF(float fNoL, float fNoV, float fVoH, float fNoH, float fAlpha)
{
    //float3 vfF0 = 0.16 * cbMaterialData.fReflectance * cbMaterialData.fReflectance * (1.0 - cbMaterialData.fMetalness) + cbMaterialData.vfAlbedo * cbMaterialData.fMetalness;
    //float3 vfF0 = 0.16 * cbMaterialData.fReflectance * cbMaterialData.fReflectance;

    float3 vfF0 = float3(0.04.xxx);
    vfF0 = lerp(vfF0, pow(cbMaterialData.vfAlbedo, 2.2.xxx), cbMaterialData.fMetalness);

    float3 vfF = F_Frostbite(fVoH, vfF0, 1.0);
    float fV = V_Frostbite(fNoV, fNoL, fAlpha);
    float fD = D_Frostbite(fNoH, fAlpha);
    
    float fEpsilon = 0.0001;
    float SpecularBRDFNumerator = (fD * fV) * vfF / PI;
    float SpecularBRDFDenominator = max(4.0 * fNoV * fNoL, fEpsilon);
    
    return SpecularBRDFNumerator / SpecularBRDFDenominator;
}

// Disney Frostbite
float3 ComputeDiffuseBRDF(float fNoL, float fNoV, float fVoH, float fLinearRoughness)
{
    float fEnergyBias = lerp(0.0, 0.5, fLinearRoughness);
    float fFd90 = fEnergyBias + 2.0 * fVoH * fVoH * fLinearRoughness;
    float3 fF0 = 1.0.xxx;
    
    float fLightScatter = F_Frostbite(fNoL, fF0, fFd90).x;
    float fViewScatter = F_Frostbite(fNoV, fF0, fFd90).x;
    float fEnergyFactor = lerp(1.0, 1.0 / 1.51, fLinearRoughness);
    
    return fLightScatter * fViewScatter * fEnergyFactor / PI;
}

float3 ComputeLightContribution(LIGHT_DATA sLightData, float3 vfWsSurfaceNormal, float3 vfWsSurfacePos, float3 vfWsViewDir)
{
    // Early exit if light disabled.
    if (!CheckBit(sLightData.uiFlags, LF_LIGHT_ENABLED))
    {
        return float3(0.0f, 0.0f, 0.0f);
    }
    
    float3 vfWsRayToLight;
    float3 vfWsRayToLightDir; // L
    float fLightDistance;
    
    if (sLightData.uiLightType == LT_POINT_LIGHT)
    {
        vfWsRayToLight = sLightData.vfWsPosition - vfWsSurfacePos;
        vfWsRayToLightDir = normalize(vfWsRayToLight);
        fLightDistance = length(vfWsRayToLight);
    }
    else if (sLightData.uiLightType == LT_DIRECTIONAL_LIGHT)
    {
        vfWsRayToLight = -sLightData.vfWsDirection;
        vfWsRayToLightDir = normalize(vfWsRayToLight);
        fLightDistance = 1.0;
    }
    
    float fAttenuation = 1.0 / (fLightDistance * fLightDistance);
    
    float3 vfWsHalfVectorDir = normalize(vfWsViewDir + vfWsRayToLightDir); // H
    
    float fNoV = max(abs(dot(vfWsSurfaceNormal, vfWsViewDir)), 1e-5f);
    float fNoL = saturate(dot(vfWsSurfaceNormal, vfWsRayToLightDir));
                 
    float fNoH = saturate(dot(vfWsSurfaceNormal, vfWsHalfVectorDir));
    float fVoH = saturate(dot(vfWsViewDir, vfWsHalfVectorDir));
    
    const float fMinRoughness = 0.001;
    const float fLinearRoughness = max(cbMaterialData.fRoughness, fMinRoughness);
    const float fRemappedRoughness = fLinearRoughness * fLinearRoughness;
    
    float3 vfDiffuseLighting = sLightData.vfBaseColor * fAttenuation; // include intensity and diffuse scale
    float3 vfSpecularLighting = sLightData.vfBaseColor * fAttenuation; // include intensity and specular scale
    
    float vfDiffuseBRDF = ComputeDiffuseBRDF(fNoL, fNoV, fVoH, fLinearRoughness);
    float3 vfSpecularBRDF = ComputeSpecularBRDF(fNoL, fNoV, fVoH, fNoH, fRemappedRoughness);
    
    float3 vfSurfaceColor = cbMaterialData.vfAlbedo - (cbMaterialData.vfAlbedo * cbMaterialData.fMetalness);
    
    float3 fDiffuse = vfDiffuseLighting * vfDiffuseBRDF * fNoL;
    float3 fSpecular = vfSpecularLighting * vfSpecularBRDF * fNoL;
    
    return (fDiffuse * vfSurfaceColor + fSpecular) * sLightData.fIntensity;
}
#endif