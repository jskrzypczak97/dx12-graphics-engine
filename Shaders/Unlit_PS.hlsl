struct PS_INPUT_UNLIT
{
    float4 vfSsPosition : SV_POSITION;
};

struct PS_OUTPUT_UNLIT
{
    float4 vfColor : SV_TARGET;
};

struct MATERIAL_DATA_UNLIT
{
    float3 vfAlbedo;
    uint1 _pad0;
};

// Resources
ConstantBuffer<MATERIAL_DATA_UNLIT> cbMaterialData : register(b0);

PS_OUTPUT_UNLIT main(PS_INPUT_UNLIT psInput)
{
    PS_OUTPUT_UNLIT psOutput;

    psOutput.vfColor = float4(cbMaterialData.vfAlbedo, 1.0);

    return psOutput;
}