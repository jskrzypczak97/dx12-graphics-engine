
struct VS_INPUT_LIT
{
    float3 vfMsPosition : POSITION; // Position in model space.
    float3 vfMsNormal : NORMAL; // Normal in model space.
    float3 vfMsTangent : TANGENT; // Tangent in model space.
    float3 vfMsBitangent : BITANGENT; // Bitangent in model space.
    float2 vfTextureCoords : TEXCOORD;
};

struct VS_OUTPUT_LIT
{
    float4 vfSsPosition : SV_POSITION; // Position in screen space.
    float3 vfWsPosition : POSITION; // Position in world space.
    float3 vfWsNormal : NORMAL; // Normal in world space.
    float3 vfWsTangent : TANGENT; // Normal in world space.
    float3 vfWsBitangent : BITANGENT; // Normal in world space.
    float2 vfTextureCoords : TEXCOORD;
    //float3x3 mTBN : TBN_MATRIX; // Tangent, Bitangent, Normal matrix.
};

struct VERTEX_TRANSFORM_MATRICES_LIT
{
    float4x4 mfWVP; // World view projection matrix.
    float4x4 mfWorld; // World matrix.
};

// Resources
ConstantBuffer<VERTEX_TRANSFORM_MATRICES_LIT> cbTransformMatrices : register(b0);

VS_OUTPUT_LIT main(VS_INPUT_LIT vsInput)
{
    VS_OUTPUT_LIT vsOutput;

    // Make it more coherent!
    vsOutput.vfSsPosition = mul(float4(vsInput.vfMsPosition, 1.0f), cbTransformMatrices.mfWVP);
    vsOutput.vfWsPosition = mul(float4(vsInput.vfMsPosition, 1.0f), cbTransformMatrices.mfWorld).xyz;
    vsOutput.vfTextureCoords = vsInput.vfTextureCoords;

    vsOutput.vfWsNormal = normalize(mul(vsInput.vfMsNormal, (float3x3)(cbTransformMatrices.mfWorld)));
    vsOutput.vfWsTangent = normalize(mul(vsInput.vfMsTangent, (float3x3)(cbTransformMatrices.mfWorld)));
    vsOutput.vfWsBitangent = normalize(mul(vsInput.vfMsBitangent, (float3x3)(cbTransformMatrices.mfWorld)));

    return vsOutput;
}