#pragma once

#include <DirectXMath.h>

namespace MATH
{
	constexpr float_t PI_32() { return 3.14159265f; }
	constexpr double_t PI_64() { return 3.1415926535897932; }

	constexpr float_t TWO_PI_32() { return 2.0f * PI_32(); }
	constexpr double_t TWO_PI_64() { return 2.0 * PI_64(); }

	constexpr float_t PI_OVER_TWO_32() { return PI_32() * 0.5f; }
	constexpr double_t PI_OVER_TWO_64() { return PI_64() * 0.5; }

	constexpr float_t DegToRad(const float_t deg)
	{
		return deg * PI_32() / 180.0f;
	}

	constexpr float_t RadToDeg(const float_t rad)
	{
		return rad * 180.0f / PI_32();
	}

	// Wrap angle between -PI and PI
	template<typename T>
	T WrapAngleNegPiPosPi(T angle) noexcept
	{
		const T modulo = (T)fmod(angle, (T)TWO_PI_64());

		T output = modulo;
		if (modulo > (T)PI_64())
		{
			output = modulo - (T)TWO_PI_64();
		}
		else if (modulo < -(T)PI_64())
		{
			output = modulo + (T)TWO_PI_64();
		}

		return output;
	}
}

inline void SetBit(uint32_t& value, uint32_t bitPos)
{
	value |= 1 << bitPos;
}

inline void ClearBit(uint32_t& value, uint32_t bitPos)
{
	value &= ~(1 << bitPos);
}

inline void ModifyBit(uint32_t& value, uint32_t bitPos, bool newState)
{
	newState ? SetBit(value, bitPos) : ClearBit(value, bitPos);
}
