#pragma once

#include <unordered_map>
#include <chrono>

#include "Utilities/Utility.h"

class Timer
{
public:
	enum class Unit : uint32_t
	{
		US = 1,
		MS = 1'000,
		S = 1'000'000,
		MIN = 60'000'000,
		H = 3'600'000'000
	};
public:
	Timer();
	~Timer() = default;

	void Mark(const std::string& timePointStr = "DEFAULT");
	double Peek(const std::string& timePointStr = "DEFAULT", Unit unit = Unit::MS) const;
private:
	std::unordered_map<std::string, std::chrono::steady_clock::time_point> m_MarkedTimePoints;
};
