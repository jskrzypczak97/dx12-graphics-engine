#pragma once

#include <unordered_map>
#include <functional>

#include "Scene/SceneModel/Mesh.h"

enum class BasicMeshType
{
	CUBE,
	SPHERE,
	PYRAMID
};

class BasicMeshGenerator
{
public:
	static Mesh MakeMesh(BasicMeshType meshType);
	static void MakeMesh(BasicMeshType meshType, Mesh& mesh);

	static void MakeCube(Mesh& mesh);

	static void MakeSphere(Mesh& mesh);
	static void MakeCustomSphere(Mesh& mesh, float radius, uint32_t latDiv, uint32_t longDiv);

	static void MakePyramid(Mesh& mesh);
private:
	static void Cache(const std::string& name, std::function<Mesh(const std::string&)> createFunction);

	static Mesh CreateCube(const std::string& name);
	static Mesh CreateSphere(const std::string& name, float radius, uint32_t latDiv, uint32_t longDiv);
	static Mesh CreatePyramid(const std::string& name);

	static std::vector<DirectX::XMFLOAT3> CalculateNormals(const std::vector<DirectX::XMFLOAT3>& vertices, const std::vector<uint32_t>& indices);
private:
	static std::unordered_map<std::string, Mesh> m_MeshCache;
};