#pragma once

#include <stdint.h>
#include <string>
#include <string_view>
#include <type_traits>

#include "Scene/SceneModel/Mesh.h"

namespace Utility
{
	template<typename T>
	concept Hashable = requires(T a)
	{
		{ std::hash<T>{}(a) } -> std::convertible_to<std::size_t>;
	};

	class Hasher
	{
	public:
		template<Hashable T>
		static uint64_t Hash(const T& data)
		{
			return std::hash<T>{}(data);
		}

		static uint64_t Hash(const Mesh& mesh)
		{
			std::string hashString;
			hashString.reserve(128);

			ConcatenateToString(hashString, mesh.Name, mesh.ParentModelName, mesh.GetVertexCount(), mesh.GetIndexCount());

			return Hash(hashString);
		}

		static uint64_t Hash(const TextureData& textureData)
		{
			return Hash(textureData.FullPath);
		}

	private:
		template<typename T>
		static std::string ToString(const T& data) requires std::is_arithmetic_v<T>
		{
			return std::to_string(data);
		}

		static std::string ToString(const std::string& data)
		{
			return data;
		}

		static std::string ToString(const std::string_view data)
		{
			return std::string(data);
		}

		template<typename FirstT>
		static void ConcatenateToString(std::string& string, const FirstT& first)
		{
			string.append(ToString(first) + '_');
		}

		template<typename FirstT, typename... RestT>
		static void ConcatenateToString(std::string& string, const FirstT& first, const RestT&... rest)
		{
			string.append(ToString(first) + '_');
			ConcatenateToString(string, rest...);
		}
	};
}