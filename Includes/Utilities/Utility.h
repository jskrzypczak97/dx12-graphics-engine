#pragma once

#define NOMINMAX

#include <Windows.h>
#include <string>
#include <sstream>
#include <format>
#include <vector>

#define TO_STRING(x) #x
#define TO_STRING_BUILTIN(x) TO_STRING(x)

#define CREATE_VARS(name) name,
#define CREATE_STRINGS(name) #name,

#define BREAK __debugbreak();
#ifdef RELEASE
	#define EXIT exit(EXIT_FAILURE);
#else // DEBUG
	#define EXIT BREAK exit(EXIT_FAILURE);
#endif

namespace Utility
{
#define LOGGING_COMPONENTS(comp) \
	comp(_ENGINE_) \
	comp(_WINDOW_) \
	comp(_RENDERER_) \
	comp(_SHADER_) \
	comp(_CAMERA_) \
	comp(_SCENE_) \
	comp(_UI_) \
	comp(_INPUT_) \
	comp(_DEBUG_)

	enum COMPONENTS
	{
		LOGGING_COMPONENTS(CREATE_VARS)
	};

	extern std::vector<std::string> g_LoggingComponents;
	extern std::hash<std::string> g_StringHasher;

	// At this point no support for wstrings in most functions :<
	void Print(const std::string& str);
	void Print(const std::wstring& str);
	void PrintMessage(const std::string& str);

	void StringToUpper(std::string& str);
	std::string StringToUpper(const std::string& str);

	std::wstring UTF8ToWideString(const std::string& str);
	std::string WideStringToUTF8(const std::wstring& wstr);

	uint64_t HashString(const std::string& string);

	template<typename... Types>
	std::string StringFormat(const std::string& format, Types&&... args)
	{
		return std::vformat(format, std::make_format_args(args...));
	}

	template<typename First>
	void PrintSubmessage(const First& first)
	{
		std::string str(first);

		if (str.find('\n') != std::string::npos)
		{
			std::stringstream streamStr(first);
			while (std::getline(streamStr, str, '\n'))
			{
				Print(" | " + str + '\n');
			}
		}
		else
		{
			Print(" | " + str + '\n');
		}
	}

	template<typename First, typename... Rest>
	void PrintSubmessage(const First& first, const Rest&... rest)
	{
		if constexpr (std::is_same<First, std::string>::value)
		{
			PrintSubmessage(first);
		}
		else
		{
			PrintSubmessage(std::string(first));
		}

		PrintSubmessage(rest...);
	}

	template<typename... StringsT>
	inline void PrintError(const char* file, const char* line, StringsT... subMsgs)
	{
		PrintMessage(StringFormat("Error in {}, line {}", file, line));
		if constexpr (sizeof...(StringsT) > 0)
			PrintSubmessage(subMsgs...);
	}

	template<typename... StringsT>
	inline void PrintInfo(COMPONENTS component, const std::string& msg, StringsT... subMsgs)
	{
		PrintMessage(StringFormat("[{}] : {}", g_LoggingComponents[static_cast<uint32_t>(component)], msg));
		if constexpr (sizeof...(StringsT) > 0)
			PrintSubmessage(subMsgs...);
	}

	template<typename... StringsT>
	inline void PrintAssert(const char* file, const char* line, StringsT... subMsgs)
	{
		PrintMessage(StringFormat("Assertion failed in {}, line {}", file, line));
		if constexpr (sizeof...(StringsT) > 0)
			PrintSubmessage(subMsgs...);
	}

}

#ifdef RELEASE
	#define GFXE_ASSERT(condition, ...)
	#define GFXE_ASSERT_HR(hr, ...)
	#define GFXE_DBG_ERROR(errMsg, ...)
	#define GFXE_DBG_INFO(component, msg, ...)
#else // DEBUG
	#define GFXE_ASSERT(condition, ...) \
		if (!(bool)(condition)) \
		{ \
			Utility::PrintAssert(TO_STRING_BUILTIN(__FILE__), TO_STRING_BUILTIN(__LINE__), __VA_ARGS__); \
			BREAK \
		}
	#define GFXE_ASSERT_HR(hr, ...) \
		if (FAILED((HRESULT)(hr))) \
		{ \
			Utility::PrintAssert(TO_STRING_BUILTIN(__FILE__), TO_STRING_BUILTIN(__LINE__), __VA_ARGS__); \
			BREAK \
		}
	#define GFXE_DBG_ERROR(errMsg, ...) \
		Utility::PrintError(TO_STRING_BUILTIN(__FILE__), TO_STRING_BUILTIN(__LINE__), __VA_ARGS__); \
		BREAK
	#define GFXE_DBG_INFO(component, msg, ...) \
		Utility::PrintInfo(component, msg, __VA_ARGS__);
#endif

#define GFXE_ERROR(...) \
	Utility::PrintError(TO_STRING_BUILTIN(__FILE__), TO_STRING_BUILTIN(__LINE__), __VA_ARGS__); \
	EXIT

#define GFXE_INFO(component, msg, ...)\
	Utility::PrintInfo(component, msg, __VA_ARGS__);
