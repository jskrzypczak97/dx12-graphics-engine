#pragma once

#include "Scene/SceneNodeEntity.h"

#include "Renderer/Lights/LightDefines.h"

enum LIGHT_TYPE
{
	POINT_LIGHT = LT_POINT_LIGHT,
	DIRECTIONAL_LIGHT = LT_DIRECTIONAL_LIGHT
};

struct LightAttributes
{
	DirectX::XMFLOAT3 AmbientColor;
	DirectX::XMFLOAT3 BaseColor;
	float_t Intensity;
};

class SceneLight : public SceneNodeEntity
{
	friend class EngineUI;
private:
	static std::atomic<uint64_t> s_CurrentLightId;
	// Allow UI access to private fields to enable object control
public:
	SceneLight(LIGHT_TYPE lightType);
	virtual ~SceneLight() = default;
	SceneLight(const SceneLight&) = delete;
	SceneLight& operator=(const SceneLight&) = delete;
	SceneLight(SceneLight&&) noexcept = default;

	virtual SCENE_ENTITY_TYPES Type() const override { return SCENE_ENTITY_TYPES::LIGHT; }

	inline bool IsActive() const { return m_IsActive; }
	inline void SetActive(bool isActive) { m_IsActive = isActive; }

	inline void SetBaseColor(const float_t r, const float_t g, const float_t b) { m_BaseColor = { r, g, b }; }
	inline void SetBaseColor(const DirectX::XMFLOAT3 rgb) { m_BaseColor = { rgb.x, rgb.y, rgb.z }; }

	inline void SetAmbientColor(float_t r, float_t g, float_t b) { m_AmbientColor = { r, g, b }; }
	inline void SetIntensity(float_t intensity) { m_Intensity = intensity; }

	inline LIGHT_TYPE GetLightType() const { return m_LightType; }

	inline LightAttributes GetLightAttributes() const
	{
		LightAttributes attributes = {};

		attributes.BaseColor = m_BaseColor;
		attributes.AmbientColor = m_AmbientColor;
		attributes.Intensity = m_Intensity;

		return attributes;
	}

private:
	LIGHT_TYPE m_LightType;

	DirectX::XMFLOAT3 m_BaseColor{ 1.0f, 1.0f, 1.0f };
	DirectX::XMFLOAT3 m_AmbientColor{ 0.15f, 0.15f, 0.15f };
	float_t m_Intensity = 300.0f;

	bool m_IsActive = true;
};