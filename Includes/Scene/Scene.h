#pragma once

#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <array>
#include <queue>
#include <algorithm>

#include "Engine/EngineConfig.h"
#include "Utilities/Utility.h"

#include "Scene/SceneNodeEntity.h"
#include "Scene/SceneModel/SceneMesh.h"
#include "Scene/SceneModel/ModelImporter.h"
#include "Scene/SceneLight/SceneLight.h"

struct SceneEntitiesIds
{
	SceneEntitiesIds()
	{
		constexpr size_t initialSize = 32;

		Ids.reserve(initialSize);
		NewIds.reserve(initialSize);
	}

	void AddId(uint64_t id)
	{
		Ids.insert(id);
		NewIds.insert(id);
	}

	void RemoveId(uint64_t id)
	{
		Ids.erase(id);
		NewIds.erase(id);
	}

	void Clear()
	{
		Ids.clear();
		NewIds.clear();
	}

	void ResetNewIds() { NewIds.clear(); }
	bool HasNewIds() const { return !NewIds.empty(); }

	const std::unordered_set<uint64_t>& GetIds() const { return Ids; }
	const std::unordered_set<uint64_t>& GetNewIds() const { return NewIds; }
private:
	std::unordered_set<uint64_t> Ids;
	std::unordered_set<uint64_t> NewIds;
};

class Scene
{
private:
	static ModelImporter sm_ModelImporter;
public:
	Scene()
	{
		constexpr size_t initialSize = 32;

		m_SceneEntities.reserve(initialSize);
	}
	~Scene()
	{
		m_SceneEntities.clear();
		
		for (auto& idsContainer : m_SceneEntitiesIds)
		{
			idsContainer.Clear();
		}
	}

	// TODO: We should cache loaded models somehow!
	SceneModel* LoadModel(const std::string& modelName, const std::string& modelPath)
	{
		auto pSceneModel = RegisterNewEntity<SceneModel>();
		pSceneModel->SetName(modelName);

		auto modelInfo = sm_ModelImporter.ReadModelFile(modelName, modelPath);

		sm_ModelImporter.ParseModelMeshes(modelInfo.ModelId, pSceneModel, pSceneModel->m_Meshes);
		sm_ModelImporter.ParseModelMaterials(modelInfo.ModelId, pSceneModel->m_Materials);

		std::array<std::queue<SceneNodeEntity*>, SCENE_ENTITY_TYPES::TYPES_COUNT> entityPool;
		for (uint32_t i = 0; i < modelInfo.MeshNodesCount; ++i)
		{
			entityPool[SCENE_ENTITY_TYPES::MESH].push(RegisterNewEntity<SceneMesh>());
		}

		for (uint32_t i = 0; i < modelInfo.EntityNodesCount; ++i)
		{
			entityPool[SCENE_ENTITY_TYPES::ENTITY].push(RegisterNewEntity<SceneNodeEntity>());
		}

		sm_ModelImporter.BuildModelTree(modelInfo.ModelId, pSceneModel, entityPool);

		sm_ModelImporter.ReleaseModelData(modelInfo.ModelId);

		return pSceneModel;
	}

	template<typename EntityT>
	EntityT* RegisterNewEntity()
	{
		EntityT entity;
		uint64_t entityId = entity.GetId();

		m_SceneEntitiesIds[entity.Type()].AddId(entityId);
		m_SceneEntities.insert(std::make_pair(
			entityId,
			std::make_unique<EntityT>(std::move(entity))));

		return static_cast<EntityT*>(m_SceneEntities.at(entityId).get());
	}

	template<typename T>
	void Append(T&& entity)
	{
		uint64_t id = entity.GetId();

		m_SceneEntitiesIds[entity.Type()].AddId(id);
		m_SceneEntities.insert(
			std::make_pair(id, std::make_unique<T>(std::move(entity)))
		);
	}

	inline bool HasNewEntities() const
	{
		for (const auto& idsContainer : m_SceneEntitiesIds)
		{
			if (idsContainer.HasNewIds())
			{
				return true;
			}
		}

		return false;
	}

	template<SCENE_ENTITY_TYPES EntityType>
	inline const std::unordered_set<uint64_t>& GetNewSceneEntitiesIds() const
	{
		return m_SceneEntitiesIds[EntityType].GetNewIds();
	}

	template<SCENE_ENTITY_TYPES EntityType>
	inline const std::unordered_set<uint64_t>& GetSceneEntitiesIds() const
	{
		return m_SceneEntitiesIds[EntityType].GetIds();
	}

	template<SCENE_ENTITY_TYPES EntityType>
	inline const void ResetNewSceneEntitiesIds()
	{
		return m_SceneEntitiesIds[EntityType].ResetNewIds();
	}

	inline const std::unordered_map<uint64_t, std::unique_ptr<SceneNodeEntity>>& GetSceneEntities() const { return m_SceneEntities; }

	template<typename T = SceneNodeEntity>
	inline const T& GetSceneEntity(const uint64_t entityID) const
	{
		GFXE_ASSERT((m_SceneEntities.contains(entityID)), Utility::StringFormat("Entity with ID {} is not part of the scene!", entityID));

		return dynamic_cast<const T&>(*m_SceneEntities.at(entityID));
	}

	template<typename T = SceneNodeEntity>
	inline T& GetSceneEntityRef(const uint64_t entityID) const
	{
		GFXE_ASSERT((m_SceneEntities.contains(entityID)), Utility::StringFormat("Entity with ID {} is not part of the scene!", entityID));

		return dynamic_cast<T&>(*m_SceneEntities.at(entityID));
	}

private:


	std::unordered_map<uint64_t, std::unique_ptr<SceneNodeEntity>> m_SceneEntities;

	std::array<SceneEntitiesIds, (size_t)(SCENE_ENTITY_TYPES::TYPES_COUNT)> m_SceneEntitiesIds;
};