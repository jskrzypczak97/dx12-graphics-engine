#pragma once

#include <vector>
#include <array>
#include <atomic>
#include <string>

#include <DirectXMath.h>

#include "Scene/Transform.h"

enum SCENE_ENTITY_TYPES : uint8_t
{
	ENTITY = 0,
	MODEL,
	MESH,
	LIGHT,

	// s_TypeToString needs to be filled with each new type.
	TYPES_COUNT
};

class SceneNodeEntity
{
private:
	static std::atomic<uint64_t> s_CurrentEntityId;
	static const std::array<std::string, SCENE_ENTITY_TYPES::TYPES_COUNT> s_TypeToString;
public:
	static std::string_view GetEntityTypeString(SCENE_ENTITY_TYPES type) { return s_TypeToString[(uint8_t)type]; }
public:
	SceneNodeEntity()
	{
		m_EntityId = s_CurrentEntityId++;
		m_pParentNode = nullptr;
		m_IsEnabled = true;
	}
	virtual ~SceneNodeEntity() = default;
	SceneNodeEntity(const SceneNodeEntity&) = delete;
	SceneNodeEntity& operator=(const SceneNodeEntity&) = delete;
	SceneNodeEntity(SceneNodeEntity&&) noexcept = default;

	virtual constexpr SCENE_ENTITY_TYPES Type() const { return SCENE_ENTITY_TYPES::ENTITY; };

	inline bool IsEnabled() const { return m_IsEnabled; }

	inline uint64_t GetId() const { return m_EntityId; }
	inline const std::string& GetName() const { return m_Name; }

	inline const SceneNodeEntity* GetParent() const { return m_pParentNode; }
	inline const SceneNodeEntity* GetChild(uint32_t index) const { return m_ChildNodes[index]; }
	inline uint32_t GetChildrenCount() const { return (uint32_t)m_ChildNodes.size(); }

	inline const Transform& GetTransform() const { return m_Transform; }
	inline Transform& GetTransformRef() { return m_Transform; }

	inline void SetName(const std::string& name) { m_Name = name; }
	inline void SetTransform(const Transform& transform) { m_Transform = transform; }

	inline void AssignParent(SceneNodeEntity* parent) { m_pParentNode = parent; }
	inline void AddChildNode(SceneNodeEntity* child) { m_ChildNodes.push_back(child); }
protected:
	std::string m_Name;
	uint64_t m_EntityId;

	bool m_IsEnabled;
	Transform m_Transform;

	SceneNodeEntity* m_pParentNode;
	std::vector<SceneNodeEntity*> m_ChildNodes;
};