﻿#pragma once

#include <atomic>

#include <DirectXMath.h>

#include "Utilities/MathHelper.h"
#include "Utilities/Utility.h"

class EngineUI;

class Transform
{
	friend class EngineUI;
public:
	// Move this to math helpers?
	static inline const DirectX::XMFLOAT4X4 UnpackMatrix(const DirectX::XMMATRIX& packedMatrix)
	{
		DirectX::XMFLOAT4X4 unpackedMatrix;
		UnpackMatrix(packedMatrix, unpackedMatrix);
		return unpackedMatrix;
	}
	static inline void UnpackMatrix(const DirectX::XMMATRIX& packedMatrix, DirectX::XMFLOAT4X4& unpackedMatrix)
	{
		DirectX::XMStoreFloat4x4(&unpackedMatrix, packedMatrix);
	}
	static inline const DirectX::XMMATRIX PackMatrix(const DirectX::XMFLOAT4X4& unpackedMatrix)
	{
		return DirectX::XMLoadFloat4x4(&unpackedMatrix);
	}
	static inline void PackMatrix(const DirectX::XMFLOAT4X4& unpackedMatrix, DirectX::XMMATRIX& packedMatrix)
	{
		packedMatrix = PackMatrix(unpackedMatrix);
	}
	static inline void TransposeMatrix(DirectX::XMMATRIX& packedMatrix)
	{
		packedMatrix = DirectX::XMMatrixTranspose(packedMatrix);
	}
	static inline float_t Round(float_t input)
	{
		constexpr float_t fPrecision = 0.00001f;
		return std::round(input / fPrecision) * fPrecision;
	}
public:
	Transform()
		: m_Translation(0.0f, 0.0f, 0.0f), m_Rotation(0.0f, 0.0f, 0.0f), m_Scale(1.0f, 1.0f, 1.0f), m_TransformMatrix()
	{
		XMStoreFloat4x4(&m_TransformMatrix, DirectX::XMMatrixIdentity());
	}
	Transform(const DirectX::XMFLOAT4X4& transformMatrix)
		: m_TransformMatrix(transformMatrix)
	{
		m_Translation = DirectX::XMFLOAT3(Round(transformMatrix._14), Round(transformMatrix._24), Round(transformMatrix._34));
		m_Scale = DirectX::XMFLOAT3(
			DirectX::XMVector3Length(DirectX::XMVECTOR{ transformMatrix._11, transformMatrix._21, transformMatrix._31 }).m128_f32[0],
			DirectX::XMVector3Length(DirectX::XMVECTOR{ transformMatrix._12, transformMatrix._22, transformMatrix._32 }).m128_f32[0],
			DirectX::XMVector3Length(DirectX::XMVECTOR{ transformMatrix._13, transformMatrix._23, transformMatrix._33 }).m128_f32[0]);

		DirectX::XMFLOAT3X3 rotationMatrix(
			transformMatrix._11 / m_Scale.x, transformMatrix._12 / m_Scale.y, transformMatrix._13 / m_Scale.z,
			transformMatrix._21 / m_Scale.x, transformMatrix._22 / m_Scale.y, transformMatrix._23 / m_Scale.z,
			transformMatrix._31 / m_Scale.x, transformMatrix._32 / m_Scale.y, transformMatrix._33 / m_Scale.z
		);

		// PSI (ψ) -> angle around X axis | Pitch
		// THETA (θ) -> angle around Y axis | Yaw
		// PHI (φ) -> angle around Z axis | Roll
		float_t fPsi, fTheta, fPhi;
		if (rotationMatrix._31 != -1.0f && rotationMatrix._31 != 1.0f)
		{
			fTheta = (float_t)(-asin(rotationMatrix._31)); // angle around y axis -> THETA
			float_t fThetaCos = (float_t)cos(fTheta);

			fPsi = (float_t)atan2(rotationMatrix._32 / fThetaCos, rotationMatrix._33 / fThetaCos); // angle around x axis -> PSI

			fPhi = (float_t)atan2(rotationMatrix._21 / fThetaCos, rotationMatrix._11 / fThetaCos); // angle around z axis -> PHI
		}
		else
		{
			fPhi = 0.0f; // This could be anything but we assume 0.

			if (rotationMatrix._31 < 0.0f) // -1.0
			{
				fTheta = MATH::PI_OVER_TWO_32();
				fPsi = fPhi + (float_t)atan2(rotationMatrix._12, rotationMatrix._13);
			}
			else // 1.0
			{
				fTheta = -MATH::PI_OVER_TWO_32();
				fPsi = -fPhi + (float_t)atan2(-rotationMatrix._12, -rotationMatrix._13);
			}
		}

		m_Rotation = DirectX::XMFLOAT3(Round(fPsi), Round(fTheta), Round(fPhi)); // Pitch, Yaw, Roll

		MarkAsDirty();
	}
	~Transform() = default;
	Transform(const Transform&) = default;
	Transform& operator=(const Transform&) = default;
	Transform(Transform&&) noexcept = default;

	inline DirectX::XMFLOAT3 GetTranslation() const { return m_Translation; }
	inline DirectX::XMFLOAT3 GetRotation() const { return m_Rotation; }
	inline DirectX::XMFLOAT3 GetScale() const { return m_Scale; }

	void SetTranslation(float_t x, float_t y, float_t z)
	{
		m_Translation = DirectX::XMFLOAT3(x, y, z);
		MarkAsDirty();
	}
	void SetRotation(float_t pitch, float_t yaw, float_t roll)
	{
		m_Rotation = DirectX::XMFLOAT3(pitch, yaw, roll);
		MarkAsDirty();
	}
	void SetScale(float_t x, float_t y, float_t z)
	{
		m_Scale = DirectX::XMFLOAT3(x, y, z);
		MarkAsDirty();
	}

	void Translate(float_t x, float_t y, float_t z)
	{
		m_Translation.x += x;
		m_Translation.y += y;
		m_Translation.z += z;

		MarkAsDirty();
	}
	void Rotate(float_t pitch, float_t yaw, float_t roll)
	{
		m_Rotation.x += pitch;
		m_Rotation.y += yaw;
		m_Rotation.z += roll;

		MarkAsDirty();
	}
	void Scale(float_t x, float_t y, float_t z)
	{
		m_Scale.x *= x;
		m_Scale.y *= y;
		m_Scale.z *= z;

		MarkAsDirty();
	}

	DirectX::XMMATRIX GetTransformMatrix() const
	{
		DirectX::XMMATRIX outputMatrix;

		if (m_IsDirty)
		{
			outputMatrix = RecalculateTransformMatrix();
			DirectX::XMStoreFloat4x4(&m_TransformMatrix, outputMatrix);
		}
		else
		{
			outputMatrix = XMLoadFloat4x4(&m_TransformMatrix);
		}

		return outputMatrix;
	}

	DirectX::XMMATRIX GetRotationMatrix() const
	{
		return DirectX::XMMatrixRotationRollPitchYaw(m_Rotation.x, m_Rotation.y, m_Rotation.z);
	}

	void MarkAsDirty() { m_IsDirty = true; }
private:
	const DirectX::XMMATRIX RecalculateTransformMatrix() const
	{
		return DirectX::XMMatrixScaling(m_Scale.x, m_Scale.y, m_Scale.z) *
			   DirectX::XMMatrixRotationX(m_Rotation.x) * DirectX::XMMatrixRotationY(m_Rotation.y) * DirectX::XMMatrixRotationZ(m_Rotation.z) * // XMMatrixRotationRollPitchYaw gives gimbal lock :/
			   DirectX::XMMatrixTranslation(m_Translation.x, m_Translation.y, m_Translation.z);
	}

private:
	bool m_IsDirty = false;

	DirectX::XMFLOAT3 m_Translation;
	DirectX::XMFLOAT3 m_Rotation; // Pitch / Yaw / Roll
	DirectX::XMFLOAT3 m_Scale;

	mutable DirectX::XMFLOAT4X4 m_TransformMatrix;
};