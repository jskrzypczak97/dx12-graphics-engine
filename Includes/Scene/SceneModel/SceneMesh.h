#pragma once

#include <vector>

#include "Scene/SceneNodeEntity.h"
#include "Scene/SceneModel/Mesh.h"
#include "Scene/SceneModel/SceneModel.h"

class SceneMesh : public SceneNodeEntity
{
	// Allow UI access to private fields to enable object control
	friend class EngineUI;
public:
	virtual constexpr SCENE_ENTITY_TYPES Type() const override { return SCENE_ENTITY_TYPES::MESH; };

	inline const Mesh* GetMesh() const { return m_pMesh; }
	inline void SetMesh(const Mesh* pMesh) { m_pMesh = pMesh; }

	inline bool IsVisible() const { return m_pParentModel->IsVisible() && m_IsVisible; }

	inline void AssignParentModel(const SceneModel* pParentModel) { m_pParentModel = pParentModel; }
private:
	bool m_IsVisible = true;
	const Mesh* m_pMesh;

	const SceneModel* m_pParentModel;
};