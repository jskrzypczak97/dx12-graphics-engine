#pragma once

#include <vector>
#include <string>

#include <DirectXMath.h>

#include "Utilities/Utility.h"
#include "Scene/SceneModel/Material.h"

class Mesh
{
public:
	Mesh() = default;
	~Mesh() = default;
	Mesh(const Mesh&) = default;
	Mesh& operator=(const Mesh&) = default;

	uint64_t Hash = 0;
	std::string Name = "";
	std::string_view ParentModelName;

	std::vector<DirectX::XMFLOAT3> Vertices;
	std::vector<DirectX::XMFLOAT3> Normals;
	std::vector<DirectX::XMFLOAT3> Tangents;
	std::vector<DirectX::XMFLOAT3> Bitangents;
	std::vector<DirectX::XMFLOAT3> TexCoords0;
	std::vector<uint32_t> Indices;

	uint32_t MaterialIndex = -1;
	const Material* pMaterial = nullptr;

	inline const uint32_t GetVertexCount() const { return (uint32_t)Vertices.size(); }
	inline const uint32_t GetIndexCount() const { return (uint32_t)Indices.size(); }
};