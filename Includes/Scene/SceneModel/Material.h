#pragma once

#include <DirectXMath.h>
#include <string>

struct TextureData
{
	inline bool IsDefined() const { return UvIndex < 0xffffffff; }

	uint32_t UvIndex = -1;
	uint64_t Hash = 0;

	std::string Name = "";
	std::string FullPath = "";
};

class Material
{
public:
	Material() = default;
	~Material() = default;
	Material(const Material&) = default;
	Material& operator=(const Material&) = default;

	std::string Name;

	TextureData BaseColorTexData;
	DirectX::XMFLOAT4 BaseColor{ 1.0f, 0.0f, 1.0f, 1.0f };

	TextureData RoughnessMetalnessTexData;
	float_t Roughness = 0.0f;
	float_t Metalness = 0.0f;

	TextureData NormalTexData;
};