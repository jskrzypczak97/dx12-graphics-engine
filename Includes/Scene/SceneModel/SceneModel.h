#pragma once

#include <vector>
#include <atomic>
#include <string>

#include <DirectXMath.h>

#include "Scene/SceneNodeEntity.h"
#include "Scene/SceneModel/Mesh.h"
#include "Scene/SceneModel/Material.h"

class SceneModel : public SceneNodeEntity
{
	// Allow UI access to private fields to enable object control
	friend class EngineUI;
	friend class Scene;
public:
	//SceneModel(const std::string& modelName, const std::string& modelPath);
	SceneModel() = default;
	virtual ~SceneModel() = default;
	SceneModel(const SceneModel&) = delete;
	SceneModel& operator=(const SceneModel&) = delete;
	SceneModel(SceneModel&&) noexcept = default;

	virtual constexpr SCENE_ENTITY_TYPES Type() const override { return SCENE_ENTITY_TYPES::MODEL; }

	inline const Mesh& GetMesh(uint32_t index) const { return m_Meshes[index]; }
	inline Mesh& GetMeshRef(uint32_t index) { return m_Meshes[index]; }

	inline const Material& GetMaterial(uint32_t index) const { return m_Materials[index]; }
	inline Material& GetMaterialRef(uint32_t index) { return m_Materials[index]; }

	inline bool IsVisible() const { return m_IsVisible; }

private:
	bool m_IsVisible = true;

	std::vector<Mesh> m_Meshes;
	std::vector<Material> m_Materials;
};