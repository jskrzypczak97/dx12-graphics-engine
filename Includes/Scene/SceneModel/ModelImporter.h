#pragma once

#include "assimp/scene.h"
#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"

#include "Scene/SceneNodeEntity.h"
#include "Scene/SceneModel/SceneModel.h"
#include "Utilities/Timer.h"
#include "Utilities/Hasher.h"

#include <string>
#include <vector>
#include <memory>
#include <filesystem>

class ModelImporter
{
public:
	struct ModelInfo
	{
		uint32_t ModelId;

		uint32_t MeshesCount;
		uint32_t MaterialsCount;

		uint32_t EntityNodesCount;
		uint32_t MeshNodesCount;

		std::string ModelName;
		std::string ModelFilePath;
		std::string ModelFileName;
	};
private:
	struct ModelData
	{
		ModelInfo Info;
		std::unique_ptr<aiScene> pData;
	};
public:
	const ModelInfo ReadModelFile(const std::string& modelName, const std::string& filePath)
	{
		static uint32_t s_ModelId = 0;

		GFXE_INFO(Utility::COMPONENTS::_SCENE_, Utility::StringFormat("Loading model \'{}\'...", modelName));
		m_Timer.Mark("LOADING_MODEL");

		//if (!ModelLoadSuccessful(m_DataImporter.ReadFile(filePath, aiProcess_Triangulate | aiProcess_MakeLeftHanded)))
		//if (!ModelLoadSuccessful(m_DataImporter.ReadFile(filePath, aiProcess_Triangulate | aiProcess_ConvertToLeftHanded)))
		//if (!ModelLoadSuccessful(m_DataImporter.ReadFile(filePath, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_ConvertToLeftHanded)))
		if (!ModelLoadSuccessful(m_DataImporter.ReadFile(filePath, aiProcess_Triangulate | aiProcess_RemoveRedundantMaterials | aiProcess_GenNormals | aiProcess_CalcTangentSpace | aiProcess_ConvertToLeftHanded)))
		//if (!ModelLoadSuccessful(m_DataImporter.ReadFile(filePath, aiProcess_Triangulate | aiProcess_RemoveRedundantMaterials | aiProcess_GenNormals | aiProcess_CalcTangentSpace)))
		//if (!ModelLoadSuccessful(m_DataImporter.ReadFile(filePath, aiProcess_Triangulate | aiProcess_RemoveRedundantMaterials | aiProcess_CalcTangentSpace)))
		{
			GFXE_INFO(Utility::COMPONENTS::_SCENE_, "Model file cound not be loaded correctly!", m_DataImporter.GetErrorString());
		}

		GFXE_INFO(Utility::COMPONENTS::_SCENE_, "Loading completed!", Utility::StringFormat("Took {:.3f}s.", m_Timer.Peek("LOADING_MODEL", Timer::Unit::S)));

		// Take ownership of the model.
		ModelData modelData = { .pData = std::make_unique<aiScene>(*m_DataImporter.GetOrphanedScene()) };

		modelData.Info.ModelId = s_ModelId;
		modelData.Info.ModelName = modelName;
		modelData.Info.ModelFileName = std::filesystem::path(filePath).filename().string();
		modelData.Info.ModelFilePath = std::filesystem::path(filePath).parent_path().string();
		modelData.Info.MeshesCount = modelData.pData->mNumMeshes;
		modelData.Info.MaterialsCount = modelData.pData->mNumMaterials;

		CountNodes(modelData.Info, modelData.pData->mRootNode);

		m_LoadedModels.insert(std::make_pair(modelData.Info.ModelId, std::move(modelData)));

		return m_LoadedModels.at(s_ModelId++).Info;
	}

	void ParseModelMeshes(uint32_t loadedModelId, const SceneModel* pParentModel, std::vector<Mesh>& meshes)
	{
		GFXE_ASSERT(m_LoadedModels.contains(loadedModelId), "Model not found!");

		const ModelData& loadedModel = m_LoadedModels.at(loadedModelId);
		auto* pAssimpModel = loadedModel.pData.get();

		GFXE_INFO(Utility::COMPONENTS::_SCENE_, "Processing meshes...");
		m_Timer.Mark("PROCESSING_MESHES");

		meshes.reserve(pAssimpModel->mNumMeshes);

		for (uint32_t i = 0; i < pAssimpModel->mNumMeshes; ++i)
		{
			meshes.emplace_back(Mesh());
			ProcessAssimpMesh(pAssimpModel->mMeshes[i], meshes.back());
			meshes.back().ParentModelName = pParentModel->GetName();
		}

		GFXE_INFO(Utility::COMPONENTS::_SCENE_, "Parsing meshes completed!", Utility::StringFormat("Took {:.3f}s", m_Timer.Peek("PROCESSING_MESHES", Timer::Unit::S)));

	}

	void ParseModelMaterials(uint32_t loadedModelId, std::vector<Material>& materials)
	{
		GFXE_ASSERT(m_LoadedModels.contains(loadedModelId), "Model not found!");

		const ModelData& modelData = m_LoadedModels.at(loadedModelId);

		auto pAssimpModel = modelData.pData.get();

		GFXE_INFO(Utility::COMPONENTS::_SCENE_, "Processing materials...");
		m_Timer.Mark("PROCESSING_MATERIALS");

		materials.reserve(pAssimpModel->mNumMaterials);

		for (uint32_t i = 0; i < pAssimpModel->mNumMaterials; ++i)
		{
			materials.emplace_back(Material());
			ProcessAssimpMaterial(pAssimpModel->mMaterials[i], materials.back(), modelData.Info);
		}

		GFXE_INFO(Utility::COMPONENTS::_SCENE_, "Parsing materials completed!", Utility::StringFormat("Took {:.3f}s", m_Timer.Peek("PROCESSING_MATERIALS", Timer::Unit::S)));

	}

	void BuildModelTree(uint32_t loadedModelId, SceneModel* pRootSceneModel, std::array<std::queue<SceneNodeEntity*>, SCENE_ENTITY_TYPES::TYPES_COUNT>& entityPool)
	{
		GFXE_ASSERT(m_LoadedModels.contains(loadedModelId), "Model not found!");

		auto pAssimpModel = m_LoadedModels.at(loadedModelId).pData.get();

		pRootSceneModel->SetTransform(Transform());

		ProcessNode(pAssimpModel->mRootNode, pRootSceneModel, pRootSceneModel, entityPool);
	}

	void ReleaseModelData(uint32_t loadedModelId)
	{
		GFXE_ASSERT(m_LoadedModels.contains(loadedModelId), "Model not found!");

		m_LoadedModels.erase(loadedModelId);
	}

private:
	void ProcessNode(aiNode* pAssimpNode, SceneNodeEntity* pParentNode, SceneModel* pRootSceneModel, std::array<std::queue<SceneNodeEntity*>, SCENE_ENTITY_TYPES::TYPES_COUNT>& entityPool)
	{
		SceneNodeEntity* pNode;
		pNode = entityPool[SCENE_ENTITY_TYPES::ENTITY].front();
		entityPool[SCENE_ENTITY_TYPES::ENTITY].pop();

		for (uint32_t i = 0; i < pAssimpNode->mNumMeshes; ++i)
		{
			auto pMeshNode = reinterpret_cast<SceneMesh*>(entityPool[SCENE_ENTITY_TYPES::MESH].front());
			entityPool[SCENE_ENTITY_TYPES::MESH].pop();

			auto& mesh = pRootSceneModel->GetMeshRef(pAssimpNode->mMeshes[i]);
			mesh.pMaterial = &pRootSceneModel->GetMaterial(mesh.MaterialIndex);

			pMeshNode->SetMesh(&mesh);

			pMeshNode->AssignParent(pNode);
			pMeshNode->AssignParentModel(pRootSceneModel);
			pMeshNode->SetName(mesh.Name);
			pMeshNode->SetTransform(Transform());

			pNode->AddChildNode(pMeshNode);
		}

		pNode->AssignParent(pParentNode);
		pNode->SetName(std::string(pAssimpNode->mName.C_Str()));

		if (pNode->GetName().contains("curtain_09"))
		{
			int dbg = 0;
		}

		aiVector3D scale;
		aiVector3D rotation;
		aiVector3D translation;
		pAssimpNode->mTransformation.Decompose(scale, rotation, translation);
		//pAssimpNode->mTransformation.Transpose();

		const DirectX::XMFLOAT4X4* pTransform = reinterpret_cast<DirectX::XMFLOAT4X4*>(&(pAssimpNode->mTransformation));
		pNode->SetTransform(Transform(*pTransform));

		pParentNode->AddChildNode(pNode);

		for (uint32_t i = 0; i < pAssimpNode->mNumChildren; ++i)
		{
			ProcessNode(pAssimpNode->mChildren[i], pNode, pRootSceneModel, entityPool);
		}
	}

	void ProcessAssimpMesh(const aiMesh* pAssimpMesh, Mesh& mesh)
	{
		mesh.Vertices.reserve(pAssimpMesh->mNumVertices);
		auto* pAssimpVerticesStart = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mVertices);
		auto* pAssimpVerticesEnd = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mVertices + pAssimpMesh->mNumVertices);
		mesh.Vertices.insert(mesh.Vertices.end(), pAssimpVerticesStart, pAssimpVerticesEnd);

		mesh.Normals.reserve(pAssimpMesh->mNumVertices);
		auto* pAssimpNormalsStart = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mNormals);
		auto* pAssimpNormalsEnd = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mNormals + pAssimpMesh->mNumVertices);
		mesh.Normals.insert(mesh.Normals.end(), pAssimpNormalsStart, pAssimpNormalsEnd);

		if (pAssimpMesh->mTangents)
		{
			mesh.Tangents.reserve(pAssimpMesh->mNumVertices);
			auto* pAssimpTangentStart = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mTangents);
			auto* pAssimpTangentEnd = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mTangents + pAssimpMesh->mNumVertices);
			mesh.Tangents.insert(mesh.Tangents.end(), pAssimpTangentStart, pAssimpTangentEnd);
		}

		if (pAssimpMesh->mBitangents)
		{
			mesh.Bitangents.reserve(pAssimpMesh->mNumVertices);
			auto* pAssimpBitangentStart = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mBitangents);
			auto* pAssimpBitangentEnd = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mBitangents + pAssimpMesh->mNumVertices);
			mesh.Bitangents.insert(mesh.Bitangents.end(), pAssimpBitangentStart, pAssimpBitangentEnd);
		}

		if (pAssimpMesh->HasTextureCoords(0))
		{
			mesh.TexCoords0.reserve(pAssimpMesh->mNumVertices);
			auto* pAssimpTexCoordsStart = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mTextureCoords[0]);
			auto* pAssimpTexCoordsEnd = reinterpret_cast<DirectX::XMFLOAT3*>(pAssimpMesh->mTextureCoords[0] + pAssimpMesh->mNumVertices);
			mesh.TexCoords0.insert(mesh.TexCoords0.end(), pAssimpTexCoordsStart, pAssimpTexCoordsEnd);
		}

		mesh.Indices.reserve(pAssimpMesh->mNumFaces * 3); // Triangulate flag handles that.
		for (uint32_t i = 0; i < pAssimpMesh->mNumFaces; ++i)
		{
			mesh.Indices.insert(mesh.Indices.end(), pAssimpMesh->mFaces[i].mIndices, pAssimpMesh->mFaces[i].mIndices + 3);
		}

		mesh.Name = Utility::StringFormat("{}", std::string(pAssimpMesh->mName.C_Str()));

		mesh.MaterialIndex = pAssimpMesh->mMaterialIndex;
		mesh.pMaterial = nullptr;

		mesh.Hash = Utility::Hasher::Hash(mesh);
	}

	void ProcessAssimpMaterial(const aiMaterial* pAssimpMaterial, Material& material, const ModelInfo& modelInfo)
	{
		material.Name = pAssimpMaterial->GetName().C_Str();

		if (pAssimpMaterial->GetTextureCount(aiTextureType_BASE_COLOR) > 0)
		{
			aiString textureFilePath;
			uint32_t uvIndex;
			if (pAssimpMaterial->GetTexture(aiTextureType_BASE_COLOR, 0, &textureFilePath, nullptr, &uvIndex) == aiReturn::aiReturn_SUCCESS)
			{
				material.BaseColorTexData.UvIndex = uvIndex;
				material.BaseColorTexData.FullPath = Utility::StringFormat("{}\\{}", modelInfo.ModelFilePath, std::filesystem::path(textureFilePath.C_Str()).make_preferred().string());
				material.BaseColorTexData.Hash = Utility::Hasher::Hash(material.BaseColorTexData);
			}
			else
			{
				GFXE_ERROR("Could not obtain texture data!");
			}
		}

		// Roughness and Metalness
		if (pAssimpMaterial->GetTextureCount(aiTextureType_DIFFUSE_ROUGHNESS) > 0)
		{
			aiString textureFilePath;
			uint32_t uvIndex;
			if (pAssimpMaterial->GetTexture(aiTextureType_DIFFUSE_ROUGHNESS, 0, &textureFilePath, nullptr, &uvIndex) == aiReturn::aiReturn_SUCCESS)
			{
				material.RoughnessMetalnessTexData.UvIndex = uvIndex;
				material.RoughnessMetalnessTexData.FullPath = Utility::StringFormat("{}\\{}", modelInfo.ModelFilePath, std::filesystem::path(textureFilePath.C_Str()).make_preferred().string());
				material.RoughnessMetalnessTexData.Hash = Utility::Hasher::Hash(material.RoughnessMetalnessTexData);
			}
			else
			{
				GFXE_ERROR("Could not obtain texture data!");
			}
		}

		if (pAssimpMaterial->GetTextureCount(aiTextureType_NORMALS) > 0)
		{
			aiString textureFilePath;
			uint32_t uvIndex;
			if (pAssimpMaterial->GetTexture(aiTextureType_NORMALS, 0, &textureFilePath, nullptr, &uvIndex) == aiReturn::aiReturn_SUCCESS)
			{
				material.NormalTexData.UvIndex = uvIndex;
				material.NormalTexData.FullPath = Utility::StringFormat("{}\\{}", modelInfo.ModelFilePath, std::filesystem::path(textureFilePath.C_Str()).make_preferred().string());
				material.NormalTexData.Hash = Utility::Hasher::Hash(material.NormalTexData);
			}
			else
			{
				GFXE_ERROR("Could not obtain texture data!");
			}
		}
	}

	void CountNodes(ModelInfo& modelInfo, aiNode* pRootNode)
	{
		modelInfo.EntityNodesCount++;
		modelInfo.MeshNodesCount += pRootNode->mNumMeshes;

		for (uint32_t i = 0; i < pRootNode->mNumChildren; ++i)
		{
			CountNodes(modelInfo, pRootNode->mChildren[i]);
		}

		return;
	}

	bool ModelLoadSuccessful(const aiScene* pModel)
	{
		return pModel && pModel->mRootNode && !(pModel->mFlags & AI_SCENE_FLAGS_INCOMPLETE);
	}

private:
	std::unordered_map<uint32_t, ModelData> m_LoadedModels;
	Timer m_Timer;
	Assimp::Importer m_DataImporter;
};