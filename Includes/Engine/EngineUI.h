#pragma once

#include <imgui/imgui_impl_win32.h>

#include "Engine/EngineConfig.h"
#include "Scene/Scene.h"
#include "Renderer/Renderer.h"

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

class EngineWindow;
class Renderer;

//template<SCENE_ENTITY_TYPES> struct SceneNodeEntityT {};
//
//template<> struct SceneNodeEntityT<SCENE_ENTITY_TYPES::ENTITY> { typedef SceneNodeEntity type; };
//template<> struct SceneNodeEntityT<SCENE_ENTITY_TYPES::LIGHT> { typedef SceneLight type; };
//template<> struct SceneNodeEntityT<SCENE_ENTITY_TYPES::MODEL> { typedef SceneModel type; };
//template<> struct SceneNodeEntityT<SCENE_ENTITY_TYPES::MESH> { typedef SceneMesh type; };

class EngineUI
{
public:
	EngineUI() = default;
	~EngineUI();
	EngineUI(const EngineUI&) = delete;
	EngineUI& operator=(const EngineUI&) = delete;
	EngineUI(const EngineUI&&) noexcept = delete;
	EngineUI& operator=(const EngineUI&&) noexcept = delete;

	bool Initialize(EngineWindow& window);

	bool IsEnabled() { return m_IsEnabled; }
	bool EnableSwitch() { m_IsEnabled = !m_IsEnabled; return m_IsEnabled; }

	bool ProcessWndMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	void StartFrame();
	void EndFrame();

	void DisplaySceneNodes(Scene* pScene);
	void DisplayRenderOptions(RenderOptions& renderOptions);
	void DisplayPerformanceData(FrameData& frameData);

private:
	void DisplaySceneModelTree(const SceneNodeEntity* pNode);
	void DisplaySceneLightTree(const SceneNodeEntity* pNode);

	void DisplaySelectedEntityProperties(SceneNodeEntity& sceneEntity);
	void DisplaySelectedModelProperties(SceneModel& sceneModel);
	void DisplaySelectedMeshProperties(SceneMesh& sceneMesh);
	void DisplaySelectedLightProperties(SceneLight& sceneLight);

	void DisplayNodeTransform(Transform& transform);
	void DisplayNodeTransformReadOnly(const Transform& transform);

private:
	bool m_IsEnabled = true;

	uint64_t m_SelectedNodeId = std::numeric_limits<uint64_t>::max();
};