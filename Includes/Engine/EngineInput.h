#pragma once

#include <Windows.h>
// External dependency
#include <Keyboard.h>
#include <Mouse.h>
//

#include "Engine/EngineWindow.h"

class EngineInput
{
public:
	EngineInput();
	~EngineInput() = default;
	EngineInput(const EngineInput&) = delete;
	EngineInput& operator=(const EngineInput&) = delete;
	EngineInput(const EngineInput&&) noexcept = delete;
	EngineInput& operator=(const EngineInput&&) noexcept = delete;

	bool Initialize(EngineWindow& window);

	void UpdateState();

	// A thin wrapper that makes the use of this object more consistent.
	inline bool IsKeyDown(DirectX::Keyboard::Keys key) { return m_CurrentKeyboardState.IsKeyDown(key); }
	inline bool IsKeyUp(DirectX::Keyboard::Keys key) { return m_CurrentKeyboardState.IsKeyUp(key); }
	inline bool IsKeyJustPressed(DirectX::Keyboard::Keys key)
	{
		return m_CurrentKeyboardState.IsKeyDown(key) && m_LastKeyboardState.IsKeyUp(key);
	}
	inline bool IsKeyJustReleased(DirectX::Keyboard::Keys key)
	{
		return m_CurrentKeyboardState.IsKeyUp(key) && m_LastKeyboardState.IsKeyDown(key);
	}
	inline bool IsLeftMouseButtonJustPressed()
	{
		return m_CurrentMouseState.leftButton && !m_LastMouseState.leftButton;
	}
	inline bool IsLeftMouseButtonJustReleased()
	{
		return !m_CurrentMouseState.leftButton && m_LastMouseState.leftButton;
	}
	inline bool IsRightMouseButtonJustPressed()
	{
		return m_CurrentMouseState.rightButton && !m_LastMouseState.rightButton;
	}
	inline bool IsRightMouseButtonJustReleased()
	{
		return !m_CurrentMouseState.rightButton && m_LastMouseState.rightButton;
	}

	inline const DirectX::Keyboard::State GetKeyboardState() { return m_CurrentKeyboardState; }
	inline const DirectX::Mouse::State GetMouseState() { return m_CurrentMouseState; }
private:
	bool ProcessWndMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	std::unique_ptr<DirectX::Keyboard> m_Keyboard;
	std::unique_ptr<DirectX::Mouse> m_Mouse;

	DirectX::Keyboard::State m_CurrentKeyboardState;
	DirectX::Mouse::State m_CurrentMouseState;

	DirectX::Keyboard::State m_LastKeyboardState;
	DirectX::Mouse::State m_LastMouseState;
};