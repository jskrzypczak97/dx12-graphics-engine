#pragma once

#include "Engine/EngineWindow.h"
#include "Engine/EngineUI.h"
#include "Engine/EngineInput.h"
#include "Renderer/Renderer.h"

#include "Camera/Camera.h"
#include "Utilities/Timer.h"

class GraphicsEngine
{
public:
	GraphicsEngine() = default;
	~GraphicsEngine() = default;
	GraphicsEngine(const GraphicsEngine&) = delete;
	GraphicsEngine& operator=(const GraphicsEngine&) = delete;
	GraphicsEngine(const GraphicsEngine&&) noexcept = delete;
	GraphicsEngine& operator=(const GraphicsEngine&&) noexcept = delete;

	bool Initialize(HINSTANCE hInstance, uint32_t nCmdShow, uint32_t width, uint32_t height, const std::wstring& name, const std::wstring& title);
	int32_t Run();
private:
	void StartFrame();
	void EndFrame();
	void DoFrame(float_t frameDelta);

	bool SetupScene();
	bool SetupCamera();

	void ProcessInput(float_t frameDelta);
	void ProcessUI();

	void UpdateScene(float_t frameDelta);
private:
	EngineWindow m_Window;
	Renderer m_Renderer;
#ifdef GFXE_ENABLE_UI
	EngineUI m_UI;
#endif
	EngineInput m_Input;

	Timer m_Timer;

	std::unique_ptr<Camera> m_ActiveCamera;
	std::unique_ptr<Scene> m_ActiveScene;
};