#pragma once

#include <Windows.h>
#include <optional>
#include <format>
#include <functional>
#include <unordered_map>

#include "Utilities/Utility.h"

class EngineWindow
{
public:
	static HWND GetHwnd() { return m_Hwnd; }
	static uint32_t GetWidth() { return m_Width; }
	static uint32_t GetHeight() { return m_Height; }
	static std::optional<int> ProccessMessages();

public:
	EngineWindow() = default;
	~EngineWindow()
	{
		FreeCursor();
	}
	EngineWindow(const EngineWindow&) = delete;
	EngineWindow& operator=(const EngineWindow&) = delete;
	EngineWindow(const EngineWindow&&) noexcept = delete;
	EngineWindow& operator=(const EngineWindow&&) noexcept = delete;

	bool Initialize(HINSTANCE hInstance, uint32_t nCmdShow, uint32_t width, uint32_t height, const std::wstring& name, const std::wstring& title);

	void RegisterWinMsgsListeningCallback(const std::string& stringId, std::function<bool(HWND,UINT, WPARAM, LPARAM)>&& callbackFunction) const;
	void UnregisterWinMsgsListeningCallback(const std::string& stringId) const;

	void SetTitle(const std::wstring& newTitle);
	void EnableCursor();
	void DisableCursor();

private:
	static LRESULT CALLBACK HandleMsgThunk(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK HandleMsgSetup(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	// Only need one window for now
	static HWND m_Hwnd;
	static WNDCLASSEX m_WndClass;
	static uint32_t m_Width;
	static uint32_t m_Height;

private:
	LRESULT HandleWndMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	inline void HideCursor() const;
	inline void ShowCursor() const;
	inline void ConfineCursor() const;
	inline void FreeCursor() const;

	mutable std::unordered_map<std::string, std::function<bool(HWND, UINT, WPARAM, LPARAM)>> m_WinMsgsListeningCallbacks;

	bool m_CursorEnabled = true;
	std::wstring m_Name;
	std::wstring m_Title;
};
