#pragma once

#include <DirectXMath.h>

#include "Utilities/MathHelper.h"
#include "Utilities/Utility.h"

class Camera
{
public:
	Camera();
	~Camera() = default;
	Camera(const Camera&) = delete;
	Camera& operator=(const Camera&) = delete;
	Camera(Camera&&) noexcept = default;

	void Translate(float x, float y, float z);
	void Rotate(float deltaX, float deltaY);
	void SetRotation(float_t yaw, float_t pitch);

	void ResetTransformations();

	inline void EnableControl() { m_IsControlled = true; }
	inline void DisableControl() { m_IsControlled = false; }
	inline bool IsControlled() { return m_IsControlled;	}

	const DirectX::XMMATRIX GetViewMatrix();

	inline DirectX::XMFLOAT3 GetPosition() const { return m_Position; }
	void SetPosition(float x, float y, float z, bool setDefault = false);
	inline void SetPosition(DirectX::XMFLOAT3 newPosition, bool setDefault = false) { SetPosition(newPosition.x, newPosition.y, newPosition.z, setDefault); }
	void SetDefaultPosition(float x, float y, float z);

	inline const DirectX::XMMATRIX GetProjectionMatrix() const { return XMLoadFloat4x4(&m_ProjectionMatrix); }
	inline void SetProjectionMatrix(DirectX::XMFLOAT4X4&& projectionMatrix) { m_ProjectionMatrix = std::move(projectionMatrix); }
private:
	DirectX::XMMATRIX CalculateViewMatrix() const;

private:
	DirectX::XMFLOAT3 m_Position;
	DirectX::XMFLOAT3 m_DefaultPosition;

	DirectX::XMFLOAT3 m_BaseForwardDirection;
	DirectX::XMFLOAT3 m_BaseUpDirection;

	float m_Pitch;
	float m_Yaw;

	float m_RotationSpeed;
	float m_TravelSpeed;

	bool m_ViewMatrixUpdateNeeded;

	DirectX::XMFLOAT4X4 m_ViewMatrix;
	DirectX::XMFLOAT4X4 m_ProjectionMatrix;

	bool m_IsControlled;
};