#pragma once

#include <dxgi1_6.h>

#include <stdint.h>
#include <string>

#include "Utilities/Utility.h"

struct Adapter
{
	enum class VendorId : uint32_t
	{
		INTEL = 0x8086,
		NVIDIA = 0x10DE,
		AMD = 0x3EA,
		SOFTWARE = 0xFFFFFFFF
	};

	static VendorId VendorStringToId(const std::string& vendorString)
	{
		std::string vendorStringUpper = Utility::StringToUpper(vendorString);

		if (vendorStringUpper == "INTEL")
		{
			return VendorId::INTEL;
		}
		else if (vendorStringUpper == "NVIDIA")
		{
			return VendorId::NVIDIA;
		}
		else if (vendorStringUpper == "AMD")
		{
			return VendorId::AMD;
		}
		else if (vendorStringUpper == "SOFTWARE")
		{
			return VendorId::SOFTWARE;
		}
		else
		{
			GFXE_ERROR("Unrecognised vendor!");
		}

		return VendorId::SOFTWARE;
	}

	void CollectData()
	{
		if (pDxAdapter != nullptr)
		{
			pDxAdapter->GetDesc1(&dxDesc);
			vendorId = (VendorId)(dxDesc.VendorId);
		}
	}

	VendorId vendorId;
	DXGI_ADAPTER_DESC1 dxDesc;
	Microsoft::WRL::ComPtr<IDXGIAdapter1> pDxAdapter;
};