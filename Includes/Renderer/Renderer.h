#pragma once

#include <d3d12.h>
#include <dxgi1_6.h>

#include "directx\d3dx12.h"

#include "Utilities/Utility.h"
#include "Renderer/Adapter.h"
#include "Renderer/Device.h"
#include "Renderer/Display.h"
#include "Renderer/Profiler.h"

#include "Renderer/Context/CommandContextManager.h"
#include "Renderer/Resources/ResourceManager.h"
#include "Renderer/DescriptorHeaps/DescriptorHeapManager.h"
#include "Renderer/ConstantBuffers/ConstantBufferPoolManager.h"

#include "Scene/Scene.h"
#include "Camera/Camera.h"

#include "Renderer/RenderEntities/RenderObject.h"
#include "Renderer/RenderPasses/MainPass.h"
#include "Renderer/RenderPasses/GuiPass.h"

struct RenderOptions
{
	bool EnableNormalMapping = true;
};

struct FrameData
{
	uint64_t FrameNumber = 0;
	uint32_t ProfilerSlot = 0xffffffff;

	std::array<double_t, ProfileGpuStages::STAGES_COUNT> FrameGpuMeasurements;

	void GetGpuMeasurements(std::array<double_t, Profiler::s_TimestampSlotSize> gpuMeasurements)
	{
		for (uint32_t i = 0; i < ProfileGpuStages::STAGES_COUNT; ++i)
		{
			FrameGpuMeasurements[i] = gpuMeasurements[i];
		}
	}
};

class Renderer
{
	friend class MainPass;
public:
	static std::unique_ptr<CommandContextManager> sm_CommandContextManager;
	static std::unique_ptr<ResourceManager> sm_ResourceManager;
	static std::unique_ptr<DescriptorHeapManager> sm_DescriptorHeapManager;
	static std::unique_ptr<ConstantBufferPoolManager> sm_ConstantBufferPoolManager;
public:
	Renderer() = default;
	~Renderer() = default;
	Renderer(const Renderer&) = delete;
	Renderer& operator=(Renderer const&) = delete;

	bool Initialize();
	void ShutDown();

	void PreRender(Scene* pScene);
	void Render(Scene* pScene, Camera* pCamera);
	void PostRender();

	void ProcessNewSceneEntities(Scene* pScene);

	RenderObject CreateRenderObject(const Mesh* mesh, const Material* material, const SceneNodeEntity* parentNode);
	uint64_t RegisterRenderObject(RenderObject&& renderObject);

	inline void SetRenderUI(bool renderUI) { m_RenderUI = renderUI; }

	inline RenderOptions& GetRenderOptionsRef() { return m_RenderOptions; }
	inline FrameData& GetCurrentFrameDataRef() { return m_CurrentFrameData; }

private:
	bool m_RenderUI;

	Device m_Device;
	Adapter m_Adapter;
	Display m_Display;
	Profiler m_Profiler;

	MainPass m_MainPass;
#ifdef GFXE_ENABLE_UI
	GuiPass m_GuiPass;
#endif

	RenderOptions m_RenderOptions;
	FrameData m_CurrentFrameData;

	std::array<Texture, GFXE_CONFIG_RT_COUNT> m_RtSurfaces;
	std::array<Texture, GFXE_CONFIG_DS_COUNT> m_DsSurfaces;
private:
	static std::atomic<uint64_t> sm_CurrentRenderEntityId;
	// Render object should be per pass?
	// Each object would contain a data needed to render given entity in the given pass
	std::unordered_map<uint64_t, RenderObject> m_RenderObjects;
};