#pragma once

#include <stdint.h>

#include "Renderer/Device.h"
#include "Utilities/Utility.h"

using ContextId = uint64_t;
using FenceValue = uint64_t;

class CommandContext
{
public:
	CommandContext(const D3D12_COMMAND_LIST_TYPE type, ContextId id)
		: m_Type(type), m_CommandList(nullptr), m_Id(id), m_DoneFenceValue(UINT64_MAX), m_IsClosed(false), m_AssignedCommandAllocator(nullptr)
	{}
	~CommandContext() = default;
	CommandContext(const CommandContext&) = delete;
	CommandContext& operator=(CommandContext const&) = delete;

	bool Initialize(const DeviceHandle& deviceHandle, ID3D12CommandAllocator* allocator);

	void Close();
	void Clear();

	void Reset(ID3D12CommandAllocator* allocator);

	ID3D12CommandAllocator* GetAssignedCommandAllocator() { return m_AssignedCommandAllocator; }

	inline ID3D12GraphicsCommandList* GetDxCommandList() const { return m_CommandList.Get(); }
	inline ID3D12GraphicsCommandList* const* GetDxCommandListPtr() const { return m_CommandList.GetAddressOf(); }

	inline const D3D12_COMMAND_LIST_TYPE GetType() const { return m_Type; }

	inline const ContextId GetContextId() const { return m_Id; }

private:
	const D3D12_COMMAND_LIST_TYPE m_Type;

	bool m_IsClosed;
	ContextId m_Id;
	FenceValue m_DoneFenceValue;
	ID3D12CommandAllocator* m_AssignedCommandAllocator;

	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> m_CommandList;
};