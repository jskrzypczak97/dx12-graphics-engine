#pragma once

#include <unordered_map>
#include <array>

#include "Renderer/Context/CommandQueue.h"
#include "Renderer/Context/CommandContext.h"

#include "Utilities/Utility.h"

class CommandContextManager
{
public:
	CommandContextManager(const DeviceHandle& deviceHandle)
		: m_DeviceHandle(deviceHandle), m_GraphicsQueue(D3D12_COMMAND_LIST_TYPE_DIRECT), m_ComputeQueue(D3D12_COMMAND_LIST_TYPE_COMPUTE)
	{
		m_GraphicsQueue.Initialize(m_DeviceHandle);
		m_ComputeQueue.Initialize(m_DeviceHandle);

		constexpr uint32_t initCommandContextCount = GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT;
		for (const auto type : { D3D12_COMMAND_LIST_TYPE_DIRECT, D3D12_COMMAND_LIST_TYPE_COMPUTE })
		{
			m_CommandContextPool[type].reserve(initCommandContextCount);
			m_AvailableCommandContexts.insert({
				type,
				std::queue<CommandContext*>()
				});

			auto& queue = GetCommandQueue(type);
			for (uint32_t i = 0; i < initCommandContextCount; ++i)
			{
				auto* cmdAllocator = queue.RequestAllocator();

				m_CommandContextPool[type].emplace_back(std::make_unique<CommandContext>(type, i));
				m_CommandContextPool[type][i]->Initialize(m_DeviceHandle, cmdAllocator);
				m_AvailableCommandContexts[type].push(m_CommandContextPool[type][i].get());

				queue.DisposeAllocator(cmdAllocator);
			}
		}
	}
	~CommandContextManager() = default;
	CommandContextManager(const CommandContextManager&) = delete;
	CommandContextManager& operator=(CommandContextManager const&) = delete;

	void ShutDown();

	CommandContext* RequestCommandContext(const D3D12_COMMAND_LIST_TYPE type);
	void DisposeCommandContext(CommandContext* commandContext)
	{
		D3D12_COMMAND_LIST_TYPE type = commandContext->GetType();

		commandContext->Close();
		GetCommandQueue(type).DisposeAllocator(commandContext->GetAssignedCommandAllocator());

		m_AvailableCommandContexts[type].push(commandContext);
	}

	uint64_t ExecuteContext(CommandContext* context, bool disposeContext = true)
	{
		context->Close();

		auto& queue = GetCommandQueue(context->GetType());

		queue.GetDxCommandQueue()->ExecuteCommandLists(1, reinterpret_cast<ID3D12CommandList* const*>(context->GetDxCommandListPtr()));

		if (disposeContext)
		{
			DisposeCommandContext(context);
		}

		return queue.SetFence();
	}

	uint64_t ExecuteContextAndWait(CommandContext* context, bool disposeContext = true)
	{
		uint64_t fenceValue = ExecuteContext(context, false);

		WaitForQueueFence(context->GetType(), fenceValue);

		if (disposeContext)
		{
			DisposeCommandContext(context);
		}

		return fenceValue;
	}

	uint64_t GetCurrentQueueFence(const D3D12_COMMAND_LIST_TYPE queueType)
	{
		return GetCommandQueue(queueType).GetCompletedFenceValue();
	}

	uint64_t SetQueueFence(const D3D12_COMMAND_LIST_TYPE queueType)
	{
		return GetCommandQueue(queueType).SetFence();
	}

	void WaitForQueueFence(const D3D12_COMMAND_LIST_TYPE queueType, uint64_t fenceValue)
	{
		auto& queue = GetCommandQueue(queueType);

		queue.WaitForFence(fenceValue);
	}

	inline CommandQueue& GetCommandQueue(const D3D12_COMMAND_LIST_TYPE type)
	{ 
		return type == D3D12_COMMAND_LIST_TYPE_DIRECT ? m_GraphicsQueue : m_ComputeQueue;
	}
private:
	DeviceHandle m_DeviceHandle;

	std::unordered_map<D3D12_COMMAND_LIST_TYPE, std::vector<std::unique_ptr<CommandContext>>> m_CommandContextPool;
	std::unordered_map<D3D12_COMMAND_LIST_TYPE, std::queue<CommandContext*>> m_AvailableCommandContexts;

	CommandQueue m_GraphicsQueue;
	CommandQueue m_ComputeQueue;
};