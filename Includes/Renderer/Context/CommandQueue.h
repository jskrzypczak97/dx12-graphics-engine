#pragma once

#include <vector>
#include <memory>
#include <queue>

#include <d3d12.h>
#include <wrl.h>

#include "Engine/EngineConfig.h"
#include "Renderer/Device.h"

#include "Utilities/Utility.h"

class CommandQueue
{
public:
	CommandQueue(D3D12_COMMAND_LIST_TYPE type)
		: m_DeviceHandle(nullptr), m_Type(type), m_FenceValue(0), m_FenceEvent(nullptr), m_TimestampFrequency(0)
	{}
	~CommandQueue() = default;
	CommandQueue(const CommandQueue&) = delete;
	CommandQueue& operator=(CommandQueue const&) = delete;

	bool Initialize(const DeviceHandle& deviceHandle);
	void ShutDown();

	ID3D12CommandAllocator* RequestAllocator();
	void DisposeAllocator(ID3D12CommandAllocator* allocator);

	uint64_t SetFence();
	void WaitForFence(uint64_t expectedValue = UINT64_MAX);
	void SyncQueue();

	inline uint64_t GetFenceValue() { return m_FenceValue; }
	inline uint64_t GetCompletedFenceValue() { return m_pFence->GetCompletedValue(); }

	inline ID3D12CommandQueue* GetDxCommandQueue() { return m_pCommandQueue.Get(); }

	inline uint64_t GetTimestampFrequency() const { return m_TimestampFrequency; }

private:
	DeviceHandle m_DeviceHandle;
	const D3D12_COMMAND_LIST_TYPE m_Type;
	Microsoft::WRL::ComPtr<ID3D12CommandQueue> m_pCommandQueue;

	Microsoft::WRL::ComPtr<ID3D12Fence> m_pFence;
	uint64_t m_FenceValue;
	HANDLE m_FenceEvent;

	uint64_t m_TimestampFrequency;

	std::vector<Microsoft::WRL::ComPtr<ID3D12CommandAllocator>> m_CommandAllocatorPool;
	std::queue<ID3D12CommandAllocator*> m_AvailableCommandAllocators;
};