#pragma once

#include <d3d12.h>
#include <wrl.h>

#include <unordered_map>
#include <string>

#include "Renderer/DescriptorHeaps/DescriptorHeapRange.h"

class DescriptorHeap
{
	friend DescriptorHeapRange;
public:
	DescriptorHeap(const uint32_t size, const uint32_t incrementSize, const D3D12_DESCRIPTOR_HEAP_DESC dxDesc, ID3D12DescriptorHeap* dxDescriptorHeap);
	~DescriptorHeap() = default;
	DescriptorHeap(const DescriptorHeap&) = delete;
	DescriptorHeap& operator=(DescriptorHeap const&) = delete;
	DescriptorHeap(DescriptorHeap&&) noexcept = default;
	DescriptorHeap& operator=(DescriptorHeap&&) noexcept = default;

	void Initialize();

	inline DescriptorHeapRange* GetMainRange() { return &m_Ranges.at("MAIN_RANGE"); }
	inline DescriptorHeapRange* GetRange(const std::string& subrangeId)
	{ 
		GFXE_ASSERT(m_Ranges.contains(subrangeId), "Subrange does not exist!");

		return &m_Ranges.at(subrangeId);
	}
	inline ID3D12DescriptorHeap* GetDxDescriptorHeap() { return m_DxDescriptorHeap.Get(); }
	inline ID3D12DescriptorHeap** GetDxDescriptorHeapAddress() { return m_DxDescriptorHeap.GetAddressOf(); }

	inline bool IsShaderVisible() const
	{
		return m_DxDesc.Flags == D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	}

private:
	std::unordered_map<std::string, DescriptorHeapRange> m_Ranges;

	uint32_t m_Size;
	uint32_t m_IncrementSize;
	D3D12_DESCRIPTOR_HEAP_DESC m_DxDesc;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_DxDescriptorHeap;
};
