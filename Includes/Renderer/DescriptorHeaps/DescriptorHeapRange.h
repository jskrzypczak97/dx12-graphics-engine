#pragma once

#include <d3d12.h>
#include <wrl.h>

#include <string>
#include <memory>

#include "Utilities/Utility.h"

class DescriptorHeap;

class DescriptorHeapRange
{
public:
	DescriptorHeapRange(
		const std::string& stringId,
		const uint32_t size,
		const uint32_t globalOffset,
		const D3D12_CPU_DESCRIPTOR_HANDLE& cpuHandle,
		const D3D12_GPU_DESCRIPTOR_HANDLE& gpuHandle,
		DescriptorHeap* parent);

	DescriptorHeapRange* ReserveSubrange(const std::string& subrangeId, const uint32_t subsize);
	uint32_t ReserveDescriptor();

	D3D12_CPU_DESCRIPTOR_HANDLE GetCpuDescriptorHandleLocal(uint32_t offset = 0) const;
	D3D12_CPU_DESCRIPTOR_HANDLE GetCpuDescriptorHandleGlobal(uint32_t offset) const;

	D3D12_GPU_DESCRIPTOR_HANDLE GetGpuDescriptorHandleLocal(uint32_t offset = 0) const;
	D3D12_GPU_DESCRIPTOR_HANDLE GetGpuDescriptorHandleGlobal(uint32_t offset) const;

private:
	inline bool IsDescriptorShaderVisible() const;

	DescriptorHeap* m_Parent;
	std::string m_StringId;

	D3D12_CPU_DESCRIPTOR_HANDLE m_StartCpuHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE m_StartGpuHandle;
	uint32_t m_GlobalOffset;
	uint32_t m_CurrentLocalOffset = 0u;

	uint32_t m_DescriptorsCount;
	uint32_t m_AvailableDescriptorsCount;
};