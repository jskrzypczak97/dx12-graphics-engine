#pragma once

#include <d3d12.h>
#include <wrl.h>

#include <unordered_map>
#include <utility>

#include "Renderer/DescriptorHeaps/DescriptorHeap.h"

#include "Renderer/Device.h"
#include "Utilities/Utility.h"

enum class DescriptorHeapType : uint32_t
{
	RTV = 0,
	DSV = 1,
	CBV_SRV_UAV = 2,
	SAMPLER = 3
};

// Create 4 main DH (one of each type) by default and allow adding more if needed.
// D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV requires special attention as we may want to divide this heap internally to
// have each sub type in control. For example first N descriptors will be for CBs. Maybe we should introduce sth like DH range
// to make the division more flexible. In that case D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV will consist of 3 ranges. Ranges could be
// split in sub ranges so that other objects/classes can manage descriptors internally without bothering descriptor heap manager
// For example DH range describing CBs will be given to constant buffer pool manager which will divide the range according to its needs
// like each cb pool will get its range
class DescriptorHeapManager
{
public:
	DescriptorHeapManager(const DeviceHandle& deviceHandle)
		: m_DeviceHandle(deviceHandle)
	{}
	~DescriptorHeapManager() = default;
	DescriptorHeapManager(const DescriptorHeapManager&) = delete;
	DescriptorHeapManager& operator=(DescriptorHeapManager const&) = delete;

	void CreateGeneralUseDescriptorHeaps();
	void ShutDown();

	DescriptorHeap* CreateDescriptorHeap(const uint32_t id, uint32_t descriptorsCount, D3D12_DESCRIPTOR_HEAP_TYPE type, bool isShaderVisible);
	DescriptorHeap* GetDescriptorHeap(const DescriptorHeapType id);
	DescriptorHeapRange* GetDescriptorHeapRange(const DescriptorHeapType type, const std::string& rangeString);
private:
	DeviceHandle m_DeviceHandle;
	std::unordered_map<uint32_t, DescriptorHeap> m_DescriptorHeaps;
};