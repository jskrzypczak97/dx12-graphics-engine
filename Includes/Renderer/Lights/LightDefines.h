
#ifndef __LIGHT_DEFINES__
#define __LIGHT_DEFINES__

#define LIGHT_COUNT_LIMIT 16

// Light flags. Max 32.
#define LF_LIGHT_ENABLED 0

// Light types.
#define LT_POINT_LIGHT 0
#define LT_DIRECTIONAL_LIGHT 1

#endif // __LIGHT_DEFINES__