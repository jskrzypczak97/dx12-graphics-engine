#pragma once

#include <stdint.h>
#include <string>
#include <unordered_map>

#include <d3d12.h>
#include <dxgi1_6.h>
#include <dxgidebug.h>

#include "directx\d3dx12.h"

#include "Renderer/Adapter.h"
#include "Engine/EngineConfig.h"
#include "Utilities/Utility.h"

class RendererUtilities
{
	friend class Renderer;

	static void EnableDebugLayer(uint32_t dxgiFactoryFlags, bool enableGPUBasedValidation)
	{
		Microsoft::WRL::ComPtr<ID3D12Debug1> pDebugInterface;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&pDebugInterface))))
		{
			pDebugInterface->EnableDebugLayer();
			pDebugInterface->SetEnableGPUBasedValidation(enableGPUBasedValidation);
		}
		else
		{
			GFXE_DBG_ERROR("Unable to obtain D3D12 debug interface!");
		}

		Microsoft::WRL::ComPtr<IDXGIInfoQueue> pDXGIInforQueue;
		if (SUCCEEDED(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&pDXGIInforQueue))))
		{
			pDXGIInforQueue->SetBreakOnSeverity(DXGI_DEBUG_ALL, DXGI_INFO_QUEUE_MESSAGE_SEVERITY_ERROR, true);
			pDXGIInforQueue->SetBreakOnSeverity(DXGI_DEBUG_ALL, DXGI_INFO_QUEUE_MESSAGE_SEVERITY_CORRUPTION, true);

			DXGI_INFO_QUEUE_MESSAGE_ID hide[] =
			{
				80 /* IDXGISwapChain::GetContainingOutput: The swapchain's adapter does not control the output on which the swapchain's window resides. */,
			};
			DXGI_INFO_QUEUE_FILTER filter = {};
			filter.DenyList.NumIDs = _countof(hide);
			filter.DenyList.pIDList = hide;
			pDXGIInforQueue->AddStorageFilterEntries(DXGI_DEBUG_DXGI, &filter);
		}
		else
		{
			GFXE_ERROR("Unable to obtain DXGI debug interface!");
		}
	}

	static bool FindAdapter(const std::string& requestedAdapterStr, IDXGIFactory6* pDxgiFactory, Adapter& adapter)
	{
		bool canUseRequestedAdapter = false;
		auto requestedAdapterId = Adapter::VendorStringToId(requestedAdapterStr);

		GFXE_DBG_INFO(Utility::COMPONENTS::_RENDERER_, std::format("Looking for {} adapter...", Utility::StringToUpper(requestedAdapterStr)).c_str());

		for (uint32_t i = 0; DXGI_ERROR_NOT_FOUND != pDxgiFactory->EnumAdapters1(i, adapter.pDxAdapter.GetAddressOf()); ++i)
		{
			adapter.CollectData();

			if (adapter.vendorId != requestedAdapterId)
			{
				continue;
			}

			if (SUCCEEDED(D3D12CreateDevice(adapter.pDxAdapter.Get(), GFXE_CONFIG_DX_FEATURE_LEVEL, _uuidof(ID3D12Device), nullptr)))
			{
				canUseRequestedAdapter = true;

				GFXE_DBG_INFO(Utility::COMPONENTS::_RENDERER_,
					Utility::StringFormat("Requested adapter ({}) found and can be used!", Utility::StringToUpper(requestedAdapterStr)),
					Utility::StringFormat("{} ({} MB)", Utility::WideStringToUTF8(std::wstring(adapter.dxDesc.Description)), adapter.dxDesc.DedicatedVideoMemory >> 20));

				break;
			}
			else
			{
				GFXE_ERROR("Requested adapter was found but D3D12 device cannot be created using this adapter!", "Perhaps requested DX Feature Level is too high.");
			}
		}

		return adapter.pDxAdapter != nullptr;
	}
};