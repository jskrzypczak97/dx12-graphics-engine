#pragma once

#include "Renderer/Context/CommandContext.h"
#include "Renderer/Resources/Texture.h"

// Set of free helper functions for simple render operations
namespace RenderOperations
{
	void ClearRtsDss(CommandContext* pCommandContext, std::vector<Texture*> pRts, std::vector<Texture*> pDss);
	void CopySurface2D(CommandContext* pCommandContext, Texture* pSrc, Texture* pDst);
	void TransitionResource(CommandContext* pCommandContext, Resource* pResource, D3D12_RESOURCE_STATES newState);
}