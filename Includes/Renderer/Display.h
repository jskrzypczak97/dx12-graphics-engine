#pragma once

#include <d3d12.h>
#include <dxgi1_6.h>
#include <wrl.h>

#include <array>

#include "Renderer/Resources/Texture.h"
#include "Renderer/DescriptorHeaps/DescriptorHeapRange.h"

#include "Engine/EngineConfig.h"
#include "Renderer/Device.h"
#include "Utilities/Utility.h"

class Display
{
public:
	Display() = default;
	~Display() = default;
	Display(const Display&) = delete;
	Display& operator=(Display const&) = delete;

	bool Initialize(const DeviceHandle& deviceHandle, IDXGIFactory6* pDxgiFactory);

	void Present()
	{
		m_pSwapChain->Present(SYNC_DISPLAY, 0);
		m_FrameIndex = ++m_FrameIndex % GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT;
	}

	inline const uint32_t GetWidth() const { return m_SwapChainDesc.Width; }
	inline const uint32_t GetHeight() const { return m_SwapChainDesc.Height; }

	inline const D3D12_VIEWPORT GetViewport() { return m_DisplayViewport; }
	inline const D3D12_RECT GetScissorRect() { return m_DisplayScissorRect; }

	inline Texture* GetCurrentBackBuffer() { return &m_BackBuffers[m_FrameIndex]; }
	inline uint32_t GetCurrentFrameIndex() { return m_FrameIndex; }

private:
	DeviceHandle m_DeviceHandle;

	uint32_t m_FrameIndex = 0;

	DescriptorHeapRange* m_pDescriptorHeapRange;
	std::array<Texture, GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT> m_BackBuffers;

	DXGI_SWAP_CHAIN_DESC1 m_SwapChainDesc;
	Microsoft::WRL::ComPtr<IDXGISwapChain1> m_pSwapChain;

	D3D12_VIEWPORT m_DisplayViewport;
	D3D12_RECT m_DisplayScissorRect;
};