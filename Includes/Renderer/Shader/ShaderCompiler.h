#pragma once

#include <wrl.h>
//#include <atlbase.h>        // Common COM helpers.
#include <dxcapi.h>         // Be sure to link with dxcompiler.lib.
#include <d3d12shader.h>    // Shader reflection.

#include <vector>
#include <string>
#include <unordered_map>

#include "Engine/EngineConfig.h"
#include "Utilities/Utility.h"
#include "Renderer/Shader/Shader.h"

class ShaderCompiler
{
public:
	ShaderCompiler()
	{
		DxcCreateInstance(CLSID_DxcUtils, IID_PPV_ARGS(&m_pDxUtils));
		DxcCreateInstance(CLSID_DxcCompiler, IID_PPV_ARGS(&m_pDxCompiler));

		m_pDxUtils->CreateDefaultIncludeHandler(&m_pDxIncludeHandler);

		std::wstring shaderModelVersionString = Utility::UTF8ToWideString(Utility::StringFormat("_{}_{}", GFXE_SHADER_MODEL_VERSION_MAJOR, GFXE_SHADER_MODEL_VERSION_MINOR));
		m_ShaderTypeToTargetStringMap[SHADER_TYPE::VS] = L"vs" + shaderModelVersionString;
		m_ShaderTypeToTargetStringMap[SHADER_TYPE::PS] = L"ps" + shaderModelVersionString;
		m_ShaderTypeToTargetStringMap[SHADER_TYPE::CS] = L"cs" + shaderModelVersionString;
	}
	~ShaderCompiler() = default;
	ShaderCompiler(const ShaderCompiler&) = delete;
	ShaderCompiler& operator=(ShaderCompiler const&) = delete;

	// Based on https://github.com/microsoft/DirectXShaderCompiler/wiki/Using-dxc.exe-and-dxcompiler.dll
	bool Compile(Shader* pShader)
	{
		GFXE_ASSERT(pShader != nullptr);
		GFXE_ASSERT(m_ShaderTypeToTargetStringMap.contains(pShader->m_Type));

		// Compilation arguments.
		std::vector<LPCWSTR> args;

		// Optional shader source file name for error reporting and for PIX shader source view.
		std::wstring fullShaderName = Utility::UTF8ToWideString(pShader->m_NameStr + pShader->m_FormatStr);
		args.push_back(fullShaderName.c_str());

		// Entry point.
		args.push_back(L"-E"); args.push_back(L"main");

		// Target.
		args.push_back(L"-T"); args.push_back(m_ShaderTypeToTargetStringMap.at(pShader->m_Type).c_str());

#if _DEBUG
		// Enable debug information (slim format).
		args.push_back(L"-Zs");

		// Enable debug information (full format).
		// args.push_back(L"-Zi");

		// Disable optimizations
		args.push_back(L"-Od");
#endif

		// Add define to shader.
		// args.push_back(L"-D"); args.push_back(L"MY_DEFINE_0=0"); 
		// args.push_back(L"-D"); args.push_back(L"MY_DEFINE_1=1");

		// Output object file.
		std::wstring shaderBinObjectPath = Utility::UTF8ToWideString(m_ShadersOutputPath + pShader->m_NameStr + ".bin");
		args.push_back(L"-Fo"); args.push_back(shaderBinObjectPath.c_str());

		// Output debug information file.
		std::wstring shaderDebugInfoPath = Utility::UTF8ToWideString(m_ShadersOutputPath + pShader->m_NameStr + ".pdb");
		args.push_back(L"-Fd"); args.push_back(shaderDebugInfoPath.c_str());

		// Debug info to separate blob.
		args.push_back(L"-Qstrip_debug");
		// Reflection info to separate blob.
		args.push_back(L"-Qstrip_reflect");

		Microsoft::WRL::ComPtr<IDxcBlobEncoding> pSourceFile = nullptr;
		std::wstring shaderFilePath = Utility::UTF8ToWideString(m_ShadersPath) + fullShaderName;
		if (FAILED(m_pDxUtils->LoadFile(shaderFilePath.c_str(), nullptr, &pSourceFile)))
		{
			GFXE_INFO(Utility::COMPONENTS::_SHADER_, "Loading shader file failed!");

			return false;
		}

		DxcBuffer pSourceFileBuffer;
		pSourceFileBuffer.Ptr = pSourceFile->GetBufferPointer();
		pSourceFileBuffer.Size = pSourceFile->GetBufferSize();
		pSourceFileBuffer.Encoding = DXC_CP_ACP; // Assume BOM says UTF8 or UTF16 or this is ANSI text.

		m_pDxCompiler->Compile(
			&pSourceFileBuffer,
			args.data(),
			(uint32_t)(args.size()),
			m_pDxIncludeHandler.Get(),	// User-provided interface to handle #include directives (optional).
			IID_PPV_ARGS(&pShader->m_ShaderCompilationResult));

		HRESULT compilationStatus;
		pShader->m_ShaderCompilationResult->GetStatus(&compilationStatus);
		if (FAILED(compilationStatus))
		{
			Microsoft::WRL::ComPtr<IDxcBlobUtf8> pErrors = nullptr;
			if (FAILED(pShader->m_ShaderCompilationResult->GetOutput(DXC_OUT_ERRORS, IID_PPV_ARGS(&pErrors), nullptr)))
			{
				GFXE_INFO(Utility::COMPONENTS::_SHADER_, "Shader compilation failed!", "Unable to retrieve error message!");

				return false;
			}

			GFXE_INFO(Utility::COMPONENTS::_SHADER_, "Shader compilation failed!", pErrors->GetStringPointer());

			return false;
		}

		// Get shader binary.
		Microsoft::WRL::ComPtr<IDxcBlobUtf16> pShaderName = nullptr;
		if (FAILED(pShader->m_ShaderCompilationResult->GetOutput(DXC_OUT_OBJECT, IID_PPV_ARGS(&pShader->m_ShaderBinary), &pShaderName)))
		{
			GFXE_INFO(Utility::COMPONENTS::_SHADER_, "Could not obtain shader binary object!");

			return false;
		}

		// Change later
		FILE* fpShaderBinary = NULL;
		if (_wfopen_s(&fpShaderBinary, pShaderName->GetStringPointer(), L"wb") != 0)
		{
			GFXE_INFO(Utility::COMPONENTS::_SHADER_, "Could not open/create shader binary file!");

			return false;
		}
		fwrite(pShader->m_ShaderBinary->GetBufferPointer(), pShader->m_ShaderBinary->GetBufferSize(), 1, fpShaderBinary);
		fclose(fpShaderBinary);

#if _DEBUG
		// Get shader pdb.
		Microsoft::WRL::ComPtr<IDxcBlob> pShaderPdb = nullptr;
		Microsoft::WRL::ComPtr<IDxcBlobUtf16> pShaderPdbName = nullptr;
		if (FAILED(pShader->m_ShaderCompilationResult->GetOutput(DXC_OUT_PDB, IID_PPV_ARGS(&pShaderPdb), &pShaderPdbName)))
		{
			GFXE_INFO(Utility::COMPONENTS::_SHADER_, "Could not obtain shader debug info!");

			return false;
		}

		// Change later
		FILE* fpShaderDebugInfo = NULL;
		if (_wfopen_s(&fpShaderDebugInfo, pShaderPdbName->GetStringPointer(), L"wb") != 0)
		{
			GFXE_INFO(Utility::COMPONENTS::_SHADER_, "Could not open/create shader pdb file!");

			return false;
		}
		fwrite(pShaderPdb->GetBufferPointer(), pShaderPdb->GetBufferSize(), 1, fpShaderDebugInfo);
		fclose(fpShaderDebugInfo);
#endif

		// Get shader hash.
		Microsoft::WRL::ComPtr<IDxcBlob> pHash = nullptr;
		if (FAILED(pShader->m_ShaderCompilationResult->GetOutput(DXC_OUT_SHADER_HASH, IID_PPV_ARGS(&pHash), nullptr)))
		{
			GFXE_INFO(Utility::COMPONENTS::_SHADER_, "Could not obtain shader hash!");

			return false;
		}

		DxcShaderHash* pDxHashBuffer = (DxcShaderHash*)pHash->GetBufferPointer();
		uint64_t* pHashDataPtr = reinterpret_cast<uint64_t*>(pDxHashBuffer->HashDigest);

		pShader->m_Hash0 = pHashDataPtr[0];
		pShader->m_Hash1 = pHashDataPtr[1];

		return true;
		}

private:
	bool m_IsInitialized = false;

	Microsoft::WRL::ComPtr<IDxcUtils> m_pDxUtils;
	Microsoft::WRL::ComPtr<IDxcCompiler3> m_pDxCompiler;
	Microsoft::WRL::ComPtr<IDxcIncludeHandler> m_pDxIncludeHandler;

	std::unordered_map<SHADER_TYPE, std::wstring> m_ShaderTypeToTargetStringMap;

	// For now we only need that for shaders. There should be a better way if there are more use cases.
	//const std::string m_SourcesPath = "C:\\dev\\rendering\\dx12-graphics-engine\\";
	const std::string m_SourcesPath = "C:\\Dev\\dx12-graphics-engine\\";
	const std::string m_ShadersPath = m_SourcesPath + "Shaders\\";
	const std::string m_ShadersOutputPath = m_SourcesPath + "Build\\Shaders\\";
};