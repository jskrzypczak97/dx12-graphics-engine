#pragma once

#include <wrl.h>
#include <atlbase.h>        
#include <dxcapi.h>
#include <d3d12.h>

#include <stdint.h>
#include <string>

class ShaderCompiler;

enum class SHADER_TYPE : uint8_t
{
	VS = 0,
	PS = 1,
	CS = 2
};

class Shader
{
	friend class ShaderCompiler;
private:
	static ShaderCompiler sm_ShaderCompiler;
public:
	Shader(std::string fullName, SHADER_TYPE type);
	~Shader() = default;
	Shader(const Shader&) = delete;
	Shader& operator=(Shader const&) = delete;

	bool Compile();
	inline D3D12_SHADER_BYTECODE GetDxBytecode()
	{
		return m_IsCompiled ?
			D3D12_SHADER_BYTECODE{ m_ShaderBinary->GetBufferPointer(), m_ShaderBinary->GetBufferSize() } :
			D3D12_SHADER_BYTECODE{};
	}
private:
	bool m_IsCompiled = false;

	SHADER_TYPE m_Type;

	uint64_t m_Hash0;
	uint64_t m_Hash1;

	std::string m_NameStr;
	std::string m_FormatStr;

	Microsoft::WRL::ComPtr<IDxcResult> m_ShaderCompilationResult; // Maybe we don't need that?
	Microsoft::WRL::ComPtr<IDxcBlob> m_ShaderBinary;
};