#pragma once

#include <d3d12.h>
#include <wrl.h>

// Does it make sense?

struct DeviceHandle
{
	DeviceHandle() = default;
	DeviceHandle(ID3D12Device* pDxDevice)
	{
		pDxDev = pDxDevice;
	}
	~DeviceHandle() = default;
	DeviceHandle(const DeviceHandle&) = default;
	DeviceHandle& operator=(DeviceHandle const&) = default;

	ID3D12Device* pDxDev;
};

struct Device
{
	Device() = default;
	~Device() = default;
	Device(const Device&) = delete;
	Device& operator=(Device const&) = delete;

	inline const DeviceHandle MakeDeviceHandle() { return DeviceHandle{ pDxDev.Get() }; }

	Microsoft::WRL::ComPtr<ID3D12Device> pDxDev;
};