#pragma once

class RenderObject
{
public:
	RenderObject() = default;
	~RenderObject() = default;
	RenderObject(const RenderObject&) = default;
	RenderObject& operator=(const RenderObject&) = default;

	const SceneNodeEntity* m_pParentSceneNode;

	ResourceHandle m_GeometryHandle;
	ResourceHandle m_BaseColorTextureHandle;
	ResourceHandle m_RoughnessMetalnessTextureHandle;
	ResourceHandle m_NormalTextureHandle;
};