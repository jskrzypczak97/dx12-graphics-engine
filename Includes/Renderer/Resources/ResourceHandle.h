#pragma once

#include <stdint.h>

class ResourceManager;

struct ResourceHandle
{
	friend class ResourceManager;
private:
	union {
		struct {
			uint16_t type;
			uint16_t index;
		};
		uint32_t value = 0xffffffff;
	};
public:
	inline void Invalidate() { value = 0xffffffff; }
	inline bool IsValid() const { return value != 0xffffffff; }
};