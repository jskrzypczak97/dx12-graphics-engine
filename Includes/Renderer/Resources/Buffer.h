#pragma once

#include "Renderer/Resources/Resource.h"

class Buffer : public Resource
{
public:
	Buffer() = default;
	Buffer(Resource&& resource)
		: Resource(std::move(resource))
	{}
	~Buffer() = default;
	Buffer(const Buffer&) = default;
	Buffer& operator=(Buffer const&) = default;
};
