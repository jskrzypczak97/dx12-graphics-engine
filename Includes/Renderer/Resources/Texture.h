#pragma once

#include "Renderer/Resources/Resource.h"

struct RenderTargetView
{
	D3D12_CPU_DESCRIPTOR_HANDLE Handle{};
	D3D12_RENDER_TARGET_VIEW_DESC DxDesc;
};

struct DepthStencilView
{
	D3D12_CPU_DESCRIPTOR_HANDLE Handle{};
	D3D12_DEPTH_STENCIL_VIEW_DESC DxDesc;
};

struct ShaderResourceView
{
	D3D12_CPU_DESCRIPTOR_HANDLE Handle{};
	D3D12_SHADER_RESOURCE_VIEW_DESC DxDesc;
};

class Texture : public Resource
{
public:
	Texture() = default;
	~Texture() = default;
	Texture(const Texture&) = default;
	Texture& operator=(Texture const&) = default;

	void SetRenderTargetView(D3D12_CPU_DESCRIPTOR_HANDLE viewHandle, D3D12_RENDER_TARGET_VIEW_DESC rtDxDesc)
	{
		m_RenderTargetView = { viewHandle, rtDxDesc };
	}
	void SetDepthStencilView(D3D12_CPU_DESCRIPTOR_HANDLE viewHandle, D3D12_DEPTH_STENCIL_VIEW_DESC dsDxDesc)
	{
		m_DepthStencilView = { viewHandle, dsDxDesc };
	}

	inline void SetSrvBindingIndex(uint32_t globalBindingIndex) { m_SrvBindingIndex = globalBindingIndex; }

	const RenderTargetView& GetRenderTargetView() { return m_RenderTargetView; }
	D3D12_CPU_DESCRIPTOR_HANDLE GetRenderTargetViewHandle() { return m_RenderTargetView.Handle; };

	const DepthStencilView& GetDepthStencilView() { return m_DepthStencilView; }
	D3D12_CPU_DESCRIPTOR_HANDLE GetDepthStencilViewHandle() { return m_DepthStencilView.Handle; };

	inline uint32_t GetSrvBindingIndex() { return m_SrvBindingIndex; }

private:
	RenderTargetView m_RenderTargetView;
	DepthStencilView m_DepthStencilView;
	uint32_t m_SrvBindingIndex;
};
