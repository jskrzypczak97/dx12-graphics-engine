#pragma once

#include "Renderer/Resources/Buffer.h"

class Geometry
{
public:
	Geometry() = default;
	~Geometry() = default;
	Geometry(const Geometry&) = default;
	Geometry& operator=(const Geometry&) = default;
	
	Buffer m_VertexBuffer;
	D3D12_VERTEX_BUFFER_VIEW m_VertexBufferView;

	Buffer m_IndexBuffer;
	D3D12_INDEX_BUFFER_VIEW m_IndexBufferView;
	uint32_t m_IndicesCount;
};