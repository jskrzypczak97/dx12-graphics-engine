#pragma once

#include <d3d12.h>
#include "directx/d3dx12.h"

#include <string>
#include <unordered_map>
#include <ranges>

#include "Engine/EngineConfig.h"
#include "Renderer/Resources/Texture.h"
#include "Renderer/Resources/Buffer.h"
#include "Renderer/Resources/Geometry.h"
#include "Renderer/Resources/ResourceHandle.h"

#include "Renderer/Context/CommandContext.h"

#include "Scene/SceneModel/Mesh.h"
#include "Scene/SceneModel/Material.h"
#include "Renderer/Misc.h"

#include "Renderer/Device.h"
#include "Utilities/Utility.h"
#include <DirectXTex.h>

enum ResourceType : uint16_t
{
	BUFFER = 0,
	TEXTURE = 1,
	GEOMETRY = 2,
	RESOURCE_TYPE_COUNT = 3
};

class ResourceManager
{
public:
	ResourceManager(const DeviceHandle& deviceHandle)
		: m_DeviceHandle(deviceHandle)
	{
		m_TextureSet.reserve(2048);
		m_BufferSet.reserve(2048);
		m_GeometrySet.reserve(2048);
	}
	~ResourceManager() = default;
	ResourceManager(const ResourceManager&) = delete;
	ResourceManager& operator=(const ResourceManager&) = delete;

	void ShutDown()
	{
		GFXE_DBG_INFO(Utility::COMPONENTS::_RENDERER_, "Shutting down Resource Manager!");
	}

	void BeginOperation();
	void EndOperation();

	void CopyDataToGpu(Resource& destinationBuffer, void* pData, uint32_t dataSize)
	{
		GFXE_ASSERT(m_pCommandContext, "Command context is not set!");

		m_IntermediateBuffers.emplace_back();
		CreateUploadBuffer(m_IntermediateBuffers.back(), dataSize);

		D3D12_SUBRESOURCE_DATA resourceData = {};
		resourceData.pData = reinterpret_cast<uint8_t*>(pData);
		resourceData.RowPitch = dataSize;
		resourceData.SlicePitch = dataSize;

		// TODO: Don't use hit wrapper function
		UpdateSubresources(m_pCommandContext->GetDxCommandList(), destinationBuffer.GetDxResourcePtr(), m_IntermediateBuffers.back().GetDxResourcePtr(), 0, 0, 1, &resourceData);
	}

	void CopyDataToGpu(Resource& destinationBuffer, const std::vector<D3D12_SUBRESOURCE_DATA>& resourceData)
	{
		GFXE_ASSERT(m_pCommandContext, "Command context is not set!");

		m_IntermediateBuffers.emplace_back();
		CreateUploadBuffer(m_IntermediateBuffers.back(), (uint32_t)GetRequiredIntermediateSize(destinationBuffer.GetDxResourcePtr(), 0, (uint32_t)resourceData.size()));

		// TODO: Don't use hit wrapper function
		UpdateSubresources(m_pCommandContext->GetDxCommandList(), destinationBuffer.GetDxResourcePtr(), m_IntermediateBuffers.back().GetDxResourcePtr(), 0, 0, (uint32_t)resourceData.size(), resourceData.data());
	}

	// CPU and GPU access buffer
	void CreateUploadResource(
		Resource& resource,
		const D3D12_RESOURCE_DESC& resourceDesc,
		D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE
	);

	void CreateReadbackResource(
		Resource& resource,
		const D3D12_RESOURCE_DESC& resourceDesc,
		D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE
	);

	// GPU access buffer
	void CreateBuffer(
		Buffer& buffer,
		uint32_t size,
		D3D12_RESOURCE_STATES initState = D3D12_RESOURCE_STATE_GENERIC_READ,
		D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE
	);

	// CPU and GPU access buffer
	void CreateUploadBuffer(
		Buffer& buffer,
		uint32_t size,
		D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE
	);

	void CreateReadbackBuffer(
		Buffer& buffer,
		uint32_t size,
		D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE
	);

	void CreateTexture(
		Texture& surface,
		uint32_t width,
		uint32_t height,
		DXGI_FORMAT format,
		D3D12_RESOURCE_STATES initState = D3D12_RESOURCE_STATE_COMMON,
		D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE,
		uint16_t arraySize = 1,
		uint16_t mipLevels = 1,
		uint32_t sampleCount = 1,
		uint32_t sampleQuality = 0
	);

	void CreateTexture(
		Texture& surface,
		const D3D12_RESOURCE_DESC& resourceDesc,
		D3D12_RESOURCE_STATES initState
	);

	template<ResourceType type>
	bool IsRegistered(uint64_t resourceHash) const
	{
		GFXE_ASSERT(type < ResourceType::RESOURCE_TYPE_COUNT, "Unknown resource type!");

		return m_ResourceHandles[type].contains(resourceHash);
	}

	template<ResourceType type>
	ResourceHandle GetResourceHandle(uint64_t resourceHash) const
	{
		GFXE_ASSERT(type < ResourceType::RESOURCE_TYPE_COUNT, "Unknown resource type!");

		return m_ResourceHandles[type].at(resourceHash);
	}

	template<typename ResourceT>
	ResourceT* GetResource(ResourceHandle resourceHandle)
	{
		GFXE_ASSERT(false, "Unknown resource type!");

		return nullptr;
	}

	template<>
	Buffer* GetResource(ResourceHandle resourceHandle)
	{
		GFXE_ASSERT(resourceHandle.type == ResourceType::BUFFER, "Resource type mismatch!");

		return &m_BufferSet[resourceHandle.index];
	}

	template<>
	Texture* GetResource(ResourceHandle resourceHandle)
	{
		GFXE_ASSERT(resourceHandle.type == ResourceType::TEXTURE, "Resource type mismatch!");

		return &m_TextureSet[resourceHandle.index];
	}

	template<>
	Geometry* GetResource(ResourceHandle resourceHandle)
	{
		GFXE_ASSERT(resourceHandle.type == ResourceType::GEOMETRY, "Resource type mismatch!");

		return &m_GeometrySet[resourceHandle.index];
	}

	ResourceHandle RegisterGeometryResource(const Mesh* pMesh);

	// Loading should take place somewhere else?
	ResourceHandle RegisterTextureResource(const TextureData* pTextureData);

private:
	void TransitionResource(Resource& resource, D3D12_RESOURCE_STATES newState)
	{
		GFXE_ASSERT(m_pCommandContext, "Command context is not set!");

		if (resource.GetCurrentState() != newState)
		{
			auto transition = CD3DX12_RESOURCE_BARRIER::Transition(resource.GetDxResourcePtr(), resource.GetCurrentState(), newState);
			m_pCommandContext->GetDxCommandList()->ResourceBarrier(1, &transition);
			resource.SetCurrentState(newState);
		}
	}

private:
	DeviceHandle m_DeviceHandle;

	std::vector<Buffer> m_BufferSet;
	std::vector<Texture> m_TextureSet;
	std::vector<Geometry> m_GeometrySet;

	// Resource hash to resource handle
	std::array<std::unordered_map<uint64_t, ResourceHandle>, RESOURCE_TYPE_COUNT> m_ResourceHandles;

private:
	CommandContext* m_pCommandContext; // Private use command context. TODO: Make it copy context, not graphics context.
	std::vector<Buffer> m_IntermediateBuffers;
private:
	ResourceHandle m_DefaultTextureHandle;
};
