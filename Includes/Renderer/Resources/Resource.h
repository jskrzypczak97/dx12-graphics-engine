#pragma once

#include <d3d12.h>
#include <wrl.h>

#include <string>

#include "Utilities/Utility.h"

class Resource
{
public:
	Resource() = default;
	~Resource() = default;
	Resource(const Resource&) = default;
	Resource& operator=(Resource const&) = default;
	Resource(Resource&&) noexcept = default;
	Resource& operator=(Resource&&) noexcept = default;

	inline void SetName(const std::string& resourceName)
	{
		m_DxResource->SetName(Utility::UTF8ToWideString(resourceName).c_str());
	}

	inline D3D12_RESOURCE_STATES GetCurrentState() const { return m_CurrentState; }
	inline void SetCurrentState(D3D12_RESOURCE_STATES newState) { m_CurrentState = newState; }

	inline ID3D12Resource* GetDxResourcePtr() { return m_DxResource.Get(); }
	inline ID3D12Resource** GetDxResourcePtrAddr() { return m_DxResource.GetAddressOf(); }

	inline void SetResourceDescription(D3D12_RESOURCE_DESC resourceDesc) { m_ResourceDesc = resourceDesc; }
	inline const D3D12_RESOURCE_DESC& GetResourceDescription() const { return m_ResourceDesc; }

protected:
	D3D12_RESOURCE_DESC m_ResourceDesc;
	D3D12_RESOURCE_STATES m_CurrentState = D3D12_RESOURCE_STATE_COMMON;
	D3D12_RESOURCE_STATES m_TransitionState = D3D12_RESOURCE_STATE_COMMON;
	Microsoft::WRL::ComPtr<ID3D12Resource> m_DxResource;
};