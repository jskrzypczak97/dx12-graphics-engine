#pragma once

#include <array>

#include "Renderer/Device.h"

#include "Renderer/DescriptorHeaps/DescriptorHeapRange.h"

#include "Renderer/ConstantBuffers/DynamicConstantBufferPool.h"

using ContextIdType = uint64_t;
using FenceValueType = uint64_t;

enum class ConstantBufferSize : uint16_t
{
	SIZE_256 = 0,
	SIZE_512 = 1,
	SIZE_1024 = 2,
	SIZES_NUM,
};

class ConstantBufferPoolManager
{
public:
	ConstantBufferPoolManager(const DeviceHandle& deviceHandle, DescriptorHeapRange* constantBufferDescriptorHeap)
	{
		m_pDescriptorHeapRange = constantBufferDescriptorHeap;
		m_DynamicPools = {
			DynamicConstantBufferPool(256, GFXE_CONFIG_CBV_256_COUNT, m_pDescriptorHeapRange->ReserveSubrange("DYNAMIC_CB_256_RANGE", GFXE_CONFIG_CBV_256_COUNT), deviceHandle),
			DynamicConstantBufferPool(512, GFXE_CONFIG_CBV_512_COUNT, m_pDescriptorHeapRange->ReserveSubrange("DYNAMIC_CB_512_RANGE", GFXE_CONFIG_CBV_512_COUNT), deviceHandle),
			DynamicConstantBufferPool(1024, GFXE_CONFIG_CBV_1024_COUNT, m_pDescriptorHeapRange->ReserveSubrange("DYNAMIC_CB_1024_RANGE", GFXE_CONFIG_CBV_1024_COUNT), deviceHandle)
		};
	}
	~ConstantBufferPoolManager() = default;
	ConstantBufferPoolManager(const ConstantBufferPoolManager&) = delete;
	ConstantBufferPoolManager& operator=(ConstantBufferPoolManager const&) = delete;

	void ShutDown();

	ConstantBufferSlot* RequestDynamicSlot(ContextIdType contextId, uint32_t bufferSize);
	void EndDynamicSlotsUsage(ContextIdType contextId, FenceValueType fenceValue);
	void TryReleaseDynamicSlots(FenceValueType fenceValue);

private:
	std::array<DynamicConstantBufferPool, (size_t)(ConstantBufferSize::SIZES_NUM)> m_DynamicPools;
	DescriptorHeapRange* m_pDescriptorHeapRange = nullptr;
};