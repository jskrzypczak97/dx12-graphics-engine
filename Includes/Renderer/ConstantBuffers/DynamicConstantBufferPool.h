#pragma once

#include <stdint.h>
#include <numeric>

#include <vector>
#include <unordered_map>

#include <deque>
#include <queue>

#include "Renderer/Resources/Buffer.h"

#include "Renderer/DescriptorHeaps/DescriptorHeapRange.h"

#include "Renderer/Device.h"
#include "Utilities/Utility.h"

#define CONSTANT_BUFFER_ALIGNMENT 256

using ContextIdType = uint64_t;
using FenceValueType = uint64_t;

// data needed to use cb
struct ConstantBufferSlot
{
	uint32_t Index;
	uint8_t* CpuAddress;
	uint64_t GpuAddress;
};

// We get const buffers for specific context so when we start recording the context we signal to the pool that it has to service this context
// Each buffer obtained with getbuffer() will be associated with specific context within which it is used in order to track the usage.
// When context is done pool needs to be notified so it can know which buffers are in use and what is the fence value. Then we can feed the pool
// with the current fence value so it can check it there are some buffers that are no longer used and can be freed
class DynamicConstantBufferPool
{
	friend class ConstantBufferPoolManager;
private:
	static std::atomic<uint32_t> sm_PoolId;
public:
	DynamicConstantBufferPool() = default;
	DynamicConstantBufferPool(uint32_t slotSize, uint32_t capacity, DescriptorHeapRange* pDescriptorHeapRange, DeviceHandle deviceHandle);
	~DynamicConstantBufferPool() = default;
	DynamicConstantBufferPool(const DynamicConstantBufferPool&) = default;
	DynamicConstantBufferPool& operator=(DynamicConstantBufferPool const&) = default;
	DynamicConstantBufferPool(DynamicConstantBufferPool&&) noexcept = default;
	DynamicConstantBufferPool& operator=(DynamicConstantBufferPool&&) noexcept = default;

private:
	void ContextServiceStart(ContextIdType contextId);
	void ContextServiceEnd(ContextIdType contextId, FenceValueType fenceValue);
	void AllContextsServiceEnd();

	void TryReleaseSlots(FenceValueType fenceValue);

	ConstantBufferSlot* GetBufferSlot(ContextIdType contextId);

	uint32_t m_SlotSize;
	uint32_t m_Capacity;

	Buffer m_Buffer;
	DescriptorHeapRange* m_pDecriptorHeapRange;

	std::vector<ConstantBufferSlot> m_ConstantBufferSlots;
	std::deque<uint32_t> m_ConstantBufferSlotsQueue;

	// Context ids and indices of the slots that these contexts use.
	std::unordered_map<ContextIdType, std::vector<uint32_t>> m_CurrentSlotUsage;
	// Queue of fence values and indices of the buffers that can be freed once proper fence value is reported.
	std::queue<std::pair<FenceValueType, std::vector<uint32_t>>> m_SlotsToFree;
};