#pragma once

#include <DirectXMath.h>

struct Vertex
{
	Vertex()
		: Position(0.0f, 0.0f, 0.0f), Normal(0.0f, 0.0f, 0.0f), Tangent(0.0f, 0.0f, 0.0f), Bitangent(0.0f, 0.0f, 0.0f), TexCoords0(0.0f, 0.0f) {}
	Vertex(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 normal, DirectX::XMFLOAT3 tangent, DirectX::XMFLOAT3 bitangent, DirectX::XMFLOAT2 texCoords0)
		: Position(position), Normal(normal), Tangent(tangent), Bitangent(bitangent), TexCoords0(texCoords0) {}

	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Normal;
	DirectX::XMFLOAT3 Tangent;
	DirectX::XMFLOAT3 Bitangent;
	DirectX::XMFLOAT2 TexCoords0;
};

#define BYTES_IN_DWORD 4

struct VERTEX_TRANSFORM_MATRICES_LIT
{
	DirectX::XMFLOAT4X4 mfWVP; // World view projection matrix.
	DirectX::XMFLOAT4X4 mfWorld; // World matrix.
};

struct LIGHT_DATA_LIT
{
	DirectX::XMFLOAT3 vfWsPosition;
	float_t fIntensity;

	DirectX::XMFLOAT3 vfWsDirection;
	uint32_t uiLightType;

	DirectX::XMFLOAT3 vfBaseColor;
	uint32_t uiFlags;
};

struct LIGHT_DATA_ARRAY_LIT
{
	LIGHT_DATA_LIT data[LIGHT_COUNT_LIMIT];
};

struct CAMERA_DATA_LIT
{
	DirectX::XMFLOAT3 vfWsPosition;
	uint8_t _pad0[1 * BYTES_IN_DWORD];
};

struct MATERIAL_DATA_LIT
{
	uint32_t uBaseColorTexIdx;
	DirectX::XMFLOAT3 vfBaseColor;

	uint32_t uRoughnessMetalnessTexIdx;
	float_t fRoughness;
	float_t fMetalness;
	uint32_t uNormalTexIdx;
};