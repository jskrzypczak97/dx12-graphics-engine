#pragma once

#include <queue>
#include <ranges>
#include <algorithm>

#include "Renderer/Resources/Buffer.h"
#include "Renderer/Context/CommandContext.h"

enum ProfileGpuStages
{
	FRAME = 0u,
	MAIN_PASS,
	GUI_PASS,
	STAGES_COUNT
};

class Profiler
{
public:
	static constexpr uint32_t s_SlotsCount = 16;
	static constexpr uint32_t s_TimestampSlotSize = 16; // Up to 16 measurements per slot (each measurement has begin and end timestamp).
public:
	Profiler() = default;
	~Profiler() = default;
	Profiler(const Profiler&) = default;
	Profiler& operator=(Profiler const&) = default;

	void Initialize(const DeviceHandle& deviceHandle);

	uint32_t GetTimestampSlot();
	void MarkTimestampBegin(CommandContext* pContext, const uint32_t slotId, const uint32_t measurementId);
	void MarkTimestampEnd(CommandContext* pContext, const uint32_t slotId, const uint32_t measurementId);

	void EndTimestampSlotUsage(CommandContext* pContext, const uint32_t slotId);

	std::array<double_t, s_TimestampSlotSize> FetchTimestampSlotData(const uint32_t slotId);
private:
	void MarkTimestamp(CommandContext* pContext, const uint32_t queryIndex);

	Buffer m_TimestampQueryResults;
	Microsoft::WRL::ComPtr<ID3D12QueryHeap> m_TimestampQueryHeap;

	std::queue<uint32_t> m_TimestampSlotsQueue;
	std::array<uint32_t, s_SlotsCount> m_TimestampSlotsMeasurementsCount;
	std::array<uint64_t, s_SlotsCount> m_PendingTimestampSlotsFenceValues;
};