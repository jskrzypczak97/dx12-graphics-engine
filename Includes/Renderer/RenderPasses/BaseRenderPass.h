#pragma once

#include <unordered_set>

#include <d3d12.h>

#include "Utilities/Utility.h"

class BaseRenderPass
{
	// Statics
public:
	static void UpdateDisplayDims(const D3D12_VIEWPORT& newViewport, const D3D12_RECT& newScissorRect)
	{
		sm_CurrentDisplayViewport = newViewport;
		sm_CurrentDisplayScissorRect = newScissorRect;
	}
protected:
	static D3D12_VIEWPORT sm_CurrentDisplayViewport;
	static D3D12_RECT sm_CurrentDisplayScissorRect;

public:
	virtual void RegisterObjectToPass(uint64_t objectId)
	{
		GFXE_ASSERT(!m_RegisteredObjects.contains(objectId), "Given object was already assigned!");

		m_RegisteredObjects.insert(objectId);
	}

	virtual void AssignObjectToFrame(uint64_t objectId)
	{
		GFXE_ASSERT(!m_AssignedObjects.contains(objectId), "Given object was already assigned!");

		m_AssignedObjects.insert(objectId);
	}

	inline bool AnyObjectsAssigned() const { return m_AssignedObjects.size() > 0; }
	inline bool AnyObjectsRegistered() const { return m_RegisteredObjects.size() > 0; }

	inline void ClearAssignedObjects() { m_AssignedObjects.clear(); }

protected:
	std::unordered_set<uint64_t> m_RegisteredObjects;
	std::unordered_set<uint64_t> m_AssignedObjects;
};