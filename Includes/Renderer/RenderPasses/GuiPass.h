#pragma once

#include "Renderer/Context/CommandContext.h"
#include "Renderer/Resources/Texture.h"
#include "Renderer/RenderPasses/BaseRenderPass.h"

class GuiPass : public BaseRenderPass
{
public:
	void Initialize(const DeviceHandle& deviceHandle);
	void RecordContext(CommandContext* pCommandContext, Texture* pOutputRt, Texture* pOutputDs);
};