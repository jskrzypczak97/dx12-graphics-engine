#pragma once

#include <wrl.h>
#include <d3d12.h>

#include "Renderer/Context/CommandContext.h"
#include "Renderer/Resources/Texture.h"
#include "Renderer/RenderEntities/RenderObject.h"

#include "Renderer/RenderPasses/BaseRenderPass.h"

class Renderer;

class MainPass : public BaseRenderPass
{
private:
	struct PF_RESOURCES
	{
		ConstantBufferSlot* pLightDataCB;
		ConstantBufferSlot* pCameraDataCB;
	};

	struct PO_RESOURCES
	{
		ConstantBufferSlot* pVsVertexTransformMatricesCB;
		ConstantBufferSlot* pPsMaterialPropertiesCB;
	};

public:
	MainPass() = default;
	~MainPass() = default;
	MainPass(const MainPass&) = delete;
	MainPass& operator=(MainPass const&) = delete;

	void Initialize(const DeviceHandle& deviceHandle);
	void RecordContext(
		CommandContext* pCommandContext,
		std::unordered_map<uint64_t, RenderObject>& renderObjects,
		Texture* pOutputRt,
		Texture* pOutputDs);
	void UpdateData(uint64_t contextId, Scene* pScene, Camera* pCamera, Renderer* pRenderer);
	void RegisterObjectToPass(uint64_t objectId) override;

private:
	void DefinePipelineStateObject(const DeviceHandle& deviceHandle);
	void DefineRootSignature(const DeviceHandle& deviceHandle);

	DeviceHandle m_DeviceHanlde;

	PF_RESOURCES m_PerFrameResources;
	std::unordered_map<uint64_t, PO_RESOURCES> m_PerObjectResources;

	Microsoft::WRL::ComPtr<ID3D12RootSignature> m_pRootSignature;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> m_pPipelineState;
};