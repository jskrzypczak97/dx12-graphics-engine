
#include "Engine/EngineConfig.h"
#include "Utilities/Utility.h"

#include "Engine/EngineWindow.h"
#include "Engine/GraphicsEngine.h"

using namespace Utility;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	GFXE_INFO(COMPONENTS::_ENGINE_, "Starting!");

#ifdef WAIT_FOR_DEBUGGER
	GFXE_INFO(COMPONENTS::_ENGINE_, "Waiting for debugger!");

	while (!::IsDebuggerPresent())
	{
		::Sleep(100);
	}

	GFXE_INFO(COMPONENTS::_ENGINE_, "Debugger attached!");
#endif

	constexpr uint32_t windowWidth = GFXE_CONFIG_WINDOW_WIDTH;
	constexpr uint32_t windowHeight = GFXE_CONFIG_WINDOW_HEIGHT;
	const std::wstring windowName = L"Graphics Engine";
	const std::wstring windowTitle = L"DX12 Graphics Engine";

	int32_t returnCode = 0;
	{
		GraphicsEngine Engine;
		if (!Engine.Initialize(hInstance, nCmdShow, windowWidth, windowHeight, windowName, windowTitle))
		{
			GFXE_ERROR("Engine could not be initialized correctly!");

			return 0;
		}

		returnCode = Engine.Run();

		GFXE_INFO(COMPONENTS::_ENGINE_, "Exiting!");
	}

	return returnCode;
}