
#include "Renderer/RenderPasses/GuiPass.h"
#include "Renderer/Renderer.h"
#include <imgui/imgui_impl_dx12.h>

void GuiPass::Initialize(const DeviceHandle& deviceHandle)
{
	auto imguiDescriptorRange = Renderer::sm_DescriptorHeapManager->GetDescriptorHeapRange(DescriptorHeapType::CBV_SRV_UAV, "IMGUI");

	if (!ImGui_ImplDX12_Init(deviceHandle.pDxDev,
		GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		Renderer::sm_DescriptorHeapManager->GetDescriptorHeap(DescriptorHeapType::CBV_SRV_UAV)->GetDxDescriptorHeap(),
		imguiDescriptorRange->GetCpuDescriptorHandleLocal(),
		imguiDescriptorRange->GetGpuDescriptorHandleLocal()))
	{
		GFXE_ERROR("Failed to initialize ImGui DX12!");
	}
}

void GuiPass::RecordContext(CommandContext* pCommandContext, Texture* pOutputRt, Texture* pOutputDs)
{
	ID3D12GraphicsCommandList* pCommandList = pCommandContext->GetDxCommandList();

	{
		// should be handled automatically somehow
		std::vector<D3D12_RESOURCE_BARRIER> barriers;
		if (pOutputRt->GetCurrentState() != D3D12_RESOURCE_STATE_RENDER_TARGET) barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pOutputRt->GetDxResourcePtr(), pOutputRt->GetCurrentState(), D3D12_RESOURCE_STATE_RENDER_TARGET));
		if (pOutputDs->GetCurrentState() != D3D12_RESOURCE_STATE_DEPTH_WRITE) barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pOutputDs->GetDxResourcePtr(), pOutputDs->GetCurrentState(), D3D12_RESOURCE_STATE_DEPTH_WRITE));

		if (barriers.size() > 0)
		{
			pCommandList->ResourceBarrier((uint32_t)(barriers.size()), barriers.data());

			pOutputRt->SetCurrentState(D3D12_RESOURCE_STATE_RENDER_TARGET);
			pOutputDs->SetCurrentState(D3D12_RESOURCE_STATE_DEPTH_WRITE);
		}
	}

	D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = pOutputRt->GetRenderTargetViewHandle();
	D3D12_CPU_DESCRIPTOR_HANDLE dsvHandle = pOutputDs->GetDepthStencilViewHandle();

	pCommandList->OMSetRenderTargets(1, &rtvHandle, FALSE, &dsvHandle);
 
	pCommandList->RSSetViewports(1, &sm_CurrentDisplayViewport);
	pCommandList->RSSetScissorRects(1, &sm_CurrentDisplayScissorRect);

	pCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pCommandList->SetDescriptorHeaps(1, Renderer::sm_DescriptorHeapManager->GetDescriptorHeap(DescriptorHeapType::CBV_SRV_UAV)->GetDxDescriptorHeapAddress());

	ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), pCommandList);
}
