
#include "Renderer/Renderer.h"
#include "Renderer/RenderPasses/MainPass.h"

#include "Renderer/Shader/Shader.h"
#include "Renderer/Misc.h"

#include "Renderer/Lights/LightDefines.h"

#include "Scene/SceneModel/SceneModel.h"

namespace WRL = Microsoft::WRL;

void MainPass::Initialize(const DeviceHandle& deviceHandle)
{
	m_DeviceHanlde = deviceHandle;

	DefineRootSignature(deviceHandle);
	DefinePipelineStateObject(deviceHandle);
}

void MainPass::RecordContext(CommandContext* pCommandContext, std::unordered_map<uint64_t, RenderObject>& renderObjects, Texture* pOutputRt, Texture* pOutputDs)
{
	ID3D12GraphicsCommandList* pCommandList = pCommandContext->GetDxCommandList();

	{
		// should be handled automatically somehow
		std::vector<D3D12_RESOURCE_BARRIER> barriers;
		if (pOutputRt->GetCurrentState() != D3D12_RESOURCE_STATE_RENDER_TARGET) barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pOutputRt->GetDxResourcePtr(), pOutputRt->GetCurrentState(), D3D12_RESOURCE_STATE_RENDER_TARGET));
		if (pOutputDs->GetCurrentState() != D3D12_RESOURCE_STATE_DEPTH_WRITE) barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pOutputDs->GetDxResourcePtr(), pOutputDs->GetCurrentState(), D3D12_RESOURCE_STATE_DEPTH_WRITE));

		if (barriers.size() > 0)
		{
			pCommandList->ResourceBarrier((uint32_t)(barriers.size()), barriers.data());

			pOutputRt->SetCurrentState(D3D12_RESOURCE_STATE_RENDER_TARGET);
			pOutputDs->SetCurrentState(D3D12_RESOURCE_STATE_DEPTH_WRITE);
		}
	}

	D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = pOutputRt->GetRenderTargetViewHandle();
	D3D12_CPU_DESCRIPTOR_HANDLE dsvHandle = pOutputDs->GetDepthStencilViewHandle();

	pCommandList->OMSetRenderTargets(1, &rtvHandle, FALSE, &dsvHandle);

	pCommandList->RSSetViewports(1, &sm_CurrentDisplayViewport);
	pCommandList->RSSetScissorRects(1, &sm_CurrentDisplayScissorRect);

	pCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pCommandList->SetGraphicsRootSignature(m_pRootSignature.Get());
	pCommandList->SetPipelineState(m_pPipelineState.Get());

	auto descHeap = Renderer::sm_DescriptorHeapManager->GetDescriptorHeap(DescriptorHeapType::CBV_SRV_UAV);
	pCommandList->SetDescriptorHeaps(1, descHeap->GetDxDescriptorHeapAddress());

	// Pointing GPU to correct constant buffer locations 
	pCommandList->SetGraphicsRootConstantBufferView(0, m_PerFrameResources.pLightDataCB->GpuAddress);
	pCommandList->SetGraphicsRootConstantBufferView(1, m_PerFrameResources.pCameraDataCB->GpuAddress);

	for (auto id : m_AssignedObjects)
	{
		RenderObject& renderObject = renderObjects.at(id);

		PO_RESOURCES& perObjectResources = m_PerObjectResources.at(id);

		auto* geometryData = Renderer::sm_ResourceManager->GetResource<Geometry>(renderObject.m_GeometryHandle);

		// There should be 'Context' object that stores info on what is currently bound/set so we can handle that more efficiently. 
		pCommandList->IASetVertexBuffers(0, 1, &geometryData->m_VertexBufferView);
		pCommandList->IASetIndexBuffer(&geometryData->m_IndexBufferView);

		// Pointing GPU to correct constant buffer locations 
		pCommandList->SetGraphicsRootConstantBufferView(2, perObjectResources.pVsVertexTransformMatricesCB->GpuAddress);
		pCommandList->SetGraphicsRootConstantBufferView(3, perObjectResources.pPsMaterialPropertiesCB->GpuAddress);

		auto indexCount = geometryData->m_IndicesCount;
		pCommandList->DrawIndexedInstanced(indexCount, 1, 0, 0, 0);
	}
}

void MainPass::RegisterObjectToPass(uint64_t objectId)
{
	BaseRenderPass::RegisterObjectToPass(objectId);

	m_PerObjectResources.insert(std::make_pair(objectId, PO_RESOURCES()));
}

void MainPass::DefinePipelineStateObject(const DeviceHandle& deviceHandle)
{
	D3D12_INPUT_ELEMENT_DESC inputLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "BITANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 48, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	};

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc = {};
	inputLayoutDesc.NumElements = sizeof(inputLayout) / sizeof(D3D12_INPUT_ELEMENT_DESC);
	inputLayoutDesc.pInputElementDescs = inputLayout;

	Shader vertexShader("Lit_VS.hlsl", SHADER_TYPE::VS);
	vertexShader.Compile();

	Shader pixelShader("Lit_PS.hlsl", SHADER_TYPE::PS);
	pixelShader.Compile();

	D3D12_RASTERIZER_DESC rasterDesc = {};
	rasterDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterDesc.CullMode = D3D12_CULL_MODE_NONE;
	rasterDesc.FrontCounterClockwise = FALSE;
	rasterDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
	rasterDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
	rasterDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
	rasterDesc.DepthClipEnable = TRUE;
	rasterDesc.MultisampleEnable = FALSE;
	rasterDesc.AntialiasedLineEnable = FALSE;
	rasterDesc.ForcedSampleCount = 0;
	rasterDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = inputLayoutDesc;
	psoDesc.pRootSignature = m_pRootSignature.Get();
	psoDesc.VS = vertexShader.GetDxBytecode();
	psoDesc.PS = pixelShader.GetDxBytecode();
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
	psoDesc.NumRenderTargets = 1;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.SampleMask = 0xffffffff; // sample mask has to do with multi-sampling. 0xffffffff means point sampling is done
	psoDesc.RasterizerState = rasterDesc;
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);

	if (FAILED(deviceHandle.pDxDev->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(m_pPipelineState.GetAddressOf()))))
	{
		GFXE_ERROR("Failed to create Pipeline State Object");
	}
}

void MainPass::DefineRootSignature(const DeviceHandle& deviceHandle)
{
	D3D12_ROOT_PARAMETER  rootParameters[4];

	// Per frame

	// PS constant buffer | Lights
	rootParameters[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;
	rootParameters[0].Descriptor.RegisterSpace = 0;
	rootParameters[0].Descriptor.ShaderRegister = 0;

	// PS constant buffer | Camera
	rootParameters[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;
	rootParameters[1].Descriptor.RegisterSpace = 0;
	rootParameters[1].Descriptor.ShaderRegister = 1;

	// Per object

	// VS constant buffer | Matrices
	rootParameters[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;
	rootParameters[2].Descriptor.RegisterSpace = 0;
	rootParameters[2].Descriptor.ShaderRegister = 0;

	// PS constant buffer | Material
	rootParameters[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;
	rootParameters[3].Descriptor.RegisterSpace = 0;
	rootParameters[3].Descriptor.ShaderRegister = 2;

	//CD3DX12_STATIC_SAMPLER_DESC staticSampler{ 0, D3D12_FILTER_MIN_MAG_MIP_LINEAR };
	CD3DX12_STATIC_SAMPLER_DESC staticSampler{ 0, D3D12_FILTER_ANISOTROPIC };

	D3D12_ROOT_SIGNATURE_DESC rootDesc;
	rootDesc.NumParameters = _countof(rootParameters);
	rootDesc.pParameters = rootParameters;
	rootDesc.NumStaticSamplers = 1;
	rootDesc.pStaticSamplers = &staticSampler;
	rootDesc.Flags = (
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_CBV_SRV_UAV_HEAP_DIRECTLY_INDEXED
		);

	WRL::ComPtr<ID3DBlob> serializedRootSignature;
	if (FAILED(D3D12SerializeRootSignature(&rootDesc, D3D_ROOT_SIGNATURE_VERSION_1, &serializedRootSignature, nullptr)))
	{
		GFXE_ERROR("Failed to serialize root signature!");
	}

	if (FAILED(deviceHandle.pDxDev->CreateRootSignature(0, serializedRootSignature->GetBufferPointer(), serializedRootSignature->GetBufferSize(), IID_PPV_ARGS(&m_pRootSignature))))
	{
		GFXE_ERROR("Failed to create root signature!");
	}
}

void MainPass::UpdateData(uint64_t contextId, Scene* pScene, Camera* pCamera, Renderer* pRenderer)
{
	using namespace DirectX;

	// PER FRAME

	CAMERA_DATA_LIT cameraData = {};
	ConstantBufferSlot* cameraBufferSlot = m_PerFrameResources.pCameraDataCB = Renderer::sm_ConstantBufferPoolManager->RequestDynamicSlot(contextId, sizeof(CAMERA_DATA_LIT));
	cameraData.vfWsPosition = pCamera->GetPosition();

	memcpy(cameraBufferSlot->CpuAddress, &cameraData, sizeof(CAMERA_DATA_LIT));

	LIGHT_DATA_ARRAY_LIT lightDataArray = {};
	ConstantBufferSlot* lightsBufferSlot = m_PerFrameResources.pLightDataCB = Renderer::sm_ConstantBufferPoolManager->RequestDynamicSlot(contextId, sizeof(LIGHT_DATA_ARRAY_LIT));
	uint32_t index = 0;
	for (auto id : pScene->GetSceneEntitiesIds<SCENE_ENTITY_TYPES::LIGHT>())
	{
		const auto& sceneLight = pScene->GetSceneEntity<SceneLight>(id);

		GFXE_ASSERT(index < LIGHT_COUNT_LIMIT, "Lights limit exceeded!");

		LIGHT_DATA_LIT shaderLightData = {};

		if (sceneLight.IsActive())
		{
			shaderLightData.uiLightType = sceneLight.GetLightType();

			shaderLightData.vfWsPosition = sceneLight.GetTransform().GetTranslation();

			XMVECTOR lightDirection = XMVector3Normalize(XMVector3Transform({ 0.0f, -1.0f, 0.0f }, sceneLight.GetTransform().GetRotationMatrix()));
			XMStoreFloat3(&shaderLightData.vfWsDirection, lightDirection);

			LightAttributes lightAttributes = sceneLight.GetLightAttributes();
			shaderLightData.vfBaseColor = lightAttributes.BaseColor;
			shaderLightData.fIntensity = lightAttributes.Intensity;

			SetBit(shaderLightData.uiFlags, LF_LIGHT_ENABLED);
		}
		else
		{
			ClearBit(shaderLightData.uiFlags, LF_LIGHT_ENABLED);
		}

		lightDataArray.data[index++] = shaderLightData;

	}

	memcpy(lightsBufferSlot->CpuAddress, &lightDataArray, sizeof(LIGHT_DATA_ARRAY_LIT));

	// PER OBJECT

	XMMATRIX viewProjectionMatrix = pCamera->GetViewMatrix() * pCamera->GetProjectionMatrix();

	for (auto id : m_RegisteredObjects)
	{
		const auto& renderObject = pRenderer->m_RenderObjects.at(id);
		const auto* pSceneNode = renderObject.m_pParentSceneNode;

		GFXE_ASSERT(pSceneNode->Type() == SCENE_ENTITY_TYPES::MESH);

		const SceneMesh* pSceneMesh = reinterpret_cast<const SceneMesh*>(pSceneNode);

		if (!pSceneNode->IsEnabled() || !pSceneMesh->IsVisible())
		{
			continue;
		}

		PO_RESOURCES& perObjectResources = m_PerObjectResources.at(id);

		VERTEX_TRANSFORM_MATRICES_LIT vertexTransformationMatrices = {};
		ConstantBufferSlot* vertexTransformationMatricesSlot = Renderer::sm_ConstantBufferPoolManager->RequestDynamicSlot(contextId, sizeof(VERTEX_TRANSFORM_MATRICES_LIT));

		bool dbg = false;
		if (pSceneNode->GetName().contains("curtain_09"))
		{
			dbg = true;
		}

		XMMATRIX mTransformMatrix = pSceneNode->GetTransform().GetTransformMatrix();
		const SceneNodeEntity* pParentNode = pSceneNode->GetParent();
		while (pParentNode)
		{
			mTransformMatrix = mTransformMatrix * pParentNode->GetTransform().GetTransformMatrix();
			pParentNode = pParentNode->GetParent();
		}

		if (dbg)
		{
			DirectX::XMFLOAT4X4 out;
			XMStoreFloat4x4(&out, pSceneNode->GetParent()->GetTransform().GetTransformMatrix());
			int ddfd = 4;
		}

		//Transform::TransposeMatrix(mTransformMatrix);
		//auto test = Transform(Transform::UnpackMatrix(mTransformMatrix));

		XMStoreFloat4x4(&vertexTransformationMatrices.mfWVP, XMMatrixTranspose(mTransformMatrix * viewProjectionMatrix));
		XMStoreFloat4x4(&vertexTransformationMatrices.mfWorld, XMMatrixTranspose(mTransformMatrix));

		memcpy(vertexTransformationMatricesSlot->CpuAddress, &vertexTransformationMatrices, sizeof(VERTEX_TRANSFORM_MATRICES_LIT));

		perObjectResources.pVsVertexTransformMatricesCB = vertexTransformationMatricesSlot;

		MATERIAL_DATA_LIT materialProperties = {};
		ConstantBufferSlot* materialPropertiesSlot = Renderer::sm_ConstantBufferPoolManager->RequestDynamicSlot(contextId, sizeof(MATERIAL_DATA_LIT));

		materialProperties.vfBaseColor = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f);
		materialProperties.fRoughness = 1.0f;
		materialProperties.fMetalness = 0.0f;

		const Material* pMaterial = pSceneMesh->GetMesh()->pMaterial;
		materialProperties.uBaseColorTexIdx = pMaterial->BaseColorTexData.IsDefined() ?
			Renderer::sm_ResourceManager->GetResource<Texture>(renderObject.m_BaseColorTextureHandle)->GetSrvBindingIndex() :
			0xffffffff;
		materialProperties.uRoughnessMetalnessTexIdx = pMaterial->RoughnessMetalnessTexData.IsDefined() ?
			Renderer::sm_ResourceManager->GetResource<Texture>(renderObject.m_RoughnessMetalnessTextureHandle)->GetSrvBindingIndex() :
			0xffffffff;
		materialProperties.uNormalTexIdx = pMaterial->NormalTexData.IsDefined() && pRenderer->GetRenderOptionsRef().EnableNormalMapping ?
			Renderer::sm_ResourceManager->GetResource<Texture>(renderObject.m_NormalTextureHandle)->GetSrvBindingIndex() :
			0xffffffff;

		memcpy(materialPropertiesSlot->CpuAddress, &materialProperties, sizeof(MATERIAL_DATA_LIT));

		perObjectResources.pPsMaterialPropertiesCB = materialPropertiesSlot;

		AssignObjectToFrame(id);
	}
}