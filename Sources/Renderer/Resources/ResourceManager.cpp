
#include "Renderer/Resources/ResourceManager.h"
#include "Renderer/Renderer.h"

void ResourceManager::BeginOperation()
{
	m_pCommandContext = Renderer::sm_CommandContextManager->RequestCommandContext(D3D12_COMMAND_LIST_TYPE_DIRECT);
}

void ResourceManager::EndOperation()
{
	Renderer::sm_CommandContextManager->ExecuteContextAndWait(m_pCommandContext);
	m_IntermediateBuffers.clear();
}

void ResourceManager::CreateUploadResource(
	Resource& resource,
	const D3D12_RESOURCE_DESC& resourceDesc,
	D3D12_RESOURCE_FLAGS flags)
{
	D3D12_RESOURCE_STATES initialState = D3D12_RESOURCE_STATE_GENERIC_READ; // Required for Upload Buffers

	auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	if (FAILED(m_DeviceHandle.pDxDev->CreateCommittedResource(
		&heapProperties,
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		initialState,
		nullptr,
		IID_PPV_ARGS(resource.GetDxResourcePtrAddr()))))
	{
		GFXE_ERROR("Failed to create upload buffer!");
	}

	resource.SetCurrentState(initialState);
}

void ResourceManager::CreateReadbackResource(
	Resource& resource,
	const D3D12_RESOURCE_DESC& resourceDesc,
	D3D12_RESOURCE_FLAGS flags)
{
	D3D12_RESOURCE_STATES initialState = D3D12_RESOURCE_STATE_COPY_DEST; // Required for readback buffers and must not be changed!

	auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_READBACK);
	if (FAILED(m_DeviceHandle.pDxDev->CreateCommittedResource(
		&heapProperties,
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		initialState,
		nullptr,
		IID_PPV_ARGS(resource.GetDxResourcePtrAddr()))))
	{
		GFXE_ERROR("Failed to create upload buffer!");
	}

	resource.SetCurrentState(initialState);
}

void ResourceManager::CreateReadbackBuffer(
	Buffer& buffer,
	uint32_t size,
	D3D12_RESOURCE_FLAGS flags)
{
	D3D12_RESOURCE_DESC resourceDesc = {};
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	resourceDesc.Width = size;
	resourceDesc.Height = 1;
	resourceDesc.Format = DXGI_FORMAT_UNKNOWN;
	resourceDesc.Flags = flags;
	resourceDesc.MipLevels = 1;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.SampleDesc.Count = 1;
	resourceDesc.SampleDesc.Quality = 0;
	resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	resourceDesc.Alignment = 0;

	CreateReadbackResource(buffer, resourceDesc, flags);
}

void ResourceManager::CreateUploadBuffer(
	Buffer& buffer,
	uint32_t size,
	D3D12_RESOURCE_FLAGS flags)
{
	D3D12_RESOURCE_DESC resourceDesc = {};
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	resourceDesc.Width = size;
	resourceDesc.Height = 1;
	resourceDesc.Format = DXGI_FORMAT_UNKNOWN;
	resourceDesc.Flags = flags;
	resourceDesc.MipLevels = 1;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.SampleDesc.Count = 1;
	resourceDesc.SampleDesc.Quality = 0;
	resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	resourceDesc.Alignment = 0;

	CreateUploadResource(buffer, resourceDesc, flags);
}

void ResourceManager::CreateBuffer(
	Buffer& buffer,
	uint32_t size,
	D3D12_RESOURCE_STATES initState,
	D3D12_RESOURCE_FLAGS flags)
{
	D3D12_RESOURCE_DESC resourceDesc = {};
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	resourceDesc.Width = size;
	resourceDesc.Height = 1;
	resourceDesc.Format = DXGI_FORMAT_UNKNOWN;
	resourceDesc.Flags = flags;
	resourceDesc.MipLevels = 1;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.SampleDesc.Count = 1;
	resourceDesc.SampleDesc.Quality = 0;
	resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	resourceDesc.Alignment = 0;

	auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	if (FAILED(m_DeviceHandle.pDxDev->CreateCommittedResource(
		&heapProperties,
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		initState,
		nullptr,
		IID_PPV_ARGS(buffer.GetDxResourcePtrAddr()))))
	{
		GFXE_ERROR("Failed to create buffer!");
	}

	buffer.SetCurrentState(initState);
}

void ResourceManager::CreateTexture(
	Texture& texture,
	const D3D12_RESOURCE_DESC& resourceDesc,
	D3D12_RESOURCE_STATES initState)
{
	bool clearValueDefined = false;
	D3D12_CLEAR_VALUE clearValue = {};
	if (resourceDesc.Flags & D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET || resourceDesc.Flags & D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL)
	{
		clearValueDefined = true;

		clearValue.Format = resourceDesc.Format;
		if (resourceDesc.Flags & D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET)
		{
			std::memset(clearValue.Color, 0, sizeof(clearValue.Color));
			clearValue.Color[3] = 1.0f;
		}
		else
		{
			clearValue.DepthStencil.Depth = 1.0f;
			clearValue.DepthStencil.Stencil = 0;
		}
	}

	auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	if (FAILED(m_DeviceHandle.pDxDev->CreateCommittedResource(
		&heapProperties,
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		initState,
		clearValueDefined ? &clearValue : nullptr,
		IID_PPV_ARGS(texture.GetDxResourcePtrAddr()))))
	{
		GFXE_ERROR("Failed to create 2D texture!");
	}

	texture.SetCurrentState(initState);
	texture.SetResourceDescription(resourceDesc);
}

ResourceHandle ResourceManager::RegisterGeometryResource(const Mesh* pMesh)
{
	Geometry geometry;

	// Check performance!
	std::vector<Vertex> vertexBufferData;
	vertexBufferData.reserve(pMesh->GetVertexCount());
	if (pMesh->TexCoords0.size() > 0)
	{
		vertexBufferData = std::views::transform(std::views::zip(pMesh->Vertices, pMesh->Normals, pMesh->Tangents, pMesh->Bitangents, pMesh->TexCoords0), [](const auto& vertexData) {
			const auto& [position, normal, tangent, bitangent, texCoords0] = vertexData;
			return Vertex(position, normal, tangent, bitangent, DirectX::XMFLOAT2(texCoords0.x, texCoords0.y));
			}) | std::ranges::to<std::vector>();
	}
	else
	{
		vertexBufferData = std::views::transform(std::views::zip(pMesh->Vertices, pMesh->Normals), [](const auto& vertexData) {
			const auto& [position, normal] = vertexData;
			return Vertex(position, normal, DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f));
			}) | std::ranges::to<std::vector>();
	}

	uint32_t vertexBufferSize = sizeof(Vertex) * (uint32_t)vertexBufferData.size();
	CreateBuffer(geometry.m_VertexBuffer, vertexBufferSize, D3D12_RESOURCE_STATE_COMMON);
	geometry.m_VertexBuffer.SetName(Utility::StringFormat("{}_VERTEX_BUFFER_DEFAULT", pMesh->Name));

	TransitionResource(geometry.m_VertexBuffer, D3D12_RESOURCE_STATE_COPY_DEST);
	CopyDataToGpu(geometry.m_VertexBuffer, vertexBufferData.data(), vertexBufferSize);
	TransitionResource(geometry.m_VertexBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);

	geometry.m_VertexBufferView.BufferLocation = geometry.m_VertexBuffer.GetDxResourcePtr()->GetGPUVirtualAddress();
	geometry.m_VertexBufferView.StrideInBytes = sizeof(Vertex);
	geometry.m_VertexBufferView.SizeInBytes = vertexBufferSize;


	uint32_t indexBufferSize = sizeof(uint32_t) * (uint32_t)pMesh->GetIndexCount();
	CreateBuffer(geometry.m_IndexBuffer, indexBufferSize, D3D12_RESOURCE_STATE_COMMON);
	geometry.m_IndexBuffer.SetName(Utility::StringFormat("{}_INDEX_BUFFER_DEFAULT", pMesh->Name));

	TransitionResource(geometry.m_IndexBuffer, D3D12_RESOURCE_STATE_COPY_DEST);
	CopyDataToGpu(geometry.m_IndexBuffer, (void*)pMesh->Indices.data(), indexBufferSize);
	TransitionResource(geometry.m_IndexBuffer, D3D12_RESOURCE_STATE_INDEX_BUFFER);

	geometry.m_IndexBufferView.BufferLocation = geometry.m_IndexBuffer.GetDxResourcePtr()->GetGPUVirtualAddress();
	geometry.m_IndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	geometry.m_IndexBufferView.SizeInBytes = indexBufferSize;
	geometry.m_IndicesCount = pMesh->GetIndexCount();

	m_GeometrySet.push_back(geometry);

	ResourceHandle handle = {};
	handle.type = ResourceType::GEOMETRY;
	handle.index = (uint32_t)m_GeometrySet.size() - 1;

	m_ResourceHandles[ResourceType::GEOMETRY].insert(std::make_pair(pMesh->Hash, handle));

	return handle;
}

// Loading should take place somewhere else?
ResourceHandle ResourceManager::RegisterTextureResource(const TextureData* pTextureData)
{
#if 1
	DirectX::ScratchImage textureImage;
	if (FAILED(DirectX::LoadFromWICFile(Utility::UTF8ToWideString(pTextureData->FullPath).c_str(), DirectX::WIC_FLAGS_IGNORE_SRGB, nullptr, textureImage)))
	{
		GFXE_ERROR("Could not load textue file!");
	}
	
	DirectX::ScratchImage textureMipChain;
	
	if (FAILED(DirectX::GenerateMipMaps(textureImage.GetImages(), textureImage.GetImageCount(), textureImage.GetMetadata(), DirectX::TEX_FILTER_LINEAR, 0, textureMipChain)))
	{
		GFXE_ERROR("Could not generate mip chain!");
	}
#else
	DirectX::ScratchImage textureMipChain;

	if (FAILED(DirectX::LoadFromWICFile(Utility::UTF8ToWideString(pTextureData->FullPath).c_str(), DirectX::WIC_FLAGS_IGNORE_SRGB, nullptr, textureMipChain)))
	{
		GFXE_ERROR("Could not load textue file!");
	}
#endif

	const auto& baseMip = *textureMipChain.GetImages();

	Texture texture;

	D3D12_RESOURCE_DESC textureDescription = {};
	textureDescription.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	textureDescription.Width = (uint64_t)baseMip.width;
	textureDescription.Height = (uint32_t)baseMip.height;
	textureDescription.DepthOrArraySize = 1;
	textureDescription.MipLevels = (uint16_t)textureMipChain.GetImageCount();
	textureDescription.Format = baseMip.format;
	textureDescription.SampleDesc.Count = 1;
	textureDescription.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	textureDescription.Flags = D3D12_RESOURCE_FLAG_NONE;

	CreateTexture(texture, textureDescription, D3D12_RESOURCE_STATE_COMMON);
	texture.SetName(Utility::StringFormat("{}_TEXTURE", pTextureData->Name));

	auto fillSubresourceData = [&](uint32_t mipIndex) -> D3D12_SUBRESOURCE_DATA
		{
			const auto mipImage = textureMipChain.GetImage(mipIndex, 0, 0);
			const D3D12_SUBRESOURCE_DATA subresourceData = { .pData = mipImage->pixels, .RowPitch = (LONG_PTR)mipImage->rowPitch, .SlicePitch = (LONG_PTR)mipImage->slicePitch };
			return subresourceData;
		};

	const auto subresourceData = std::ranges::views::iota(0, (int)textureMipChain.GetImageCount()) | std::ranges::views::transform(fillSubresourceData) | std::ranges::to<std::vector>();

	TransitionResource(texture, D3D12_RESOURCE_STATE_COPY_DEST);
	CopyDataToGpu(texture, subresourceData);
	TransitionResource(texture, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

	DescriptorHeapRange* bindlessRange = Renderer::sm_DescriptorHeapManager->GetDescriptorHeapRange(DescriptorHeapType::CBV_SRV_UAV, "SRV_BINDLESS");
	texture.SetSrvBindingIndex(bindlessRange->ReserveDescriptor());

	D3D12_SHADER_RESOURCE_VIEW_DESC desc = {};
	desc.Format = texture.GetResourceDescription().Format;
	desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	desc.Texture2D.MipLevels = texture.GetResourceDescription().MipLevels;
	m_DeviceHandle.pDxDev->CreateShaderResourceView(texture.GetDxResourcePtr(), &desc, bindlessRange->GetCpuDescriptorHandleGlobal(texture.GetSrvBindingIndex()));

	m_TextureSet.emplace_back(std::move(texture));

	ResourceHandle handle = {};
	handle.type = ResourceType::TEXTURE;
	handle.index = (uint32_t)m_TextureSet.size() - 1;

	m_ResourceHandles[ResourceType::TEXTURE].insert(std::make_pair(pTextureData->Hash, handle));

	return handle;
}

void ResourceManager::CreateTexture(
	Texture& texture,
	uint32_t width,
	uint32_t height,
	DXGI_FORMAT format,
	D3D12_RESOURCE_STATES initState,
	D3D12_RESOURCE_FLAGS flags,
	uint16_t arraySize,
	uint16_t mipLevels,
	uint32_t sampleCount,
	uint32_t sampleQuality)
{
	D3D12_RESOURCE_DESC resourceDesc = {};
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDesc.Width = width;
	resourceDesc.Height = height;
	resourceDesc.Format = format;
	resourceDesc.Flags = flags;
	resourceDesc.MipLevels = mipLevels;
	resourceDesc.DepthOrArraySize = arraySize;
	resourceDesc.SampleDesc.Count = sampleCount;
	resourceDesc.SampleDesc.Quality = sampleQuality;
	resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resourceDesc.Alignment = 0;

	CreateTexture(texture, resourceDesc, initState);
}