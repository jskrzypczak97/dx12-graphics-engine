
#include "Renderer/Profiler.h"
#include "Renderer/Renderer.h"

void Profiler::Initialize(const DeviceHandle& deviceHandle)
{
	constexpr uint32_t timestampQueriesCount = s_SlotsCount * s_TimestampSlotSize * 2u;

	m_TimestampSlotsQueue.push_range(std::views::iota(0u, s_SlotsCount - 1u));
	m_PendingTimestampSlotsFenceValues.fill(0u);

	D3D12_QUERY_HEAP_DESC timestampQueryHeapDesc = {};
	timestampQueryHeapDesc.NodeMask = 0;
	timestampQueryHeapDesc.Count = timestampQueriesCount;
	timestampQueryHeapDesc.Type = D3D12_QUERY_HEAP_TYPE_TIMESTAMP;
	if (FAILED(deviceHandle.pDxDev->CreateQueryHeap(&timestampQueryHeapDesc, IID_PPV_ARGS(&m_TimestampQueryHeap))))
	{
		GFXE_ERROR("Could not create query heap!");
	}

	Renderer::sm_ResourceManager->CreateReadbackBuffer(m_TimestampQueryResults, timestampQueryHeapDesc.Count * 8); // 8 bytes per single query.

	CommandContext* pContext = Renderer::sm_CommandContextManager->RequestCommandContext(D3D12_COMMAND_LIST_TYPE_DIRECT);
	for (uint32_t i = 0; i < timestampQueriesCount; ++i)
	{
		MarkTimestamp(pContext, i);
	}
	
	pContext->GetDxCommandList()->ResolveQueryData(
		m_TimestampQueryHeap.Get(),
		D3D12_QUERY_TYPE_TIMESTAMP,
		0,
		timestampQueriesCount,
		m_TimestampQueryResults.GetDxResourcePtr(),
		0
	);
	
	Renderer::sm_CommandContextManager->ExecuteContextAndWait(pContext);
}

uint32_t Profiler::GetTimestampSlot()
{
	GFXE_ASSERT(!m_TimestampSlotsQueue.empty(), "No available slots!");

	uint32_t slotIndex = m_TimestampSlotsQueue.front();
	m_TimestampSlotsQueue.pop();

	return slotIndex;
}

void Profiler::MarkTimestampBegin(CommandContext* pContext, const uint32_t slotId, const uint32_t measurementId)
{
	const uint32_t queryIndex = (s_TimestampSlotSize * 2u) * slotId + measurementId;

	MarkTimestamp(pContext, queryIndex);
}

void Profiler::MarkTimestampEnd(CommandContext* pContext, const uint32_t slotId, const uint32_t measurementId)
{
	const uint32_t queryIndex = (s_TimestampSlotSize * 2u) * slotId + s_TimestampSlotSize + measurementId;

	MarkTimestamp(pContext, queryIndex);
}

void Profiler::EndTimestampSlotUsage(CommandContext* pContext, const uint32_t slotId)
{
	pContext->GetDxCommandList()->ResolveQueryData(
		m_TimestampQueryHeap.Get(),
		D3D12_QUERY_TYPE_TIMESTAMP,
		(s_TimestampSlotSize * 2u) * slotId,
		s_TimestampSlotSize * 2u,
		m_TimestampQueryResults.GetDxResourcePtr(),
		(s_TimestampSlotSize * 2u) * slotId * 8
	);

	m_PendingTimestampSlotsFenceValues[slotId] = Renderer::sm_CommandContextManager->SetQueueFence(pContext->GetType());
}

std::array<double_t, Profiler::s_TimestampSlotSize> Profiler::FetchTimestampSlotData(const uint32_t slotId)
{
	std::array<double_t, s_TimestampSlotSize> output = {};

	if (Renderer::sm_CommandContextManager->GetCurrentQueueFence(D3D12_COMMAND_LIST_TYPE_DIRECT) >= m_PendingTimestampSlotsFenceValues[slotId])
	{
		m_PendingTimestampSlotsFenceValues[slotId] = 0u;
		m_TimestampSlotsQueue.push(slotId);

		double_t freq = double_t(Renderer::sm_CommandContextManager->GetCommandQueue(D3D12_COMMAND_LIST_TYPE_DIRECT).GetTimestampFrequency());

		uint64_t* beginTimestampQueries = nullptr;
		if (FAILED(m_TimestampQueryResults.GetDxResourcePtr()->Map(0, nullptr, reinterpret_cast<void**>(&beginTimestampQueries)))) // TODO: Define proper range
		{
			int dbg = 4;
		}

		beginTimestampQueries += (s_TimestampSlotSize * 2u) * slotId;
		uint64_t* endTimestampQueries = beginTimestampQueries + s_TimestampSlotSize;

		for (uint32_t i = 0; i < s_TimestampSlotSize; ++i)
		{
			output[i] = (endTimestampQueries[i] - beginTimestampQueries[i]) / freq * 1000.0;
		}

		m_TimestampQueryResults.GetDxResourcePtr()->Unmap(0, nullptr);
	}
	else
	{
		GFXE_ERROR();
	}

	return output;
}

inline void Profiler::MarkTimestamp(CommandContext* pContext, const uint32_t queryIndex)
{
	pContext->GetDxCommandList()->EndQuery(m_TimestampQueryHeap.Get(), D3D12_QUERY_TYPE_TIMESTAMP, queryIndex);
}
