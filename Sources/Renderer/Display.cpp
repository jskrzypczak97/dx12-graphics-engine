
#include "Renderer/Renderer.h"

#include "Engine/EngineWindow.h"
#include "Engine/EngineConfig.h"
#include "Renderer/Display.h"

bool Display::Initialize(const DeviceHandle& deviceHandle, IDXGIFactory6* pDxgiFactory)
{
	m_DeviceHandle = deviceHandle;

	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};

	swapChainDesc.Width = 0;
	swapChainDesc.Height = 0;
	swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.Scaling = DXGI_SCALING_NONE;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_IGNORE;
	swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	DXGI_SWAP_CHAIN_FULLSCREEN_DESC swapChainDescFs = {};
	swapChainDescFs.Windowed = TRUE;

	if (FAILED(pDxgiFactory->CreateSwapChainForHwnd(
		Renderer::sm_CommandContextManager->GetCommandQueue(D3D12_COMMAND_LIST_TYPE_DIRECT).GetDxCommandQueue(),
		EngineWindow::GetHwnd(),
		&swapChainDesc,
		&swapChainDescFs,
		nullptr,
		&m_pSwapChain)))
	{
		GFXE_ERROR("Failed to create swap chain");

		return false;
	}

	m_pSwapChain->GetDesc1(&m_SwapChainDesc);

	m_pDescriptorHeapRange = Renderer::sm_DescriptorHeapManager->GetDescriptorHeapRange(DescriptorHeapType::RTV, "RTV_BACKBUFFER");

	for (uint32_t i = 0; i < m_BackBuffers.size(); ++i)
	{
		if (FAILED(m_pSwapChain->GetBuffer(i, IID_PPV_ARGS(m_BackBuffers[i].GetDxResourcePtrAddr()))))
		{
			GFXE_ERROR("Failed to obtain buffer from swap chain!");

			return false;
		}

		m_DeviceHandle.pDxDev->CreateRenderTargetView(m_BackBuffers[i].GetDxResourcePtr(), nullptr, m_pDescriptorHeapRange->GetCpuDescriptorHandleLocal(i));
	}

	const uint32_t viewportWidth = GetWidth();
	const uint32_t viewportHeight = GetHeight();

	m_DisplayViewport.TopLeftX = 0;
	m_DisplayViewport.TopLeftY = 0;
	m_DisplayViewport.Width = (float)viewportWidth;
	m_DisplayViewport.Height = (float)viewportHeight;
	m_DisplayViewport.MinDepth = 0.0f;
	m_DisplayViewport.MaxDepth = 1.0f;

	m_DisplayScissorRect.left = 0;
	m_DisplayScissorRect.top = 0;
	m_DisplayScissorRect.right = (uint64_t)viewportWidth;
	m_DisplayScissorRect.bottom = (uint64_t)viewportHeight;

	BaseRenderPass::UpdateDisplayDims(m_DisplayViewport, m_DisplayScissorRect);

	return true;
}
