
#include "Renderer/Shader/Shader.h"
#include "Renderer/Shader/ShaderCompiler.h"

ShaderCompiler Shader::sm_ShaderCompiler;

Shader::Shader(std::string fullName, SHADER_TYPE type)
	: m_Type(type)
{
	auto formatIndex = fullName.find('.');
	auto formatLen = fullName.size() - formatIndex;
	m_FormatStr = fullName.substr(formatIndex, formatLen);
	m_NameStr = fullName.substr(0, fullName.size() - formatLen);

	m_Hash0 = m_Hash1 = 0;
}

bool Shader::Compile()
{
	if (!sm_ShaderCompiler.Compile(this))
	{
		GFXE_DBG_ERROR("Shader compilation failed!");

		return false;
	}
	
	m_IsCompiled = true;

	return true;
}