
#include "Engine/EngineConfig.h"

#include "Renderer/ConstantBuffers/ConstantBufferPoolManager.h"

void ConstantBufferPoolManager::ShutDown()
{
	GFXE_DBG_INFO(Utility::COMPONENTS::_RENDERER_, "Shutting down Constant Buffer Manager!");

	for (auto& dynamicPool : m_DynamicPools)
	{
		dynamicPool.AllContextsServiceEnd();
	}

	TryReleaseDynamicSlots(-1);
}

ConstantBufferSlot* ConstantBufferPoolManager::RequestDynamicSlot(ContextIdType contextId, uint32_t bufferSize)
{
	GFXE_ASSERT(bufferSize != 0, "Constant Buffer of size 0 has been requested!");

	ConstantBufferSize selectedSize;

	// TODO: make it better
	if (bufferSize <= 256)
	{
		selectedSize = ConstantBufferSize::SIZE_256;
	}
	else if (bufferSize <= 512)
	{
		selectedSize = ConstantBufferSize::SIZE_512;
	}
	else if (bufferSize <= 1024)
	{
		selectedSize = ConstantBufferSize::SIZE_1024;
	}
	else
	{
		GFXE_ERROR("Too large Constant Buffer has been requested!", "Max supported size is 1024");
	}

	auto& selectedPool = m_DynamicPools[(uint16_t)selectedSize];

	return selectedPool.GetBufferSlot(contextId);
}

void ConstantBufferPoolManager::EndDynamicSlotsUsage(ContextIdType contextId, FenceValueType fenceValue)
{
	for (auto& dynamicPool : m_DynamicPools)
	{
		dynamicPool.ContextServiceEnd(contextId, fenceValue);
	}
}

void ConstantBufferPoolManager::TryReleaseDynamicSlots(FenceValueType fenceValue)
{
	for (auto& dynamicPool : m_DynamicPools)
	{
		dynamicPool.TryReleaseSlots(fenceValue);
	}
}