
#include <utility>

#include "Renderer/ConstantBuffers/DynamicConstantBufferPool.h"
#include "Renderer/Renderer.h"

std::atomic<uint32_t> DynamicConstantBufferPool::sm_PoolId{ 0 };

DynamicConstantBufferPool::DynamicConstantBufferPool(uint32_t slotSize, uint32_t capacity, DescriptorHeapRange* pDescriptorHeapRange, DeviceHandle deviceHandle)
	: m_SlotSize(slotSize), m_Capacity(capacity), m_ConstantBufferSlotsQueue(capacity), m_ConstantBufferSlots(capacity, ConstantBufferSlot()), m_pDecriptorHeapRange(pDescriptorHeapRange)
{
	//m_DescriptorHeapStrId = Utility::StringFormat("CONST_BUFF_POOL_{}_{}", slotSize, sm_PoolId++);
	sm_PoolId++;

	std::iota(m_ConstantBufferSlotsQueue.begin(), m_ConstantBufferSlotsQueue.end(), 0);
	Renderer::sm_ResourceManager->CreateUploadBuffer(m_Buffer, m_SlotSize * m_Capacity);

	CD3DX12_RANGE readRange(0, 0);
	uint8_t* pCpuAddress;
	m_Buffer.GetDxResourcePtr()->Map(0, &readRange, reinterpret_cast<void**>(&pCpuAddress)); //Check errors

	uint64_t pGpuAddress;
	pGpuAddress = m_Buffer.GetDxResourcePtr()->GetGPUVirtualAddress();

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
	for (uint32_t i = 0; i < capacity; ++i)
	{
		uint32_t alignedSize = (slotSize + (CONSTANT_BUFFER_ALIGNMENT - 1)) & ~(CONSTANT_BUFFER_ALIGNMENT - 1); // Ensrue 256-byte aligment.
		cbvDesc.BufferLocation = pGpuAddress;
		cbvDesc.SizeInBytes = alignedSize;
		deviceHandle.pDxDev->CreateConstantBufferView(&cbvDesc, pDescriptorHeapRange->GetCpuDescriptorHandleLocal(i));

		m_ConstantBufferSlots[i].Index = i;
		m_ConstantBufferSlots[i].CpuAddress = pCpuAddress;
		m_ConstantBufferSlots[i].GpuAddress = pGpuAddress;

		pCpuAddress += alignedSize;
		pGpuAddress += alignedSize;
	}
}

void DynamicConstantBufferPool::ContextServiceStart(ContextIdType contextId)
{
	if (m_CurrentSlotUsage.contains(contextId))
	{
		GFXE_ERROR("Context was already registered and not freed!");
	}

	m_CurrentSlotUsage.insert(std::make_pair(contextId, std::vector<uint32_t>()));
}

void DynamicConstantBufferPool::ContextServiceEnd(ContextIdType contextId, FenceValueType fenceValue)
{
	if (m_CurrentSlotUsage.contains(contextId))
	{
		m_SlotsToFree.emplace(std::make_pair(fenceValue, std::move(m_CurrentSlotUsage[contextId])));
		m_CurrentSlotUsage.erase(contextId);
	}
}

void DynamicConstantBufferPool::AllContextsServiceEnd()
{
	std::vector<uint64_t> contextIds;
	contextIds.reserve(m_CurrentSlotUsage.size());
	for (const auto& contextId : m_CurrentSlotUsage) contextIds.push_back(contextId.first);

	for (const auto contextId : contextIds)
	{
		m_SlotsToFree.emplace(std::make_pair(0, std::move(m_CurrentSlotUsage[contextId])));
		m_CurrentSlotUsage.erase(contextId);
	}
}

void DynamicConstantBufferPool::TryReleaseSlots(FenceValueType fenceValue)
{
	while (!m_SlotsToFree.empty() && m_SlotsToFree.front().first <= fenceValue) // < or <= ??
	{
		auto& currentBufferSet = m_SlotsToFree.front();

		m_ConstantBufferSlotsQueue.insert(m_ConstantBufferSlotsQueue.end(), currentBufferSet.second.begin(), currentBufferSet.second.end());
		m_SlotsToFree.pop();
	}
}

ConstantBufferSlot* DynamicConstantBufferPool::GetBufferSlot(ContextIdType contextId)
{
	if (!m_CurrentSlotUsage.contains(contextId))
	{
		ContextServiceStart(contextId);
	}

	if (m_ConstantBufferSlotsQueue.empty())
	{
		//TODO: we need to handle such cases
		GFXE_ERROR("Constant buffer slots queue is empty!");
	}

	uint32_t slotIndex = m_ConstantBufferSlotsQueue.front();
	m_ConstantBufferSlotsQueue.pop_front();

	m_CurrentSlotUsage.at(contextId).push_back(slotIndex);

	return &m_ConstantBufferSlots[slotIndex];
}