
#include "Renderer/DescriptorHeaps/DescriptorHeap.h"

DescriptorHeap::DescriptorHeap(const uint32_t size, const uint32_t incrementSize, const D3D12_DESCRIPTOR_HEAP_DESC dxDesc, ID3D12DescriptorHeap* dxDescriptorHeap)
{
	m_Size = size;
	m_IncrementSize = incrementSize;
	m_DxDesc = dxDesc;
	m_DxDescriptorHeap.Attach(dxDescriptorHeap);
}

void DescriptorHeap::Initialize()
{
	const std::string name = "MAIN_RANGE";

	const D3D12_CPU_DESCRIPTOR_HANDLE cpuHandle = m_DxDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	const D3D12_GPU_DESCRIPTOR_HANDLE gpuHandle = IsShaderVisible() ? m_DxDescriptorHeap->GetGPUDescriptorHandleForHeapStart() : D3D12_GPU_DESCRIPTOR_HANDLE{};

	m_Ranges.emplace(std::make_pair(name, DescriptorHeapRange(name, m_Size, 0, cpuHandle, gpuHandle, this)));
}
