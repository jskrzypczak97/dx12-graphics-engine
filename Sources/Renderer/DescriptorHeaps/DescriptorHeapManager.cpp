
#include "Engine/EngineConfig.h"
#include "Renderer/DescriptorHeaps/DescriptorHeapManager.h"

void DescriptorHeapManager::CreateGeneralUseDescriptorHeaps()
{
		// RTV
		// Swapchain RTs : GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT (3) 
		// Main RT : 1
		constexpr uint32_t rtsCount = GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT + GFXE_CONFIG_RT_COUNT;
		auto rtvDH = CreateDescriptorHeap((uint32_t)DescriptorHeapType::RTV, rtsCount, D3D12_DESCRIPTOR_HEAP_TYPE_RTV, false);
		auto mainRtvRange = rtvDH->GetMainRange();
		mainRtvRange->ReserveSubrange("RTV_BACKBUFFER", GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT);
		mainRtvRange->ReserveSubrange("RTV_RENDER", GFXE_CONFIG_RT_COUNT);

#ifdef _DEBUG
		rtvDH->GetDxDescriptorHeap()->SetName(Utility::UTF8ToWideString("RTV_MAIN_DESC_HEAP").c_str());
#endif

		// DSV
		// Main DS : 1
		constexpr uint32_t dssCount = GFXE_CONFIG_DS_COUNT;
		auto dsvDH = CreateDescriptorHeap((uint32_t)DescriptorHeapType::DSV, dssCount, D3D12_DESCRIPTOR_HEAP_TYPE_DSV, false);
		auto mainDsvRange = dsvDH->GetMainRange();
		mainDsvRange->ReserveSubrange("DSV_RENDER", GFXE_CONFIG_DS_COUNT);

#ifdef _DEBUG
		dsvDH->GetDxDescriptorHeap()->SetName(Utility::UTF8ToWideString("DSV_MAIN_DESC_HEAP").c_str());
#endif

		// CBV_SRV_UAV
		
		// BINDLESS
		// CBV : 0
		constexpr uint32_t bindlessCbvCount = 0;
		// SRV : 128
		constexpr uint32_t bindlessSrvCount = 128;
		// UAV : 0
		constexpr uint32_t bindlessUavCount = 0;

		// NON-BINDLESS
		// CBV : GFXE_CONFIG_CBV_256_COUNT + GFXE_CONFIG_CBV_512_COUNT + GFXE_CONFIG_CBV_1024_COUNT
		constexpr uint32_t cbvCount = GFXE_CONFIG_CBV_256_COUNT + GFXE_CONFIG_CBV_512_COUNT + GFXE_CONFIG_CBV_1024_COUNT;
		// SRV : 0
		constexpr uint32_t srvCount = 128;
		// UAV : 0
		constexpr uint32_t uavCount = 0;

		constexpr uint32_t totalCount = bindlessCbvCount + bindlessSrvCount + bindlessUavCount + cbvCount + srvCount + uavCount + 1;
		auto cbvSrvUavDH = CreateDescriptorHeap((uint32_t)DescriptorHeapType::CBV_SRV_UAV, totalCount, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, true);
		auto mainCbvSrvUavRange = cbvSrvUavDH->GetMainRange();
		mainCbvSrvUavRange->ReserveSubrange("CBV_BINDLESS", bindlessCbvCount);
		mainCbvSrvUavRange->ReserveSubrange("SRV_BINDLESS", bindlessSrvCount);
		mainCbvSrvUavRange->ReserveSubrange("UAV_BINDLESS", bindlessUavCount);
		mainCbvSrvUavRange->ReserveSubrange("CBV", cbvCount);
		mainCbvSrvUavRange->ReserveSubrange("SRV", srvCount);
		mainCbvSrvUavRange->ReserveSubrange("UAV", uavCount);
#ifdef GFXE_ENABLE_UI
		mainCbvSrvUavRange->ReserveSubrange("IMGUI", 1);
#endif

#ifdef _DEBUG
		cbvSrvUavDH->GetDxDescriptorHeap()->SetName(Utility::UTF8ToWideString("CBV_SRV_UAV_MAIN_DESC_HEAP").c_str());
#endif

}

void DescriptorHeapManager::ShutDown()
{
	GFXE_DBG_INFO(Utility::COMPONENTS::_RENDERER_, "Shutting down Descriptor Heap Manager!");

	m_DescriptorHeaps.clear();
}

DescriptorHeap* DescriptorHeapManager::CreateDescriptorHeap(const uint32_t id, uint32_t descriptorsCount, D3D12_DESCRIPTOR_HEAP_TYPE type, bool isShaderVisible)
{
	if (m_DescriptorHeaps.contains(id))
	{
		GFXE_ASSERT("Descriptor Heap with a given id already exists!");

		return &m_DescriptorHeaps.at(id);
	}

	D3D12_DESCRIPTOR_HEAP_DESC dxDesc = {};
	dxDesc.NumDescriptors = descriptorsCount;
	dxDesc.Type = type;
	dxDesc.Flags = isShaderVisible ? D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE : D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

	uint32_t incrementSize = m_DeviceHandle.pDxDev->GetDescriptorHandleIncrementSize(type);

	ID3D12DescriptorHeap* descriptorHeap;
	if (FAILED(m_DeviceHandle.pDxDev->CreateDescriptorHeap(&dxDesc, IID_PPV_ARGS(&descriptorHeap))))
	{
		GFXE_ERROR("Failed to create Descriptor Heap!");

		return nullptr;
	}

	m_DescriptorHeaps.emplace(std::move(std::make_pair(id, std::move(DescriptorHeap(descriptorsCount, incrementSize, dxDesc, descriptorHeap)))));
	m_DescriptorHeaps.at(id).Initialize();

	return &m_DescriptorHeaps.at(id);
}

DescriptorHeap* DescriptorHeapManager::GetDescriptorHeap(const DescriptorHeapType type)
{
	uint32_t typeId = (uint32_t)type;
	if (!m_DescriptorHeaps.contains(typeId))
	{
		GFXE_ERROR("Descriptor Heap with a given id does not exist!");

		return nullptr;
	}

	return &m_DescriptorHeaps.at(typeId);
}

DescriptorHeapRange* DescriptorHeapManager::GetDescriptorHeapRange(const DescriptorHeapType type, const std::string& rangeString)
{
	return GetDescriptorHeap(type)->GetRange(rangeString);
}