
#include "Renderer/DescriptorHeaps/DescriptorHeapRange.h"
#include "Renderer/DescriptorHeaps/DescriptorHeap.h"

DescriptorHeapRange::DescriptorHeapRange(
	const std::string& stringId,
	const uint32_t size,
	const uint32_t globalOffset,
	const D3D12_CPU_DESCRIPTOR_HANDLE& cpuHandle,
	const D3D12_GPU_DESCRIPTOR_HANDLE& gpuHandle,
	DescriptorHeap* parent)
{
	m_StringId = stringId;
	m_DescriptorsCount = size;
	m_AvailableDescriptorsCount = size;
	m_GlobalOffset = globalOffset;
	m_StartCpuHandle = cpuHandle;
	m_StartGpuHandle = gpuHandle;
	m_Parent = parent;
}

DescriptorHeapRange* DescriptorHeapRange::ReserveSubrange(const std::string& subrangeId, const uint32_t subsize)
{
	if (!subsize)
	{
		GFXE_INFO(Utility::COMPONENTS::_RENDERER_, "Attempted to create Descriptor Heap range with 0 size!", "Range was not created!");

		return nullptr;
	}

	if (subsize > m_AvailableDescriptorsCount)
	{
		GFXE_ERROR("Not enough available descriptors to reserve this range!");
	}

	m_Parent->m_Ranges.emplace(std::make_pair(subrangeId, DescriptorHeapRange(
		subrangeId,
		subsize,
		m_GlobalOffset + m_CurrentLocalOffset,
		GetCpuDescriptorHandleLocal(m_CurrentLocalOffset),
		IsDescriptorShaderVisible() ? GetGpuDescriptorHandleLocal(m_CurrentLocalOffset) : D3D12_GPU_DESCRIPTOR_HANDLE(),
		m_Parent
	)));

	m_AvailableDescriptorsCount -= subsize;
	m_CurrentLocalOffset += subsize;

	return &m_Parent->m_Ranges.at(subrangeId);
}

// Returns global binding index
uint32_t DescriptorHeapRange::ReserveDescriptor()
{
	if (m_AvailableDescriptorsCount == 0)
	{
		GFXE_ERROR("Range is full!");
	}

	m_AvailableDescriptorsCount--;

	return m_GlobalOffset + m_CurrentLocalOffset++;
}

D3D12_CPU_DESCRIPTOR_HANDLE DescriptorHeapRange::GetCpuDescriptorHandleGlobal(uint32_t offset) const
{
	GFXE_ASSERT(offset >= m_GlobalOffset && offset < m_GlobalOffset + m_DescriptorsCount, "Offset out of range bounds!");

	return GetCpuDescriptorHandleLocal(offset - m_GlobalOffset);
}

D3D12_CPU_DESCRIPTOR_HANDLE DescriptorHeapRange::GetCpuDescriptorHandleLocal(uint32_t offset) const
{
	D3D12_CPU_DESCRIPTOR_HANDLE handle = m_StartCpuHandle;

	handle.ptr += (SIZE_T)(offset * m_Parent->m_IncrementSize);

	return handle;
}

D3D12_GPU_DESCRIPTOR_HANDLE DescriptorHeapRange::GetGpuDescriptorHandleGlobal(uint32_t offset) const
{
	GFXE_ASSERT(offset >= m_GlobalOffset && offset < m_GlobalOffset + m_DescriptorsCount, "Offset out of range bounds!");

	return GetGpuDescriptorHandleLocal(offset - m_GlobalOffset);
}

D3D12_GPU_DESCRIPTOR_HANDLE DescriptorHeapRange::GetGpuDescriptorHandleLocal(uint32_t offset) const
{
	GFXE_ASSERT(m_Parent->m_DxDesc.Flags == D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE, "Only shader visible Descriptor Heaps have GPU handle!");

	D3D12_GPU_DESCRIPTOR_HANDLE handle = m_StartGpuHandle;

	handle.ptr += (SIZE_T)(offset * m_Parent->m_IncrementSize);

	return handle;
}

bool DescriptorHeapRange::IsDescriptorShaderVisible() const
{
	return m_Parent->IsShaderVisible();
}