
#include <dxgidebug.h>
#include <d3dcompiler.h>

#include <DirectXMath.h>
#include <imgui/imgui_impl_dx12.h>
#include <DirectXTex.h>

#include <ranges>

#include "Renderer/Renderer.h"
#include "Renderer/RendererUtilities.h"
#include "Renderer/RenderOperations.h"

#include "Renderer/RenderEntities/RenderObject.h"
#include "Renderer/Misc.h"

#include "Renderer/Shader/ShaderCompiler.h"

#include "Scene/SceneModel/SceneModel.h"

#include "Utilities/Hasher.h"

using namespace Utility;

namespace WRL = Microsoft::WRL;

std::atomic<uint64_t> Renderer::sm_CurrentRenderEntityId{ 0 };

std::unique_ptr<CommandContextManager> Renderer::sm_CommandContextManager;
std::unique_ptr<ResourceManager> Renderer::sm_ResourceManager;
std::unique_ptr<DescriptorHeapManager> Renderer::sm_DescriptorHeapManager;
std::unique_ptr<ConstantBufferPoolManager> Renderer::sm_ConstantBufferPoolManager;

bool Renderer::Initialize()
{
	uint32_t dxgiFactoryFlags;

	// Initialize COM. Needed for DirectXTex?
	if (FAILED(CoInitializeEx(nullptr, COINIT_MULTITHREADED)))
	{
		GFXE_ERROR("Failed to initialize COM!");

		return false;
	}

#if _DEBUG
	dxgiFactoryFlags = DXGI_CREATE_FACTORY_DEBUG;
	bool enableGPUBasedValidation = false;

	RendererUtilities::EnableDebugLayer(dxgiFactoryFlags, enableGPUBasedValidation);
#else
	dxgiFactoryFlags = 0;
	GFXE_INFO(COMPONENTS::_RENDERER_, "D3D12 debug layer not enabled!");
#endif

	WRL::ComPtr<IDXGIFactory6> pDxgiFactory;
	if (FAILED(CreateDXGIFactory2(dxgiFactoryFlags, IID_PPV_ARGS(&pDxgiFactory))))
	{
		GFXE_ERROR("Failed to create DXGI factory!", "Initialization cannot be continued!");

		return false;
	}

	if (!RendererUtilities::FindAdapter(GFXE_CONFIG_ADAPTER_VENDOR, pDxgiFactory.Get(), m_Adapter))
	{
		GFXE_ERROR("Could not find adapter that meets requirements!", "Initialization cannot be continued!");

		return false;
	}

	if (FAILED(D3D12CreateDevice(m_Adapter.pDxAdapter.Get(), GFXE_CONFIG_DX_FEATURE_LEVEL, IID_PPV_ARGS(&m_Device.pDxDev))))
	{
		GFXE_ERROR("DirectX device could not be created!", "Initialization cannot be continued!");

		return false;
	}

	sm_CommandContextManager = std::make_unique<CommandContextManager>(m_Device.MakeDeviceHandle());

	sm_ResourceManager = std::make_unique<ResourceManager>(m_Device.MakeDeviceHandle());

	sm_DescriptorHeapManager = std::make_unique<DescriptorHeapManager>(m_Device.MakeDeviceHandle());
	sm_DescriptorHeapManager->CreateGeneralUseDescriptorHeaps();

	sm_ConstantBufferPoolManager = std::make_unique<ConstantBufferPoolManager>(m_Device.MakeDeviceHandle(), sm_DescriptorHeapManager->GetDescriptorHeapRange(DescriptorHeapType::CBV_SRV_UAV, "CBV"));

	// Init back buffers
	m_Display.Initialize(m_Device.MakeDeviceHandle(), pDxgiFactory.Get());

	m_Profiler.Initialize(m_Device.MakeDeviceHandle());

	// Create RTV and DSV
	auto renderRtvDHRange = sm_DescriptorHeapManager->GetDescriptorHeapRange(DescriptorHeapType::RTV, "RTV_RENDER");
	for (uint32_t i = 0; i < m_RtSurfaces.size(); ++i)
	{
		sm_ResourceManager->CreateTexture(m_RtSurfaces[i], m_Display.GetWidth(), m_Display.GetHeight(), DXGI_FORMAT_R8G8B8A8_UNORM, D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET);

		D3D12_RENDER_TARGET_VIEW_DESC rtvDesc = {};
		rtvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;

		m_Device.pDxDev->CreateRenderTargetView(m_RtSurfaces[i].GetDxResourcePtr(), &rtvDesc, renderRtvDHRange->GetCpuDescriptorHandleLocal(i));

		// Maybe this should be done by ResourceManager?
		// sm_DescriptorHeapManager should be used only be ResourceManager?
		m_RtSurfaces[i].SetRenderTargetView(renderRtvDHRange->GetCpuDescriptorHandleLocal(i), rtvDesc);
	}

	auto dsvDHRange = sm_DescriptorHeapManager->GetDescriptorHeapRange(DescriptorHeapType::DSV, "DSV_RENDER");
	for (uint32_t i = 0; i < m_DsSurfaces.size(); ++i)
	{
		sm_ResourceManager->CreateTexture(m_DsSurfaces[i], m_Display.GetWidth(), m_Display.GetHeight(), DXGI_FORMAT_D32_FLOAT, D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL);
		D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
		dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
		dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		dsvDesc.Flags = D3D12_DSV_FLAG_NONE;

		m_Device.pDxDev->CreateDepthStencilView(m_DsSurfaces[i].GetDxResourcePtr(), &dsvDesc, dsvDHRange->GetCpuDescriptorHandleLocal(i));

		m_DsSurfaces[i].SetDepthStencilView(dsvDHRange->GetCpuDescriptorHandleLocal(i), dsvDesc);
	}

	m_MainPass.Initialize(m_Device.MakeDeviceHandle());

#ifdef GFXE_ENABLE_UI
	m_GuiPass.Initialize(m_Device.MakeDeviceHandle());
#endif

	return true;
}

void Renderer::ShutDown()
{
	GFXE_DBG_INFO(Utility::COMPONENTS::_RENDERER_, "Shutting down Renderer!");

	sm_CommandContextManager->ShutDown();
	sm_CommandContextManager.reset();

	sm_ConstantBufferPoolManager->ShutDown();
	sm_ConstantBufferPoolManager.reset();

	sm_DescriptorHeapManager->ShutDown();
	sm_DescriptorHeapManager.reset();

	sm_ResourceManager->ShutDown();
	sm_ResourceManager.reset();
}

void Renderer::PreRender(Scene* pScene)
{
	m_CurrentFrameData.FrameNumber++;
	m_CurrentFrameData.ProfilerSlot = m_Profiler.GetTimestampSlot();

	if (pScene->HasNewEntities())
	{
		ProcessNewSceneEntities(pScene);
	}

	sm_ConstantBufferPoolManager->TryReleaseDynamicSlots(sm_CommandContextManager->GetCommandQueue(D3D12_COMMAND_LIST_TYPE_DIRECT).GetFenceValue());
}

void Renderer::Render(Scene* pScene, Camera* pCamera)
{
	PreRender(pScene);

	Texture* pRenderTarget = &m_RtSurfaces[m_Display.GetCurrentFrameIndex()];
	Texture* pDepthStencil = &m_DsSurfaces[m_Display.GetCurrentFrameIndex()];

	Texture* pBackBuffer = m_Display.GetCurrentBackBuffer();

	CommandContext* pMainContext = sm_CommandContextManager->RequestCommandContext(D3D12_COMMAND_LIST_TYPE_DIRECT);
	m_Profiler.MarkTimestampBegin(pMainContext, m_CurrentFrameData.ProfilerSlot, ProfileGpuStages::FRAME);

	RenderOperations::ClearRtsDss(pMainContext, { pRenderTarget }, { pDepthStencil });

	m_Profiler.MarkTimestampBegin(pMainContext, m_CurrentFrameData.ProfilerSlot, ProfileGpuStages::MAIN_PASS);
	if (m_MainPass.AnyObjectsRegistered())
	{
		m_MainPass.UpdateData(pMainContext->GetContextId(), pScene, pCamera, this);
		m_MainPass.RecordContext(pMainContext, m_RenderObjects, pRenderTarget, pDepthStencil);
	}
	m_Profiler.MarkTimestampEnd(pMainContext, m_CurrentFrameData.ProfilerSlot, ProfileGpuStages::MAIN_PASS);

#ifdef GFXE_ENABLE_UI
	m_Profiler.MarkTimestampBegin(pMainContext, m_CurrentFrameData.ProfilerSlot, ProfileGpuStages::GUI_PASS);
	if (m_RenderUI)
	{
		m_GuiPass.RecordContext(pMainContext, pRenderTarget, pDepthStencil);
	}
	m_Profiler.MarkTimestampEnd(pMainContext, m_CurrentFrameData.ProfilerSlot, ProfileGpuStages::GUI_PASS);
#endif

	RenderOperations::CopySurface2D(pMainContext, pRenderTarget, pBackBuffer);
	RenderOperations::TransitionResource(pMainContext, pBackBuffer, D3D12_RESOURCE_STATE_PRESENT);

	m_Profiler.MarkTimestampEnd(pMainContext, m_CurrentFrameData.ProfilerSlot, ProfileGpuStages::FRAME);
	
	m_Profiler.EndTimestampSlotUsage(pMainContext, m_CurrentFrameData.ProfilerSlot);

	auto fenceValue = sm_CommandContextManager->ExecuteContextAndWait(pMainContext);

	m_CurrentFrameData.GetGpuMeasurements(m_Profiler.FetchTimestampSlotData(m_CurrentFrameData.ProfilerSlot));

	m_Display.Present();

	sm_ConstantBufferPoolManager->EndDynamicSlotsUsage(pMainContext->GetContextId(), fenceValue);

	PostRender();
}

void Renderer::PostRender()
{
	m_MainPass.ClearAssignedObjects();
}

uint64_t Renderer::RegisterRenderObject(RenderObject&& renderObject)
{
	uint64_t id = sm_CurrentRenderEntityId;
	m_RenderObjects.insert(std::make_pair(id, renderObject));

	return sm_CurrentRenderEntityId++;
}

void Renderer::ProcessNewSceneEntities(Scene* pScene)
{
	if (!pScene->HasNewEntities())
	{
		return;
	}

	for (const auto modelId : pScene->GetNewSceneEntitiesIds<MODEL>())
	{
		// Do nothing for the model entities.
	}
	pScene->ResetNewSceneEntitiesIds<MODEL>();

	// Each scene mesh is converted to a render object.
	for (const auto sceneMeshId : pScene->GetNewSceneEntitiesIds<MESH>())
	{
		const SceneMesh& sceneMesh = pScene->GetSceneEntity<SceneMesh>(sceneMeshId);

		const Mesh* mesh = sceneMesh.GetMesh();
		const Material* material = mesh->pMaterial;

		// TEMP
		if (material->Name.contains("decal")) continue;

		RenderObject newRenderObject = CreateRenderObject(mesh, material, &sceneMesh);

		uint64_t renderObjectId = RegisterRenderObject(std::move(newRenderObject));

		m_MainPass.RegisterObjectToPass(renderObjectId);
	}
	pScene->ResetNewSceneEntitiesIds<MESH>();
}

RenderObject Renderer::CreateRenderObject(const Mesh* mesh, const Material* material, const SceneNodeEntity* parentNode)
{
	RenderObject renderObject;

	renderObject.m_pParentSceneNode = parentNode;

	sm_ResourceManager->BeginOperation();

	renderObject.m_GeometryHandle = !sm_ResourceManager->IsRegistered<ResourceType::GEOMETRY>(mesh->Hash) ?
		sm_ResourceManager->RegisterGeometryResource(mesh) :
		renderObject.m_GeometryHandle = sm_ResourceManager->GetResourceHandle<ResourceType::GEOMETRY>(mesh->Hash);

	if (material->BaseColorTexData.IsDefined())
	{
		renderObject.m_BaseColorTextureHandle = !sm_ResourceManager->IsRegistered<ResourceType::TEXTURE>(material->BaseColorTexData.Hash) ?
			sm_ResourceManager->RegisterTextureResource(&material->BaseColorTexData) :
			sm_ResourceManager->GetResourceHandle<ResourceType::TEXTURE>(material->BaseColorTexData.Hash);
	}
	else
	{
		renderObject.m_BaseColorTextureHandle.Invalidate();
	}

	if (material->NormalTexData.IsDefined())
	{
		renderObject.m_NormalTextureHandle = !sm_ResourceManager->IsRegistered<ResourceType::TEXTURE>(material->NormalTexData.Hash) ?
			sm_ResourceManager->RegisterTextureResource(&material->NormalTexData) :
			sm_ResourceManager->GetResourceHandle<ResourceType::TEXTURE>(material->NormalTexData.Hash);
	}
	else
	{
		renderObject.m_NormalTextureHandle.Invalidate();
	}

	if (material->RoughnessMetalnessTexData.IsDefined())
	{
		renderObject.m_RoughnessMetalnessTextureHandle = !sm_ResourceManager->IsRegistered<ResourceType::TEXTURE>(material->RoughnessMetalnessTexData.Hash) ?
			sm_ResourceManager->RegisterTextureResource(&material->RoughnessMetalnessTexData) :
			sm_ResourceManager->GetResourceHandle<ResourceType::TEXTURE>(material->RoughnessMetalnessTexData.Hash);
	}
	else
	{
		renderObject.m_RoughnessMetalnessTextureHandle.Invalidate();
	}

	sm_ResourceManager->EndOperation();

	return renderObject;
}
