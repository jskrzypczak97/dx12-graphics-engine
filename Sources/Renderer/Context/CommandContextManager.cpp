
#include "Renderer/Context/CommandContextManager.h"

#include "Engine/EngineConfig.h"

void CommandContextManager::ShutDown()
{
	GFXE_DBG_INFO(Utility::COMPONENTS::_RENDERER_, "Shutting down Command Context Manager!");

	m_GraphicsQueue.ShutDown();
	m_ComputeQueue.ShutDown();

	m_AvailableCommandContexts.clear();
	m_CommandContextPool.clear();
}

CommandContext* CommandContextManager::RequestCommandContext(const D3D12_COMMAND_LIST_TYPE type)
{
	CommandContext* context = m_AvailableCommandContexts[type].front();

	context->Reset(GetCommandQueue(type).RequestAllocator());

	return context;
}
