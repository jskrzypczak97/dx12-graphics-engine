
#include "Renderer/Context/CommandQueue.h"

namespace WRL = Microsoft::WRL;

bool CommandQueue::Initialize(const DeviceHandle& deviceHandle)
{
	m_DeviceHandle = deviceHandle;

	D3D12_COMMAND_QUEUE_DESC commandQueueDesc = {};
	commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	commandQueueDesc.Type = m_Type;

	if (FAILED(m_DeviceHandle.pDxDev->CreateCommandQueue(&commandQueueDesc, IID_PPV_ARGS(&m_pCommandQueue))))
	{
		GFXE_ERROR("Failed to create D3D12 Command Queue!");

		return false;
	}
	m_pCommandQueue->SetName(Utility::UTF8ToWideString(
		Utility::StringFormat("GFXE_COMMAND_QUEUE_{}", m_Type == D3D12_COMMAND_LIST_TYPE_DIRECT ? "GRAPHICS" : "COMPUTE")).c_str());

	constexpr uint32_t initCommandAllocatorCount = GFXE_CONFIG_SWAP_CHAIN_BUFFER_COUNT;
	m_CommandAllocatorPool.reserve(initCommandAllocatorCount);
	for (uint32_t i = 0; i < initCommandAllocatorCount; ++i)
	{
		WRL::ComPtr<ID3D12CommandAllocator> newCommandAllocator;
		if (FAILED(m_DeviceHandle.pDxDev->CreateCommandAllocator(m_Type, IID_PPV_ARGS(&newCommandAllocator))))
		{
			GFXE_ERROR("Failed to create D3D12 Command Allocator!");

			return false;
		}
		m_CommandAllocatorPool.emplace_back(newCommandAllocator);
		m_AvailableCommandAllocators.push(m_CommandAllocatorPool[i].Get());
	}

	if (FAILED(m_DeviceHandle.pDxDev->CreateFence(m_FenceValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_pFence))))
	{
		GFXE_ERROR("Failed to create Fence!");
	}
	m_pFence->SetName(L"FENCE");

	m_FenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (m_FenceEvent == nullptr)
	{
		GFXE_ERROR("Failed to create fence event!");
	}

	if (FAILED(m_pCommandQueue->GetTimestampFrequency(&m_TimestampFrequency)))
	{
		GFXE_ERROR("Failed to get timestamp frequency!");
	}

	return true;
}

void CommandQueue::ShutDown()
{
	GFXE_DBG_INFO(Utility::COMPONENTS::_RENDERER_, Utility::StringFormat("Shutting down Command {} Queue!", m_Type == D3D12_COMMAND_LIST_TYPE_DIRECT ? "Graphics" : "Compute"));

	SyncQueue();
}

ID3D12CommandAllocator* CommandQueue::RequestAllocator()
{
	GFXE_ASSERT(!m_AvailableCommandAllocators.empty(), "No available Command Allocators!");

	ID3D12CommandAllocator* allocator = m_AvailableCommandAllocators.front();

	m_AvailableCommandAllocators.pop();

	// This should happen when we request the allocator?
	WaitForFence();

	if (FAILED(allocator->Reset()))
	{
		GFXE_ERROR("Failed to reset Command Allocator!");
	}

	return allocator;
}

void CommandQueue::DisposeAllocator(ID3D12CommandAllocator* allocator)
{
	// Allocator might need to know the fence value that sets it free
	m_AvailableCommandAllocators.push(allocator);
}

uint64_t CommandQueue::SetFence()
{
	uint64_t expectedFenceValue = ++m_FenceValue;

	if (FAILED(m_pCommandQueue->Signal(m_pFence.Get(), expectedFenceValue)))
	{
		GFXE_ERROR("Failed to set Signal!");
	}

	return expectedFenceValue;
}

void CommandQueue::WaitForFence(uint64_t expectedValue)
{
	expectedValue = expectedValue != UINT64_MAX ? expectedValue : GetFenceValue();

	uint64_t completedFenceValue = GetCompletedFenceValue();

	if (completedFenceValue < expectedValue)
	{
		if (FAILED(m_pFence.Get()->SetEventOnCompletion(expectedValue, m_FenceEvent)))
		{
			GFXE_ERROR("Failed to set Event!");
		}

		WaitForSingleObject(m_FenceEvent, INFINITE);
	}
}

void CommandQueue::SyncQueue()
{
	WaitForFence(SetFence());
}
