
#include "Renderer/Context/CommandContext.h"

#include "Utilities/Utility.h"

bool CommandContext::Initialize(const DeviceHandle& deviceHandle, ID3D12CommandAllocator* allocator)
{
	if (FAILED(deviceHandle.pDxDev->CreateCommandList(0, m_Type, allocator, nullptr, IID_PPV_ARGS(&m_CommandList))))
	{
		GFXE_ERROR("Failed to create D3D12 Command List!");

		return false;
	}
	
	Close();

	return true;
}

void CommandContext::Reset(ID3D12CommandAllocator* allocator)
{
	if (!m_IsClosed)
	{
		GFXE_ASSERT("Command list have to be closed before resetting!");
		
		Close();
	}

	m_CommandList->Reset(allocator, nullptr);
	m_AssignedCommandAllocator = allocator;

	m_IsClosed = false;
}

void CommandContext::Clear()
{
	Reset(m_AssignedCommandAllocator);
}

void CommandContext::Close()
{
	if (!m_IsClosed)
	{
		if (FAILED(m_CommandList->Close()))
		{
			GFXE_ERROR("Could not close Command List!");
		}

		m_IsClosed = true;
	}
}
