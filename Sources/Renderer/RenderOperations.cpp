
#include "directx\d3dx12.h"

#include "Renderer/RenderOperations.h"

namespace RenderOperations
{
	void ClearRtsDss(CommandContext* pCommandContext, std::vector<Texture*> pRts, std::vector<Texture*> pDss)
	{
		ID3D12GraphicsCommandList* pCommandList = pCommandContext->GetDxCommandList();

		{
			// should be handled automatically somehow
			std::vector<D3D12_RESOURCE_BARRIER> barriers;

			for (uint32_t i = 0; i < pRts.size(); ++i)
			{
				if (pRts[i]->GetCurrentState() != D3D12_RESOURCE_STATE_RENDER_TARGET)
				{
					barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pRts[i]->GetDxResourcePtr(), pRts[i]->GetCurrentState(), D3D12_RESOURCE_STATE_RENDER_TARGET));
					pRts[i]->SetCurrentState(D3D12_RESOURCE_STATE_RENDER_TARGET);
				}
			}

			for (uint32_t i = 0; i < pDss.size(); ++i)
			{
				if (pDss[i]->GetCurrentState() != D3D12_RESOURCE_STATE_DEPTH_WRITE)
				{
					barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pDss[i]->GetDxResourcePtr(), pDss[i]->GetCurrentState(), D3D12_RESOURCE_STATE_DEPTH_WRITE));
					pDss[i]->SetCurrentState(D3D12_RESOURCE_STATE_DEPTH_WRITE);
				}
			}

			if (barriers.size() > 0)
			{
				pCommandList->ResourceBarrier((uint32_t)(barriers.size()), barriers.data());
			}
		}

		const float clearColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		for (uint32_t i = 0; i < pRts.size(); ++i)
		{
			D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = pRts[i]->GetRenderTargetViewHandle();
			pCommandList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
		}

		for (uint32_t i = 0; i < pDss.size(); ++i)
		{
			D3D12_CPU_DESCRIPTOR_HANDLE dsvHandle = pDss[i]->GetDepthStencilViewHandle();
			pCommandList->ClearDepthStencilView(dsvHandle, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
		}
	}

	void CopySurface2D(CommandContext* pCommandContext, Texture* pSrc, Texture* pDst)
	{
		ID3D12GraphicsCommandList* pCommandList = pCommandContext->GetDxCommandList();

		{
			// should be handled automatically somehow
			std::vector<D3D12_RESOURCE_BARRIER> barriers;
			if (pSrc->GetCurrentState() != D3D12_RESOURCE_STATE_COPY_SOURCE) barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pSrc->GetDxResourcePtr(), pSrc->GetCurrentState(), D3D12_RESOURCE_STATE_COPY_SOURCE));
			if (pDst->GetCurrentState() != D3D12_RESOURCE_STATE_COPY_DEST) barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pDst->GetDxResourcePtr(), pDst->GetCurrentState(), D3D12_RESOURCE_STATE_COPY_DEST));

			if (barriers.size() > 0)
			{
				pCommandList->ResourceBarrier((uint32_t)(barriers.size()), barriers.data());

				pSrc->SetCurrentState(D3D12_RESOURCE_STATE_COPY_SOURCE);
				pDst->SetCurrentState(D3D12_RESOURCE_STATE_COPY_DEST);
			}
		}

		pCommandList->CopyResource(pDst->GetDxResourcePtr(), pSrc->GetDxResourcePtr());
	}

	void TransitionResource(CommandContext* pCommandContext, Resource* pResource, D3D12_RESOURCE_STATES newState)
	{
		ID3D12GraphicsCommandList* pCommandList = pCommandContext->GetDxCommandList();

		std::vector<D3D12_RESOURCE_BARRIER> barriers;
		if (pResource->GetCurrentState() != newState) barriers.emplace_back(CD3DX12_RESOURCE_BARRIER::Transition(pResource->GetDxResourcePtr(), pResource->GetCurrentState(), newState));

		if (barriers.size() > 0)
		{
			pCommandList->ResourceBarrier((uint32_t)(barriers.size()), barriers.data());

			pResource->SetCurrentState(newState);
		}
	}
}
