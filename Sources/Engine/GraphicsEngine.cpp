
#include <random>

#include "imgui/imgui.h"
#include <imgui/imgui_impl_win32.h>
#include <imgui/imgui_impl_dx12.h>

#include "Utilities/BasicMeshGenerator.h"
#include "Utilities/Utility.h"
#include "Utilities/MathHelper.h"
#include "Engine/EngineWindow.h"
#include "Engine/GraphicsEngine.h"

#include "Scene/SceneLight/SceneLight.h"
#include "Scene/SceneModel/SceneModel.h"

using namespace Utility;
using namespace DirectX;

bool GraphicsEngine::Initialize(HINSTANCE hInstance, uint32_t nCmdShow, uint32_t width, uint32_t height, const std::wstring& name, const std::wstring& title)
{
	if (!m_Window.Initialize(hInstance, nCmdShow, width, height, name, title))
	{
		GFXE_ERROR("Window could not be initialized correctly!");

		return false;
	}

#ifdef GFXE_ENABLE_UI
	if (!m_UI.Initialize(m_Window))
	{
		GFXE_ERROR("UI could not be initialized correctly!");

		return false;
	}
#endif

	if (!m_Renderer.Initialize())
	{
		GFXE_ERROR("Renderer could not be initialized correctly!");

		return false;
	}

	if (!m_Input.Initialize(m_Window))
	{
		GFXE_ERROR("Failed to setup input!");

		return false;
	}

	return true;
}

int32_t GraphicsEngine::Run()
{
	// Create and populate Scene.
	if (!SetupScene())
	{
		GFXE_ERROR("Failed to setup scene!");

		return -1;
	}

	// Create and set initial position of the Camera.
	if (!SetupCamera())
	{
		GFXE_ERROR("Failed to setup camera!");

		return -1;
	}

	// Process initial messages.
	if (const auto ecode = m_Window.ProccessMessages())
	{
		return *ecode;
	}

	m_Renderer.SetRenderUI(m_UI.IsEnabled());
	m_Window.EnableCursor();

	int32_t returnCode = 0;
	double frameDelta = 0.016f;
	uint64_t frameCounter = 0;
	while (true)
	{
		m_Timer.Mark("FrameTime");

		if (const auto ecode = m_Window.ProccessMessages())
		{
			// If return optional has value, it means that
			// we're quitting so return exit code.
			returnCode = *ecode;

			break;
		}

		DoFrame((float)frameDelta);

		frameDelta = m_Timer.Peek("FrameTime");
		frameCounter++;

		// Dirty WA for this unexpected confinment of the cursor to the window.
		// I suspect it happens somewhere behind the scene in the mouse/keyboard input handling code that we use.
		// This needs to be revisited later.
		if (frameCounter <= 100)
		{
			ClipCursor(nullptr);
		}

		//GFXE_DBG_INFO(COMPONENTS::_ENGINE_, StringFormat("Frame time: {:.3f}", frameDelta));
	}

	m_Renderer.ShutDown();

	return returnCode;
}

bool GraphicsEngine::SetupScene()
{
	m_ActiveScene = std::make_unique<Scene>();

	std::random_device randomDevice; // obtain a random number from hardware
	std::mt19937 generator(randomDevice()); // seed the generator
	std::uniform_real_distribution<> distributionX(-9, 9); // define the range
	std::uniform_real_distribution<> distributionY(2, 4); // define the range
	std::uniform_real_distribution<> distributionZ(-2, 6); // define the range
	std::uniform_real_distribution<> distributionColor(0.7, 0.9); // define the range

	// PLANE model
#if 0
	{
		auto pSceneModel = m_ActiveScene->LoadModel("FLOOR", "Data\\Models\\Cube.obj");
		pSceneModel->GetTransformRef().Scale(80.0f, 0.1f, 80.0f);
	}
#endif

	// BUNNY model
#if 0
	{
		m_ActiveScene->LoadModel("BUNNY", "Data\\Models\\Bunny.obj");
	}
#endif

	// GUN model
#if 0
	{
		m_ActiveScene->LoadModel("GUN", "Data\\Models\\Gun.fbx");
	}
#endif

#if 1
	// SPONZA model
	{
		m_ActiveScene->LoadModel("SPONZA", "Data\\Models\\Sponza\\NewSponza_Main_glTF_002.gltf");
		m_ActiveScene->LoadModel("CURTAINS", "Data\\Models\\Sponza\\NewSponza_Curtains_glTF.gltf");

		const DirectX::XMFLOAT3 lightColor = { 210.0f / 255.0f, 168.0f / 255.0f, 67.0f / 255.0f };
		const float_t lightIntensity = 100.0f;

		SceneLight light0(LIGHT_TYPE::POINT_LIGHT);
		light0.SetBaseColor(lightColor);
		light0.GetTransformRef().SetTranslation(-13.598f, 3.4f, -0.063f);
		light0.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light0));

		SceneLight light1(LIGHT_TYPE::POINT_LIGHT);
		light1.SetBaseColor(lightColor);
		light1.GetTransformRef().SetTranslation(15.610f, 2.0f, 0.919f);
		light1.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light1));

		SceneLight light2(LIGHT_TYPE::POINT_LIGHT);
		light2.SetBaseColor(lightColor);
		light2.GetTransformRef().SetTranslation(-1.852f, 3.3f, 4.796f);
		light2.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light2));

		SceneLight light3(LIGHT_TYPE::POINT_LIGHT);
		light3.SetBaseColor(lightColor);
		light3.GetTransformRef().SetTranslation(2.183f, 3.3f, 4.796f);
		light3.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light3));

		SceneLight light4(LIGHT_TYPE::POINT_LIGHT);
		light4.SetBaseColor(lightColor);
		light4.GetTransformRef().SetTranslation(6.147f, 3.3f, 4.796f);
		light4.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light4));

		SceneLight light5(LIGHT_TYPE::POINT_LIGHT);
		light5.SetBaseColor(lightColor);
		light5.GetTransformRef().SetTranslation(-5.922f, 3.3f, 4.796f);
		light5.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light5));

		SceneLight light6(LIGHT_TYPE::POINT_LIGHT);
		light6.SetBaseColor(lightColor);
		light6.GetTransformRef().SetTranslation(10.027f, 3.3f, 4.796f);
		light6.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light6));

		SceneLight light7(LIGHT_TYPE::POINT_LIGHT);
		light7.SetBaseColor(lightColor);
		light7.GetTransformRef().SetTranslation(-9.825f, 3.3f, 4.796f);
		light7.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light7));

		SceneLight light8(LIGHT_TYPE::POINT_LIGHT);
		light8.SetBaseColor(lightColor);
		light8.GetTransformRef().SetTranslation(-5.922f, 3.3f, -4.748f);
		light8.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light8));

		SceneLight light9(LIGHT_TYPE::POINT_LIGHT);
		light9.SetBaseColor(lightColor);
		light9.GetTransformRef().SetTranslation(-9.825f, 3.3f, -4.748f);
		light9.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light9));
		
		SceneLight light10(LIGHT_TYPE::POINT_LIGHT);
		light10.SetBaseColor(lightColor);
		light10.GetTransformRef().SetTranslation(10.158f, 3.3f, -4.748f);
		light10.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light10));

		SceneLight light11(LIGHT_TYPE::POINT_LIGHT);
		light11.SetBaseColor(lightColor);
		light11.GetTransformRef().SetTranslation(2.183f, 3.3f, -4.748f);
		light11.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light11));

		SceneLight light12(LIGHT_TYPE::POINT_LIGHT);
		light12.SetBaseColor(lightColor);
		light12.GetTransformRef().SetTranslation(6.147f, 3.3f, -4.748f);
		light12.SetIntensity(lightIntensity);
		m_ActiveScene->Append(std::move(light12));
	}
#endif

#if 0
	// Test Normals model
	{
		auto pSceneModel = m_ActiveScene->LoadModel("DICE", "Data\\Models\\TestNormals\\TestNormal.gltf");
		pSceneModel->GetTransformRef().SetTranslation(0.0f, 3.0f, 0.0f);
		pSceneModel->GetTransformRef().SetRotation(MATH::DegToRad(90.0f), MATH::DegToRad(-90.0f), MATH::DegToRad(90.0f) );
		pSceneModel->GetTransformRef().SetScale(5.0f, 5.0f, 5.0f);
	}
#endif

#if 0
	// Directional light
	{
		SceneLight light(LIGHT_TYPE::DIRECTIONAL_LIGHT);
		light.SetBaseColor(1.0f, 1.0f, 1.0f);
		light.SetIntensity(1.0f);
		light.GetTransformRef().SetRotation(MATH::DegToRad(45.0f), MATH::DegToRad(0.0), MATH::DegToRad(0.0));
		m_ActiveScene->Append(std::move(light));
	}
#endif

	return true;
}

bool GraphicsEngine::SetupCamera()
{
	m_ActiveCamera = std::make_unique<Camera>();

	m_ActiveCamera->SetPosition(0.0f, 2.5f, 0.0f, true);
	m_ActiveCamera->SetRotation(MATH::DegToRad(-90.0f), 0.0f);
	//m_ActiveCamera->SetPosition(0.0f, 3.0f, -10.0f, true);
	//m_ActiveCamera->Rotate(0.0f, 0.0f);

	XMFLOAT4X4 projectionMatrix;
	DirectX::XMStoreFloat4x4
	(
		&projectionMatrix,
		DirectX::XMMatrixPerspectiveFovLH
		(
			MATH::DegToRad(45.0f),
			(float)EngineWindow::GetWidth() / (float)EngineWindow::GetHeight(),
			0.1f,
			1000.0f
		)
	);

	m_ActiveCamera->SetProjectionMatrix(std::move(projectionMatrix));

	return true;
}

void GraphicsEngine::UpdateScene(float_t frameDelta)
{

}

void GraphicsEngine::ProcessUI()
{
#ifdef GFXE_ENABLE_UI
	m_UI.StartFrame();

	m_UI.DisplaySceneNodes(m_ActiveScene.get());
	m_UI.DisplayRenderOptions(m_Renderer.GetRenderOptionsRef());
	m_UI.DisplayPerformanceData(m_Renderer.GetCurrentFrameDataRef());

	m_UI.EndFrame();
#endif
}

void GraphicsEngine::ProcessInput(float_t frameDelta)
{
	using Keys = DirectX::Keyboard::Keys;

	m_Input.UpdateState();

	if (m_Input.IsRightMouseButtonJustPressed())
	{
		m_ActiveCamera->EnableControl();
		m_Window.DisableCursor();
	}

	if (m_Input.IsRightMouseButtonJustReleased())
	{
		m_ActiveCamera->DisableControl();
		m_Window.EnableCursor();
	}

#ifdef GFXE_ENABLE_UI
	if (m_Input.IsKeyJustReleased(Keys::G))
	{
		bool isUIEnabled = m_UI.EnableSwitch();
		m_Renderer.SetRenderUI(isUIEnabled);

		if (isUIEnabled)
		{
			ImGui::GetIO().ConfigFlags &= ~ImGuiConfigFlags_NoMouse;
		}
		else
		{
			ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NoMouse;
		}
	}
#endif

	if (m_Input.IsKeyJustReleased(Keys::R))
	{
		m_ActiveCamera->ResetTransformations();

		return;
	}

	if (m_ActiveCamera->IsControlled())
	{
		float cameraMoveX = 0.0f, cameraMoveY = 0.0f, cameraMoveZ = 0.0f;

		if (m_Input.IsKeyDown(Keys::W)) cameraMoveZ += frameDelta;
		if (m_Input.IsKeyDown(Keys::S)) cameraMoveZ -= frameDelta;
		if (m_Input.IsKeyDown(Keys::A)) cameraMoveX -= frameDelta;
		if (m_Input.IsKeyDown(Keys::D)) cameraMoveX += frameDelta;
		if (m_Input.IsKeyDown(Keys::E)) cameraMoveY += frameDelta;
		if (m_Input.IsKeyDown(Keys::Q)) cameraMoveY -= frameDelta;

		if (cameraMoveX != 0.0f || cameraMoveY != 0.0f || cameraMoveZ != 0.0f)
		{
			m_ActiveCamera->Translate(cameraMoveX, cameraMoveY, cameraMoveZ);
		}

		const auto mouseState = m_Input.GetMouseState();
		if (mouseState.x != 0 || mouseState.y != 0)
		{
			m_ActiveCamera->Rotate((float)mouseState.x, (float)mouseState.y);
		}
	}
}

void GraphicsEngine::DoFrame(float_t frameDelta)
{
	StartFrame();

	ProcessInput(frameDelta);
	ProcessUI();

	//UpdateScene(frameDelta);

	m_Renderer.Render(m_ActiveScene.get(), m_ActiveCamera.get());

	EndFrame();
}

void GraphicsEngine::StartFrame() {}

void GraphicsEngine::EndFrame() {}