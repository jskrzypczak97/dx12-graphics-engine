
#include "Engine/EngineInput.h"

EngineInput::EngineInput()
{
	m_Keyboard = std::make_unique<DirectX::Keyboard>();
	m_Mouse = std::make_unique<DirectX::Mouse>();
}

bool EngineInput::Initialize(EngineWindow& window)
{
	m_Mouse->SetWindow(window.GetHwnd());
	m_Mouse->SetMode(DirectX::Mouse::MODE_RELATIVE);

	window.RegisterWinMsgsListeningCallback(
		"Input",
		[this](HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			return EngineInput::ProcessWndMessage(hWnd, msg, wParam, lParam);
		}
	);

	return true;
}

void EngineInput::UpdateState()
{
	m_LastKeyboardState = m_CurrentKeyboardState;
	m_LastMouseState = m_CurrentMouseState;

	m_CurrentKeyboardState = m_Keyboard->GetState();
	m_CurrentMouseState = m_Mouse->GetState();
}

bool EngineInput::ProcessWndMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_ACTIVATEAPP:
	{
		DirectX::Keyboard::ProcessMessage(msg, wParam, lParam);
		DirectX::Mouse::ProcessMessage(msg, wParam, lParam);
		break;
	}
	case WM_INPUT:
	case WM_MOUSEMOVE:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_MOUSEWHEEL:
	case WM_XBUTTONDOWN:
	case WM_XBUTTONUP:
	case WM_MOUSEHOVER:
	{
		DirectX::Mouse::ProcessMessage(msg, wParam, lParam);
		break;
	}
	case WM_KEYDOWN:
	case WM_KEYUP:
	case WM_SYSKEYUP:
	case WM_SYSKEYDOWN:
	{
		DirectX::Keyboard::ProcessMessage(msg, wParam, lParam);
		break;
	}
	case WM_ACTIVATE:
	{
		break;
	}
	case WM_MOUSEACTIVATE:
	{
		break;
	}
	case WM_DESTROY:
	{
		break;
	}
	}

	return false;
}
