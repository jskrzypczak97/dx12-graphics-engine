
#include "Engine/EngineWindow.h"
#include "Utilities/Utility.h"

// External dependency
#include <Keyboard.h>
#include <Mouse.h>
//

#include "imgui/imgui_impl_win32.h"

using namespace Utility;

//extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

HWND EngineWindow::m_Hwnd = nullptr;
WNDCLASSEX EngineWindow::m_WndClass;
uint32_t EngineWindow::m_Width = 0;
uint32_t EngineWindow::m_Height = 0;

LRESULT CALLBACK EngineWindow::HandleMsgSetup(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// use create parameter passed in from CreateWindow() to store window class pointer at WinAPI side
	if (msg == WM_NCCREATE)
	{
		// extract ptr to window class from creation data
		const CREATESTRUCTW* const pCreate = reinterpret_cast<CREATESTRUCTW*>(lParam);
		EngineWindow* const pWnd = static_cast<EngineWindow*>(pCreate->lpCreateParams);
		// set WinAPI-managed user data to store ptr to window instance
		SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pWnd));
		// set message proc to normal (non-setup) handler now that setup is finished
		SetWindowLongPtr(hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&EngineWindow::HandleMsgThunk));
		// forward message to window instance handler
		return pWnd->HandleWndMessage(hWnd, msg, wParam, lParam);
	}

	// if we get a message before the WM_NCCREATE message, handle with default handler
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

LRESULT CALLBACK EngineWindow::HandleMsgThunk(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// retrieve ptr to window instance
	EngineWindow* const pWnd = reinterpret_cast<EngineWindow*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
	// forward message to window instance handler
	return pWnd->HandleWndMessage(hWnd, msg, wParam, lParam);
}

LRESULT CALLBACK EngineWindow::HandleWndMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	for (auto const& [id, winMsgsListeningCallback] : m_WinMsgsListeningCallbacks)
	{
		// The callback value determines whether we continue to forward the message.
		if (winMsgsListeningCallback(hWnd, msg, wParam, lParam))
		{
			return true;
		}
	}

	switch (msg)
	{
	case WM_ACTIVATEAPP:
	{
		break;
	}
	case WM_ACTIVATE:
	{
		if (wParam & WA_ACTIVE)
		{
			if (m_CursorEnabled)
			{
				FreeCursor();
				ShowCursor();
			}
			else
			{
				ConfineCursor();
				HideCursor();
			}
		}
		else
		{
			FreeCursor();
			ShowCursor();
		}
		break;
	}
	case WM_INPUT:
	case WM_MOUSEMOVE:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_MOUSEWHEEL:
	case WM_XBUTTONDOWN:
	case WM_XBUTTONUP:
	case WM_MOUSEHOVER:
	{
		break;
	}
	case WM_MOUSEACTIVATE:
	{
		// When you click activate the window, we want Mouse to ignore it.
		return MA_ACTIVATEANDEAT;
	}
	case WM_KEYDOWN:
	case WM_KEYUP:
	case WM_SYSKEYUP:
	{
		if (wParam == VK_ESCAPE)
		{
			DestroyWindow(hWnd);
		}
		return 0;

		break;
	}
	case WM_SYSKEYDOWN:
	{
		break;
	}
	case WM_DESTROY:
	{
		GFXE_DBG_INFO(COMPONENTS::_WINDOW_, "Destroy window message received!");
		PostQuitMessage(0);
		return 0;
	}
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

bool EngineWindow::Initialize(HINSTANCE hInstance, uint32_t nCmdShow, uint32_t width, uint32_t height, const std::wstring& name, const std::wstring& title)
{
	m_Width = width;
	m_Height = height;
	m_Name = name;
	m_Title = title;

	ZeroMemory(&m_WndClass, sizeof(m_WndClass));

	m_WndClass.cbSize = sizeof(WNDCLASSEX);
	m_WndClass.style = CS_OWNDC;
	m_WndClass.lpfnWndProc = HandleMsgSetup;
	m_WndClass.cbClsExtra = 0;
	m_WndClass.cbWndExtra = 0;
	m_WndClass.hInstance = hInstance;
	m_WndClass.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	m_WndClass.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
	m_WndClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	m_WndClass.hbrBackground = GetSysColorBrush(COLOR_BTNFACE);
	m_WndClass.lpszClassName = m_Name.c_str();

	if (!RegisterClassEx(&m_WndClass))
	{
		GFXE_ERROR("Registering window class failed!", std::format("Error code: {}", GetLastError()).c_str());

		return false;
	}

	RECT adjustedWndSize;
	adjustedWndSize.left = adjustedWndSize.top = 0;
	adjustedWndSize.right = m_Width + adjustedWndSize.left;
	adjustedWndSize.bottom = m_Height + adjustedWndSize.top;
	AdjustWindowRect(&adjustedWndSize, WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU, FALSE);

	m_Hwnd = CreateWindowEx(0,
		m_Name.c_str(), m_Title.c_str(),
		WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU, 100, 40,
		adjustedWndSize.right - adjustedWndSize.left, adjustedWndSize.bottom - adjustedWndSize.top,
		nullptr,
		nullptr,
		hInstance,
		this);

	if (!m_Hwnd)
	{
		GFXE_ERROR("Creating window failed!", StringFormat("Error code: {}", GetLastError()).c_str());

		return false;
	}

	ShowWindow(m_Hwnd, nCmdShow);
	UpdateWindow(m_Hwnd);

	return true;
}

void EngineWindow::RegisterWinMsgsListeningCallback(const std::string& stringId, std::function<bool(HWND, UINT, WPARAM, LPARAM)>&& callbackFunction) const
{
	GFXE_ASSERT(!m_WinMsgsListeningCallbacks.contains(stringId), "Callback with the same ID is already registered!");
	
	m_WinMsgsListeningCallbacks.emplace(std::make_pair(stringId, callbackFunction));
}

void EngineWindow::UnregisterWinMsgsListeningCallback(const std::string& stringId) const
{
	GFXE_ASSERT(!m_WinMsgsListeningCallbacks.contains(stringId), "Callback with the given ID is currently not registered so it cannot be removed!");

	m_WinMsgsListeningCallbacks.erase(stringId);
}

std::optional<int> EngineWindow::ProccessMessages()
{
	MSG msg;

	while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		if (msg.message == WM_QUIT)
		{
			return (int)msg.wParam;
		}

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return {};
}

void EngineWindow::EnableCursor()
{
	m_CursorEnabled = true;
	ShowCursor();
	FreeCursor();

	GFXE_DBG_INFO(COMPONENTS::_INPUT_, StringFormat("{}", "CURSOR ENABLED"));
}

void EngineWindow::DisableCursor()
{
	m_CursorEnabled = false;
	HideCursor();
	ConfineCursor();

	GFXE_DBG_INFO(COMPONENTS::_INPUT_, StringFormat("{}", "CURSOR DISABLED"));
}

void EngineWindow::SetTitle(const std::wstring& newTitle)
{
	m_Title = newTitle;

	SetWindowText(m_Hwnd, m_Title.c_str());
}

void EngineWindow::HideCursor() const
{
	while (::ShowCursor(FALSE) >= 0);
}
void EngineWindow::ShowCursor() const
{
	while (::ShowCursor(TRUE) < 0);
}
void EngineWindow::ConfineCursor() const
{
	RECT clientRect;
	GetClientRect(m_Hwnd, &clientRect);
	MapWindowPoints(m_Hwnd, nullptr, reinterpret_cast<POINT*>(&clientRect), 2);
	ClipCursor(&clientRect);
}
void EngineWindow::FreeCursor() const
{
	ClipCursor(nullptr);
}
