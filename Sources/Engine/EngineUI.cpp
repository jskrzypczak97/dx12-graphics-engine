
#include <imgui/imgui_impl_dx12.h>
#include <imgui/imgui.h>

#include "Engine/EngineUI.h"
#include "Engine/EngineWindow.h"
#include "Renderer/Renderer.h"
#include "Scene/SceneModel/SceneModel.h"

#define EXIT_IF_DISABLED if (!m_IsEnabled) { return; }

EngineUI::~EngineUI()
{
	ImGui_ImplDX12_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

bool EngineUI::Initialize(EngineWindow& window)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	(void)io;

	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	if (!ImGui_ImplWin32_Init(window.GetHwnd()))
	{
		GFXE_ERROR("Failed to initialize ImGui Win32!");

		return false;
	}

	window.RegisterWinMsgsListeningCallback(
		"UI",
		[this](HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) { return EngineUI::ProcessWndMessage(hWnd, msg, wParam, lParam); }
	);

	return true;
}

inline bool EngineUI::ProcessWndMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	return ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam) ? true : false;
}

void EngineUI::StartFrame()
{
	EXIT_IF_DISABLED;

	ImGui_ImplDX12_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
}

void EngineUI::EndFrame()
{
	EXIT_IF_DISABLED;

	ImGui::Render();
}

void EngineUI::DisplaySceneNodes(Scene* pScene)
{
	EXIT_IF_DISABLED;

	ImGui::Begin("Scene Entities");
	{
		if (ImGui::BeginTabBar("SceneEntitiesTabBar"))
		{
			if (ImGui::BeginTabItem("Models"))
			{
				//	ImGui::ColorEdit3("Color", reinterpret_cast<float*>(&(material.m_Albedo)));
				//	ImGui::DragFloat("Rougness", reinterpret_cast<float*>(&(material.m_Roughness)), 0.005f, 0.0f, 1.0f);
				//	ImGui::DragFloat("Metalness", reinterpret_cast<float*>(&(material.m_Metalness)), 0.005f, 0.0f, 1.0f);

				for (auto id : pScene->GetSceneEntitiesIds<SCENE_ENTITY_TYPES::MODEL>())
				{
					auto& sceneModel = pScene->GetSceneEntityRef<SceneModel>(id);

					DisplaySceneModelTree(&sceneModel);
				}
				ImGui::EndTabItem();
			}

			if (ImGui::BeginTabItem("Lights"))
			{
				for (auto id : pScene->GetSceneEntitiesIds<SCENE_ENTITY_TYPES::LIGHT>())
				{
					auto& sceneLight = pScene->GetSceneEntityRef<SceneLight>(id);
			
					DisplaySceneLightTree(&sceneLight);
				}
				ImGui::EndTabItem();
			}

			ImGui::EndTabBar();
		}
	}
	ImGui::End();

	ImGui::Begin("Selected Entity");
	{
		if (m_SelectedNodeId < std::numeric_limits<uint64_t>::max())
		{
			auto& selectedNode = pScene->GetSceneEntityRef(m_SelectedNodeId);

			switch (selectedNode.Type())
			{
			case ENTITY:
			{
				DisplaySelectedEntityProperties(selectedNode);
				break;
			}
			case MODEL:
			{
				DisplaySelectedEntityProperties(selectedNode);
				DisplaySelectedModelProperties(pScene->GetSceneEntityRef<SceneModel>(m_SelectedNodeId));
				break;
			}
			case MESH:
			{
				DisplaySelectedEntityProperties(selectedNode);
				DisplaySelectedMeshProperties(pScene->GetSceneEntityRef<SceneMesh>(m_SelectedNodeId));
				break;
			}
			case LIGHT:
			{
				DisplaySelectedEntityProperties(selectedNode);
				DisplaySelectedLightProperties(pScene->GetSceneEntityRef<SceneLight>(m_SelectedNodeId));
				break;
			}
			default:
			{
				GFXE_ERROR("Unknown type!");
			}
			}
		}
		else
		{
			ImGui::Separator();
			std::string str = "None";
			auto windowWidth = ImGui::GetWindowSize().x;
			auto textWidth = ImGui::CalcTextSize(str.c_str()).x;
			ImGui::SetCursorPosX((windowWidth - textWidth) * 0.5f);
			ImGui::Text(str.c_str());
			ImGui::Separator();
		}
	}
	ImGui::End();
}

void EngineUI::DisplayRenderOptions(RenderOptions& renderOptions)
{
	EXIT_IF_DISABLED;

	ImGui::Begin("Render Options");
	ImGui::Checkbox("Normal mapping", &renderOptions.EnableNormalMapping);
	ImGui::End();
}

void EngineUI::DisplayPerformanceData(FrameData& frameData)
{
	EXIT_IF_DISABLED;

	ImGui::Begin("Performance");
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::Text("GPU frame time: %.3f ms", frameData.FrameGpuMeasurements[ProfileGpuStages::FRAME]);
	ImGui::Text("Main pass time: %.3f ms", frameData.FrameGpuMeasurements[ProfileGpuStages::MAIN_PASS]);
	ImGui::Text("Gui pass time: %.3f ms", frameData.FrameGpuMeasurements[ProfileGpuStages::GUI_PASS]);
	ImGui::End();
}

inline void EngineUI::DisplaySceneModelTree(const SceneNodeEntity* pNode)
{
	// Ignore leaf entity nodes.
	if (pNode->GetChildrenCount() == 0 && pNode->Type() == SCENE_ENTITY_TYPES::ENTITY)
	{
		return;
	}

	ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;

	if (pNode->GetId() == m_SelectedNodeId)
	{
		treeNodeFlags |= ImGuiTreeNodeFlags_Selected;
	}

	// Mesh node is always leaf.
	if (pNode->Type() == SCENE_ENTITY_TYPES::MESH)
	{
		treeNodeFlags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Bullet;
	}

	bool isOpened = ImGui::TreeNodeEx((const void*)(uintptr_t)pNode->GetId(), treeNodeFlags, pNode->GetName().c_str());
	if (ImGui::IsItemClicked() && !ImGui::IsItemToggledOpen())
	{
		m_SelectedNodeId = pNode->GetId();
	}

	if (isOpened)
	{
		for (uint32_t i = 0; i < pNode->GetChildrenCount(); ++i)
		{
			DisplaySceneModelTree(pNode->GetChild(i));
		}
	}

	if (isOpened) ImGui::TreePop();
}

inline void EngineUI::DisplaySceneLightTree(const SceneNodeEntity* pNode)
{
	ImGuiTreeNodeFlags treeNodeFlags =
		ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick |
		ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Bullet | ImGuiTreeNodeFlags_NoTreePushOnOpen;

	if (pNode->GetId() == m_SelectedNodeId)
	{
		treeNodeFlags |= ImGuiTreeNodeFlags_Selected;
	}

	ImGui::TreeNodeEx((const void*)(uintptr_t)pNode->GetId(), treeNodeFlags, pNode->GetName().c_str());

	if (ImGui::IsItemClicked() && !ImGui::IsItemToggledOpen())
	{
		m_SelectedNodeId = pNode->GetId();
	}
}

inline void EngineUI::DisplaySelectedEntityProperties(SceneNodeEntity& sceneEntity)
{
	ImGui::SeparatorText("Entity");
	ImGui::BeginTable("EntityInfoTable", 2, ImGuiTableFlags_None);
	ImGui::TableNextRow();
	{
		ImGui::TableSetColumnIndex(0); ImGui::Text("Name:");
		ImGui::TableSetColumnIndex(1); ImGui::Text(sceneEntity.GetName().c_str());
	}
	ImGui::TableNextRow();
	{
		ImGui::TableSetColumnIndex(0); ImGui::Text("Type:");
		ImGui::TableSetColumnIndex(1); ImGui::Text(SceneNodeEntity::GetEntityTypeString(sceneEntity.Type()).data());
	}
	ImGui::EndTable();

	ImGui::SeparatorText("Transform");
	static int32_t radioButtonValue = 0;
	ImGui::RadioButton("Local", &radioButtonValue, 0);
	ImGui::SameLine();
	ImGui::RadioButton("Global", &radioButtonValue, 1);

	if (radioButtonValue == 0) // Local
	{
		DisplayNodeTransform(sceneEntity.GetTransformRef());
	}
	else // Global
	{
		DirectX::XMMATRIX mTransformMatrix = sceneEntity.GetTransform().GetTransformMatrix();
		const SceneNodeEntity* parentNode = sceneEntity.GetParent();
		while (parentNode)
		{
			mTransformMatrix = mTransformMatrix * parentNode->GetTransform().GetTransformMatrix();
			parentNode = parentNode->GetParent();
		}

		Transform::TransposeMatrix(mTransformMatrix);
		const auto mUnpackedTransformMatrix = Transform::UnpackMatrix(mTransformMatrix);
		auto globalTransform = Transform(mUnpackedTransformMatrix);
		DisplayNodeTransformReadOnly(globalTransform);
	}
}

inline void EngineUI::DisplaySelectedModelProperties(SceneModel& sceneModel)
{
	ImGui::SeparatorText("Model");
	ImGui::Checkbox("Visible", &sceneModel.m_IsVisible);
}

inline void EngineUI::DisplaySelectedMeshProperties(SceneMesh& sceneMesh)
{
	ImGui::SeparatorText("Model");
	ImGui::Checkbox("Visible", &sceneMesh.m_IsVisible);
}

inline void EngineUI::DisplaySelectedLightProperties(SceneLight& sceneLight)
{
	ImGui::SeparatorText("Light");

	ImGui::Checkbox("Enabled", &sceneLight.m_IsActive);
	ImGui::Spacing();
	ImGui::ColorEdit3("Color", reinterpret_cast<float*>(&(sceneLight.m_BaseColor)));
	ImGui::DragFloat("Intensity", reinterpret_cast<float*>(&(sceneLight.m_Intensity)), 1.0f, 0.0f, 1000.0f);
}

inline void EngineUI::DisplayNodeTransform(Transform& transform)
{
	if (ImGui::DragFloat3("Translation", reinterpret_cast<float_t*>(&(transform.m_Translation))))
	{
		transform.MarkAsDirty();
	}
	
	float_t degRotationBuffer[] = { MATH::RadToDeg(transform.m_Rotation.x), MATH::RadToDeg(transform.m_Rotation.y), MATH::RadToDeg(transform.m_Rotation.z) };
	if (ImGui::DragFloat3("Rotation (deg)", degRotationBuffer, 0.25f, -180.0f, 180.0f))
	{
		transform.SetRotation(MATH::DegToRad(degRotationBuffer[0]), MATH::DegToRad(degRotationBuffer[1]), MATH::DegToRad(degRotationBuffer[2]));
	}

	if (ImGui::DragFloat3("Scale", reinterpret_cast<float_t*>(&(transform.m_Scale))))
	{
		transform.MarkAsDirty();
	}
}

inline void EngineUI::DisplayNodeTransformReadOnly(const Transform& transform)
{
	ImGui::BeginDisabled(true);
	DirectX::XMFLOAT3 pTranslation = transform.m_Translation;
	ImGui::DragFloat3("Translation", reinterpret_cast<float_t*>(&(pTranslation)));

	float_t degRotationBuffer[] = { MATH::RadToDeg(transform.m_Rotation.x), MATH::RadToDeg(transform.m_Rotation.y), MATH::RadToDeg(transform.m_Rotation.z) };
	ImGui::DragFloat3("Rotation (deg)", degRotationBuffer, 0.25f, -180.0f, 180.0f);

	DirectX::XMFLOAT3 pScale = transform.m_Scale;
	ImGui::DragFloat3("Scale", reinterpret_cast<float_t*>(&(pScale)));
	ImGui::EndDisabled();
}
