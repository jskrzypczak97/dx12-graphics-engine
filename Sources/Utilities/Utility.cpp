
#include <algorithm>

#include "Utilities/Utility.h"

namespace Utility
{
	std::vector<std::string> g_LoggingComponents
	{
		LOGGING_COMPONENTS(CREATE_STRINGS)
	};

	void StringToUpper(std::string& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::toupper);
	}

	std::string StringToUpper(const std::string& str)
	{
		std::string output(str);
		std::transform(output.begin(), output.end(), output.begin(), ::toupper);
		return output;
	}

	void Print(const std::string& str)
	{
		OutputDebugStringA(str.c_str());
	}

	void Print(const std::wstring& str)
	{
		OutputDebugString(str.c_str());
	}

	void PrintMessage(const std::string& str)
	{
		Print(" # " + str + '\n');
	}

	std::wstring UTF8ToWideString(const std::string& str)
	{
		wchar_t wstr[MAX_PATH];
		if (!MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, str.c_str(), -1, wstr, MAX_PATH))
			wstr[0] = L'\0';
		return wstr;
	}

	std::string WideStringToUTF8(const std::wstring& wstr)
	{
		char str[MAX_PATH];
		if (!WideCharToMultiByte(CP_ACP, MB_PRECOMPOSED, wstr.c_str(), -1, str, MAX_PATH, nullptr, nullptr))
			str[0] = L'\0';
		return str;
	}
}

#undef LOGGING_COMPS
