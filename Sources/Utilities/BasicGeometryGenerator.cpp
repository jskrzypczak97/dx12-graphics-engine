
#include "Utilities/BasicMeshGenerator.h"
#include "Utilities/MathHelper.h"
#include "Utilities/Utility.h"

using namespace DirectX;

std::unordered_map<std::string, Mesh> BasicMeshGenerator::m_MeshCache;

Mesh BasicMeshGenerator::MakeMesh(BasicMeshType meshType)
{
	Mesh mesh = {};
	MakeMesh(meshType, mesh);

	return mesh;
}

void BasicMeshGenerator::MakeMesh(BasicMeshType meshType, Mesh& mesh)
{
	switch (meshType)
	{
	case BasicMeshType::CUBE:
		MakeCube(mesh);
		break;
	case BasicMeshType::SPHERE:
		MakeSphere(mesh);
		break;
	case BasicMeshType::PYRAMID:
		MakePyramid(mesh);
		break;
	default:
		GFXE_ASSERT(false, "Undefined mesh type!");
		break;
	}

	return;
}

void BasicMeshGenerator::MakeCube(Mesh& mesh)
{
	const std::string meshName = "CUBE";

	if (!m_MeshCache.contains(meshName))
	{
		m_MeshCache.insert(std::make_pair(meshName, CreateCube(meshName)));
	}

	mesh = m_MeshCache.at(meshName);
}

void BasicMeshGenerator::MakeCustomSphere(Mesh& mesh, float radius, uint32_t latDiv, uint32_t longDiv)
{
	const std::string meshName = Utility::StringFormat("SPHERE_RAD_{:.3f}_LAT_{}_LONG_{}", radius, latDiv, longDiv);

	auto customCreateFunction = [radius, latDiv, longDiv](const std::string& meshName) -> Mesh
	{ return CreateSphere(meshName, radius, latDiv, longDiv); };

	Cache(meshName, customCreateFunction);

	mesh = m_MeshCache.at(meshName);
}

void BasicMeshGenerator::MakeSphere(Mesh& mesh)
{
	const std::string meshName = "SPHERE";

	auto customCreateFunction = [](const std::string& meshName) -> Mesh
	{ return CreateSphere(meshName, 0.5f, 36, 72); };

	Cache(meshName, customCreateFunction);

	mesh = m_MeshCache.at(meshName);
}

void BasicMeshGenerator::MakePyramid(Mesh& mesh)
{
	const std::string meshName = "PYRAMID";

	Cache(meshName, CreatePyramid);

	mesh = m_MeshCache.at(meshName);
}

void inline BasicMeshGenerator::Cache(const std::string& meshName, std::function<Mesh(const std::string&)> createFunction)
{
	if (!m_MeshCache.contains(meshName))
	{
		m_MeshCache.insert(std::make_pair(meshName, createFunction(meshName)));
	}
}

Mesh BasicMeshGenerator::CreateCube(const std::string& meshName)
{
	Mesh mesh;
	mesh.Name = meshName;

	mesh.Vertices = {

		// Front face
		{ -0.5f,  0.5f, -0.5f },
		{  0.5f, -0.5f, -0.5f },
		{ -0.5f, -0.5f, -0.5f },
		{  0.5f,  0.5f, -0.5f },

		// Right side face
		{  0.5f, -0.5f, -0.5f },
		{  0.5f,  0.5f,  0.5f },
		{  0.5f, -0.5f,  0.5f },
		{  0.5f,  0.5f, -0.5f },

		// Left side face
		{ -0.5f,  0.5f,  0.5f },
		{ -0.5f, -0.5f, -0.5f },
		{ -0.5f, -0.5f,  0.5f },
		{ -0.5f,  0.5f, -0.5f },

		// Back face
		{  0.5f,  0.5f,  0.5f },
		{ -0.5f, -0.5f,  0.5f },
		{  0.5f, -0.5f,  0.5f },
		{ -0.5f,  0.5f,  0.5f },

		// Top face
		{ -0.5f,  0.5f, -0.5f },
		{  0.5f,  0.5f,  0.5f },
		{  0.5f,  0.5f, -0.5f },
		{ -0.5f,  0.5f,  0.5f },

		// Bottom face
		{  0.5f, -0.5f,  0.5f },
		{ -0.5f, -0.5f, -0.5f },
		{  0.5f, -0.5f, -0.5f },
		{ -0.5f, -0.5f,  0.5f },

	};

	mesh.Indices = {
		// Front face
		0, 1, 2, // First triangle
		0, 3, 1, // Second triangle

		// Left face
		4, 5, 6, // First triangle
		4, 7, 5, // second triangle

		// Right face
		8, 9, 10, // First triangle
		8, 11, 9, // Second triangle

		// Back face
		12, 13, 14, // First triangle
		12, 15, 13, // Second triangle

		// Top face
		16, 17, 18, // First triangle
		16, 19, 17, // Second triangle

		// Bottom face
		20, 21, 22, // First triangle
		20, 23, 21, // Second triangle
	};

	mesh.Normals = {

		// Front face
		{ 0.0f, 0.0f, -1.0f },
		{ 0.0f, 0.0f, -1.0f },
		{ 0.0f, 0.0f, -1.0f },
		{ 0.0f, 0.0f, -1.0f },

		// Right side face
		{ 1.0f, 0.0f, 0.0f },
		{ 1.0f, 0.0f, 0.0f },
		{ 1.0f, 0.0f, 0.0f },
		{ 1.0f, 0.0f, 0.0f },

		// Left side face
		{ -1.0f, 0.0f, 0.0f },
		{ -1.0f, 0.0f, 0.0f },
		{ -1.0f, 0.0f, 0.0f },
		{ -1.0f, 0.0f, 0.0f },

		// Back face
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },

		// Top face
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f },

		// Bottom face
		{ 0.0f, -1.0f, 0.0f },
		{ 0.0f, -1.0f, 0.0f },
		{ 0.0f, -1.0f, 0.0f },
		{ 0.0f, -1.0f, 0.0f },
	};

	//mesh.TexCoords0 = std::vector<XMFLOAT3>(mesh.GetVertexCount(), { 0.0f, 0.0f, 0.0f });

	return mesh;
}

Mesh BasicMeshGenerator::CreateSphere(const std::string& meshName, float radius, uint32_t latDiv, uint32_t longDiv)
{
	Mesh mesh;
	mesh.Name = meshName;

	uint32_t estVerticesCount = (latDiv - 1) * longDiv + 2;
	uint32_t estIndicesCount = estVerticesCount * 6;

	const XMVECTOR sphereCenter = { 0.0f, 0.0f, 0.0f };
	const XMVECTOR base = { 0.0f, 0.0f, radius };
	const float lattitudeAngle = MATH::PI_32() / latDiv;
	const float longitudeAngle = 2.0f * MATH::PI_32() / longDiv;

	mesh.Vertices.reserve(estVerticesCount);
	mesh.Normals.reserve(estVerticesCount);
	for (uint32_t iLat = 1; iLat < latDiv; iLat++)
	{
		const auto latBase = XMVector3Transform(base, XMMatrixRotationX(lattitudeAngle * iLat));
		for (uint32_t iLong = 0; iLong < longDiv; iLong++)
		{
			mesh.Vertices.emplace_back();
			auto vertexPosition = XMVector3Transform(latBase, XMMatrixRotationZ(longitudeAngle * iLong));
			DirectX::XMStoreFloat3(&mesh.Vertices.back(), vertexPosition);

			mesh.Normals.emplace_back();
			auto vertexNormal = XMVector3Normalize(vertexPosition - sphereCenter);
			DirectX::XMStoreFloat3(&mesh.Normals.back(), vertexNormal);
		}
	}

	// Add the cap vertices
	const auto iNorthPole = (uint32_t)mesh.Vertices.size();
	mesh.Vertices.emplace_back();
	DirectX::XMStoreFloat3(&mesh.Vertices.back(), base);
	mesh.Normals.emplace_back();
	DirectX::XMStoreFloat3(&mesh.Normals.back(), XMVector3Normalize(base - sphereCenter));

	const auto iSouthPole = (uint32_t)mesh.Vertices.size();
	mesh.Vertices.emplace_back();
	DirectX::XMStoreFloat3(&mesh.Vertices.back(), base * XMVECTOR{ -1.0f, -1.0f, -1.0f });
	mesh.Normals.emplace_back();
	DirectX::XMStoreFloat3(&mesh.Normals.back(), XMVector3Normalize((base * XMVECTOR{ -1.0f, -1.0f, -1.0f }) - sphereCenter));

	const auto calcIdx = [latDiv, longDiv](uint32_t iLat, uint32_t iLong)
	{
		return iLat * longDiv + iLong;
	};

	mesh.Indices.reserve(estIndicesCount);
	for (uint32_t iLat = 0; iLat < latDiv - 2; iLat++)
	{
		for (uint32_t iLong = 0; iLong < longDiv - 1; iLong++)
		{
			mesh.Indices.push_back(calcIdx(iLat, iLong));
			mesh.Indices.push_back(calcIdx(iLat + 1, iLong));
			mesh.Indices.push_back(calcIdx(iLat, iLong + 1));
			mesh.Indices.push_back(calcIdx(iLat, iLong + 1));
			mesh.Indices.push_back(calcIdx(iLat + 1, iLong));
			mesh.Indices.push_back(calcIdx(iLat + 1, iLong + 1));
		}
		// Wrap band
		mesh.Indices.push_back(calcIdx(iLat, longDiv - 1));
		mesh.Indices.push_back(calcIdx(iLat + 1, longDiv - 1));
		mesh.Indices.push_back(calcIdx(iLat, 0));
		mesh.Indices.push_back(calcIdx(iLat, 0));
		mesh.Indices.push_back(calcIdx(iLat + 1, longDiv - 1));
		mesh.Indices.push_back(calcIdx(iLat + 1, 0));
	}

	// Cap fans
	for (uint32_t iLong = 0; iLong < longDiv - 1; iLong++)
	{
		// North
		mesh.Indices.push_back(iNorthPole);
		mesh.Indices.push_back(calcIdx(0, iLong));
		mesh.Indices.push_back(calcIdx(0, iLong + 1));
		// South
		mesh.Indices.push_back(calcIdx(latDiv - 2, iLong + 1));
		mesh.Indices.push_back(calcIdx(latDiv - 2, iLong));
		mesh.Indices.push_back(iSouthPole);
	}
	// Wrap triangles
	// North
	mesh.Indices.push_back(iNorthPole);
	mesh.Indices.push_back(calcIdx(0, longDiv - 1));
	mesh.Indices.push_back(calcIdx(0, 0));
	// South
	mesh.Indices.push_back(calcIdx(latDiv - 2, 0));
	mesh.Indices.push_back(calcIdx(latDiv - 2, longDiv - 1));
	mesh.Indices.push_back(iSouthPole);

	//mesh.TexCoords0 = std::vector<XMFLOAT3>(mesh.GetVertexCount(), { 0.0f, 0.0f, 0.0f });

	return mesh;
}

Mesh BasicMeshGenerator::CreatePyramid(const std::string& meshName)
{
	Mesh mesh;
	mesh.Name = meshName;

	mesh.Vertices = {

		// Front face
		{  0.0f,  0.5f,  0.0f },
		{  0.5f, -0.5f, -0.5f },
		{ -0.5f, -0.5f, -0.5f },

		// Right side face
		{  0.0f,  0.5f,  0.0f },
		{  0.5f, -0.5f,  0.5f },
		{  0.5f, -0.5f, -0.5f },

		// Back face
		{  0.0f,  0.5f,  0.0f },
		{  0.5f, -0.5f,  0.5f },
		{ -0.5f, -0.5f,  0.5f },

		// Left side face
		{  0.0f,  0.5f,  0.0f },
		{ -0.5f, -0.5f, -0.5f },
		{ -0.5f, -0.5f,  0.5f },

		// Bottom face
		{ -0.5f, -0.5f, -0.5f },
		{  0.5f, -0.5f, -0.5f },
		{  0.5f, -0.5f,  0.5f },
		{ -0.5f, -0.5f,  0.5f },
	};

	mesh.Indices = {
		// Front face
		0, 1, 2,
		// Right side face
		3, 4, 5,
		// Back face
		7, 6, 8,
		// Left side face
		9, 10, 11,
		// Bottom face
		12, 13, 15,
		13, 14, 15
	};

	mesh.Normals = CalculateNormals(mesh.Vertices, mesh.Indices);

	//mesh.TexCoords0 = std::vector<XMFLOAT3>(mesh.GetVertexCount(), { 0.0f, 0.0f, 0.0f });

	return mesh;
}

std::vector<XMFLOAT3> BasicMeshGenerator::CalculateNormals(const std::vector<DirectX::XMFLOAT3>& vertices, const std::vector<uint32_t>& indices)
{
	GFXE_ASSERT(vertices.size() > 0 && indices.size() > 0, "Vertices and indices must be defined first!");
	GFXE_ASSERT((indices.size() % 3) == 0, "Number of indices should be divisible by 3!");

	auto normals = std::vector<XMFLOAT3>(vertices.size(), { 0.0f, 0.0f, 0.0f });
	for (uint32_t i = 0; i < indices.size(); i += 3)
	{
		XMFLOAT3 currentNormal;

		auto idxO = indices[i];
		auto idxA = indices[i + 1];
		auto idxB = indices[i + 2];

		auto vertexO = XMLoadFloat3(&vertices[idxO]);
		auto vertexA = XMLoadFloat3(&vertices[idxA]);
		auto vertexB = XMLoadFloat3(&vertices[idxB]);

		auto vectorOA = vertexA - vertexO;
		auto vectorOB = vertexB - vertexO;

		XMStoreFloat3(&currentNormal, XMVector3Normalize(XMVector3Cross(vectorOA, vectorOB)));

		normals[idxO] = normals[idxA] = normals[idxB] = currentNormal;
	}

	return normals;
}