
#include "Utilities/Timer.h"
#include "Utilities/Utility.h"

using namespace std::chrono;

Timer::Timer()
	: m_MarkedTimePoints()
{
	m_MarkedTimePoints.emplace(std::make_pair("default", high_resolution_clock::now()));
}

void Timer::Mark(const std::string& timePointStr)
{
	m_MarkedTimePoints[timePointStr] = high_resolution_clock::now();
}

double Timer::Peek(const std::string& timePointStr, Unit unit) const
{
	const auto currentTimePoint = high_resolution_clock::now();

	GFXE_ASSERT(m_MarkedTimePoints.contains(timePointStr), "Can't peek time that was never marked!");

	return duration_cast<microseconds>(currentTimePoint - m_MarkedTimePoints.at(timePointStr)).count() / double(unit);
}