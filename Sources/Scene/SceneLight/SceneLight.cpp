
#include "Scene/SceneLight/SceneLight.h"
#include "Utilities/Utility.h"

using namespace DirectX;

std::atomic<uint64_t> SceneLight::s_CurrentLightId{ 0 };

SceneLight::SceneLight(LIGHT_TYPE lightType)
	: SceneNodeEntity(), m_LightType(lightType)
{
	SetName(Utility::StringFormat("Light_{}_{}", s_CurrentLightId.load(), m_LightType == LIGHT_TYPE::POINT_LIGHT ? "Point" : "Directional"));

	s_CurrentLightId++;
}