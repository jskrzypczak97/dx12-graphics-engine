
#include "Scene/SceneNodeEntity.h"

std::atomic<uint64_t> SceneNodeEntity::s_CurrentEntityId{ 0 };

const std::array<std::string, SCENE_ENTITY_TYPES::TYPES_COUNT> SceneNodeEntity::s_TypeToString =
{
	"SceneNodeEntity",	// ENTITY
	"SceneModel",		// MODEL
	"SceneMesh",		// MESH
	"SceneLight",		// LIGHT
};
