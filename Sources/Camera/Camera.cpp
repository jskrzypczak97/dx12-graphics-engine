
#include "Camera/Camera.h"
#include <algorithm>

using namespace DirectX;

Camera::Camera() :
	m_Position(0.0f, 0.0f, 0.0f),
	m_DefaultPosition(0.0f, 0.0f, 0.0f),
	m_Pitch(0.0f),
	m_Yaw(0.0f),
	m_BaseForwardDirection(0.0f, 0.0f, 1.0f),
	m_BaseUpDirection(0.0f, 1.0f, 0.0f),
	m_ViewMatrix(),
	m_ViewMatrixUpdateNeeded(false),
	m_ProjectionMatrix(),
	m_IsControlled(false)
{
	m_RotationSpeed = 0.004f;
	m_TravelSpeed = 0.02f;
}

void Camera::SetPosition(float x, float y, float z, bool setDefault)
{
	m_Position = DirectX::XMFLOAT3(x, y, z);

	if (setDefault)
	{
		SetDefaultPosition(x, y, z);
	}

	m_ViewMatrixUpdateNeeded = true;
}

void Camera::SetDefaultPosition(float x, float y, float z)
{
	m_DefaultPosition = DirectX::XMFLOAT3(x, y, z);
}

void Camera::Translate(float x, float y, float z)
{
	DirectX::XMFLOAT3 translationDelta;

	DirectX::XMStoreFloat3(&translationDelta, DirectX::XMVector3Transform(
		DirectX::XMVectorSet(x, y, z, 0.0f),
		DirectX::XMMatrixRotationRollPitchYaw(m_Pitch, m_Yaw, 0.0f) *
		DirectX::XMMatrixScaling(m_TravelSpeed, m_TravelSpeed, m_TravelSpeed)));

	m_Position.x += translationDelta.x;
	m_Position.y += translationDelta.y;
	m_Position.z += translationDelta.z;

	m_ViewMatrixUpdateNeeded = true;
}

void Camera::SetRotation(float_t yaw, float_t pitch)
{
	m_Yaw = yaw;
	m_Pitch = pitch;

	m_ViewMatrixUpdateNeeded = true;
}

void Camera::Rotate(float deltaX, float deltaY)
{
	m_Yaw = MATH::WrapAngleNegPiPosPi(m_Yaw + deltaX * m_RotationSpeed);
	m_Pitch = std::clamp(m_Pitch + deltaY * m_RotationSpeed, -MATH::PI_32() / 2.0f, MATH::PI_32() / 2.0f);

	m_ViewMatrixUpdateNeeded = true;
}

void Camera::ResetTransformations()
{
	SetPosition(m_DefaultPosition.x, m_DefaultPosition.y, m_DefaultPosition.z);
	m_Pitch = m_Yaw = 0.0f;

	m_ViewMatrixUpdateNeeded = true;
}

const XMMATRIX Camera::GetViewMatrix()
{
	if (m_ViewMatrixUpdateNeeded)
	{
		auto output = CalculateViewMatrix();
		XMStoreFloat4x4(&m_ViewMatrix, output);
		m_ViewMatrixUpdateNeeded = false;

		return output;
	}
	else
	{
		return XMLoadFloat4x4(&m_ViewMatrix);
	}
}

DirectX::XMMATRIX Camera::CalculateViewMatrix() const
{
	const auto currentPosition = XMLoadFloat3(&m_Position);
	const auto currentLookDirection = DirectX::XMVector3Transform(XMLoadFloat3(&m_BaseForwardDirection), DirectX::XMMatrixRotationRollPitchYaw(m_Pitch, m_Yaw, 0.0f));

	return DirectX::XMMatrixLookAtLH(
		currentPosition,
		currentPosition + currentLookDirection,
		XMLoadFloat3(&m_BaseUpDirection));
}